package com.xiia.servcie.imp;

import com.xiia.dao.CheckDao;
import com.xiia.service.inter.ICheck;
import com.xiia.util.Pager;
import com.xiia.vo.Check;
/**
 * 
 * @author meigang 2014-11-10 17:43
 *
 */
public class CheckService implements ICheck{
	private static CheckDao checkDao;
	public CheckService() {
		// TODO Auto-generated constructor stub
		checkDao = new CheckDao();
	}
	@Override
	public int countByType(String type) {
		// TODO Auto-generated method stub
		return checkDao.countByType(type);
	}
	@Override
	public Pager findCheckPage(Pager page, String orderBy,Check c) {
		// TODO Auto-generated method stub
		return checkDao.findCheckPage(page,orderBy,c);
	}
	@Override
	public boolean check(Check c,int status) {
		// TODO Auto-generated method stub
		return checkDao.startCheck(c,status);
	}
	@Override
	public int count() {
		// TODO Auto-generated method stub
		return checkDao.count();
	}
	@Override
	public Pager findAllCheckPage(Pager page,String orderBy) {
		// TODO Auto-generated method stub
		return checkDao.findAllCheckPage(page,orderBy);
	}

}
