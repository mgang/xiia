package com.xiia.servcie.imp;

import com.xiia.dao.PublishDao;
import com.xiia.service.inter.IPublish;
import com.xiia.util.Pager;
import com.xiia.vo.Publish;
/**
 * 
 * @author meigang 2014-11-10 17:43
 *
 */
public class PublishService implements IPublish{
	private static PublishDao publishDao;
	public PublishService(){
		publishDao = new PublishDao();
	}
	@Override
	public boolean publish(Publish p) {
		// TODO Auto-generated method stub
		return publishDao.publish(p);
	}
	@Override
	public int count() {
		// TODO Auto-generated method stub
		return publishDao.count();
	}
	@Override
	public Pager findAllPublishPage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		return publishDao.findAllPublishPage(page,orderBy);
	}

}
