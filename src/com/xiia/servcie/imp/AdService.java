package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.AdDao;
import com.xiia.service.inter.IAd;
import com.xiia.util.Pager;
import com.xiia.vo.Ad;
/**
 * 
 * @author 张文龙 2014-11-4 9:37
 *
 */
public  class AdService implements IAd{
	private static AdDao adDao;
	public AdService() {
		adDao = new AdDao();
	}

	public int getAdNum(){
		return adDao.getAdNum();
	}
	
	public boolean entryAd(Ad ad){
		if(adDao.add(ad) > 0){
			return true;
		}else{
			return false;
		}
	}
	
	public List<Ad> selectAd(Ad ad){
		return adDao.selectAd(ad);
	}
	
	public Pager selectAd1(Pager page){
		return adDao.selectAd1(page);
	}
	
	
	
	public boolean releaseAd(Ad ad){
		return adDao.updateStatus(ad);
	}
	
	public boolean undoAd(Ad ad){
		return adDao.updateStatus(ad);
	}
	
	public boolean updateAdLink(Ad ad){
		return adDao.update(ad);
	}
	
	public boolean deleteAd(Ad ad){
		return adDao.delete(ad);
	}	
	
	public boolean checkAd(Ad ad){
		return adDao.updateStatus(ad);
	}
	
	public boolean addAd(Ad ad){
		return adDao.add_check(ad);
	}
}
