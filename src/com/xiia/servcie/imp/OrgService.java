package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.OrgDao;
import com.xiia.service.inter.IOrg;
import com.xiia.util.Pager;
import com.xiia.vo.Org;
/**
 * 
 * @author 胡天天  2014-11-4
 * OrgService的实现
 *
 */
/**
 * 
 * @author 胡天天  2014-11-4
 * OrgService的实现
 *
 */

public class OrgService implements IOrg {
	OrgDao orgdao=new OrgDao();
	@Override
	public boolean addOrg(Org org){
		return orgdao.addOrg(org);
	}
	@Override
	public boolean updateOrg(Org org){
		return orgdao.updateOrg(org);
	}
	@Override
	public boolean deleteOrg(Org org){
		return orgdao.deleteOrg(org);
	}
	@Override
	public Pager findAllOrg(Pager page){
		return orgdao.findAllOrg(page);
	}
	@Override
	public Org findOrgById(int orgId){
		return orgdao.findOrgById(orgId);
	}
	@Override
	public boolean isExist(String orgName){
		return orgdao.isExist(orgName);
	}
	public int getAllCount(){
		return orgdao.getAllCount();
	}
	
}
