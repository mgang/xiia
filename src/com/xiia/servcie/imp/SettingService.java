package com.xiia.servcie.imp;

import com.xiia.dao.SettingDao;
import com.xiia.service.inter.ISetting;
import com.xiia.util.ParamUtil;
import com.xiia.vo.Setting;
/**
 * @author--王书恒--2014-11-4
 * SettingService的实现
 */

public class SettingService implements ISetting {
	private static SettingDao settingDao;
	static{
		settingDao = new SettingDao();
	}
	public SettingService(){
		
	}
	/*
	 * 初始化系统参数
	 */
	public static void initParamUtilSetting(){
		Setting setting = settingDao.getSetting();
		ParamUtil.MENMBER_SCORE = setting.getMenmberScore();
		ParamUtil.UPLOAD_RESOURCE_FILE_SIZE = setting.getUploadResourceSize();
		ParamUtil.NOMAL_MANAGER_SCORE = setting.getAdminScore();
		ParamUtil.ACCEPT_RESOURCE_FILE_TYPE = setting.getAcceptUploadFileType();
		ParamUtil.MENU_MAX_COUNT = setting.getMenuMaxCount();
		ParamUtil.HEADLINE_NEWS_NUM = setting.getHeadlineNewsNum();
	}
	@Override
	public Setting getSetting() {
		// TODO Auto-generated method stub
		return settingDao.getSetting();
	}
	@Override
	public boolean updateSetting(Setting setting) {
		// TODO Auto-generated method stub
		if(settingDao.updateSetting(setting) > 0){
			//更新成功，再次初始化
			initParamUtilSetting();
			return true;	
		}
		else
			return false;
	}
}
