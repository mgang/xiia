package com.xiia.servcie.imp;
import com.xiia.dao.ProjectDao;
import com.xiia.service.inter.IProject;
import com.xiia.util.Pager;
import com.xiia.vo.Project;
/**
 * @author	王书恒--2014-11-4
 * ProjectService的实现
 */

public class ProjectService implements IProject{
	private static ProjectDao projectDao; 
	public ProjectService(){
		projectDao= new ProjectDao();
	}
	

	@Override
	public boolean addProject(Project project) {
		return projectDao.addProject(project);
	}
	
	@Override
	public boolean deleteProject(Project project) {
		return projectDao.delete(project);
	}
	
	@Override
	public boolean updateProject(Project project) {
		return projectDao.update(project);
	}

	@Override
	public int countProject() {
		// TODO Auto-generated method stub
		return projectDao.countProject();
	}

	@Override
	public Pager findProjectPage(Pager page, String string) {
		// TODO Auto-generated method stub
		return projectDao.findProjectPage(page,string);
	}

	@Override
	public Project getProjectById(int proId) {
		// TODO Auto-generated method stub
		return projectDao.getProjectById(proId);
	}
}