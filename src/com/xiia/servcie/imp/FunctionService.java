package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.FunctionDao;
import com.xiia.service.inter.IFunction;
import com.xiia.util.Pager;
import com.xiia.vo.Function;
/**
 * 
 * @author meigang 2014-11-4 9:22
 * FunctionService的实现
 */
public class FunctionService implements IFunction{
	private static FunctionDao funDao;
	public FunctionService(){
		funDao = new FunctionDao();
	}
	
	@Override
	public boolean addFunction(Function f) {
		// TODO Auto-generated method stub
		if(funDao.addFunction(f) > 0){
			return true;
		}else{
			return false;
		}
		
	}

	@Override
	public int countFunction() {
		// TODO Auto-generated method stub
		return funDao.getCount();
	}

	@Override
	public Pager findFunPage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		return funDao.getFunPage(page,orderBy);
	}

	@Override
	public boolean deleteFunction(Function f) {
		// TODO Auto-generated method stub
		if(funDao.deleteFunction(f) > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean updateFunction(Function f) {
		// TODO Auto-generated method stub
		if(funDao.updateFunction(f) > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Function getFunctionById(int funId) {
		// TODO Auto-generated method stub
		return funDao.getFunctionById(funId);
	}

	@Override
	public List<Function> getAllFunctions() {
		// TODO Auto-generated method stub
		return funDao.getAllFunctions();
	}

}
