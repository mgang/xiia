package com.xiia.servcie.imp;

import com.xiia.dao.RevokeDao;
import com.xiia.service.inter.IRevoke;
import com.xiia.util.Pager;
import com.xiia.vo.Revoke;
/**
 * 
 * @author meigang 2014-11-10 17:44
 *
 */
public class RevokeService implements IRevoke{
	private static RevokeDao revokeDao;
	public RevokeService(){
		revokeDao = new RevokeDao();
	}
	@Override
	public boolean revoke(Revoke r) {
		// TODO Auto-generated method stub
		return revokeDao.revoke(r);
	}
	@Override
	public int count() {
		// TODO Auto-generated method stub
		return revokeDao.count();
	}
	@Override
	public Pager findAllRevokePage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		return revokeDao.findAllRevokePage(page,orderBy);
	}

}
