package com.xiia.servcie.imp;

import com.xiia.dao.RuleDao;
import com.xiia.service.inter.IRule;
import com.xiia.util.Pager;
import com.xiia.vo.Rule;
/**
 * 
 * @author 胡天天   2014-11-4
 * RuleService的实现
 *
 */
public class RuleService implements IRule {
	RuleDao ruledao=new RuleDao();
	@Override
	public boolean addRules(Rule rule){
		return ruledao.addRules(rule);
	}
	@Override
	public boolean updateRule(Rule rule,int oSortId){
		return ruledao.updateRule(rule,oSortId);
	}
	@Override
	public boolean deleteRule(Rule rule){
		return ruledao.deleteRule(rule);
	}
	@Override
	public Pager findAllRule(Pager page){
		return ruledao.findAllRule(page);
	}
	@Override
	public Rule findRuleById(int ruleId){
		return ruledao.findRuleById(ruleId);
	}
	@Override
	public boolean deleteRuleBySortId(int sortId){
		return ruledao.deleteRuleBySortId(sortId);
	}
	@Override
	public boolean ruleName(String ruleName){
		return ruledao.ruleName(ruleName);
	}
	public int getAllCount(){
		return ruledao.getAllCount();
	}
}
