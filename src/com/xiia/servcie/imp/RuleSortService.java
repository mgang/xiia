package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.RuleSortDao;
import com.xiia.service.inter.IRuleSort;
import com.xiia.util.Pager;
import com.xiia.vo.RuleSort;
/**
 * 
 * @author 胡天天  2014-11-4
 * RlueSortService的实现
 *
 */

public class RuleSortService implements IRuleSort {
	RuleSortDao rulesortdao=new RuleSortDao();
	@Override
	public boolean createRuleSort(RuleSort rulesort){
		// TODO Auto-generated method stub
		return rulesortdao.createRuleSort(rulesort);
	}
	@Override
	public boolean deleteRuleSort(RuleSort rulesort){
		// TODO Auto-generated method stub
		return rulesortdao.deleteRuleSort(rulesort);
	}
	@Override
	public boolean updateRuleSort(RuleSort rulesort){
		// TODO Auto-generated method stub
		return rulesortdao.updateRuleSort(rulesort);
	}
	@Override
	public Pager findAllRuleSortP(Pager page){
		// TODO Auto-generated method stub
		return rulesortdao.findAllRuleSortP(page);
	}
	@Override
	public boolean updateRuleSortNum(RuleSort rulesort){
		// TODO Auto-generated method stub
		return rulesortdao.updateRuleSortNum(rulesort);
	}
	@Override
	public RuleSort findRuleSortById(RuleSort rulesort){
		return rulesortdao.findRuleSortById(rulesort);
	}
	@Override
	public boolean sortName(String name){
		return rulesortdao.sortName(name);
	}
	@Override
	public int getSortCount(){
		return rulesortdao.getSortCount();
	}
	public List<RuleSort> findAllRuleSort(){
		return rulesortdao.findAllRuleSort();
	}
}
