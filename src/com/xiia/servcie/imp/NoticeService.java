package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.CheckDao;
import com.xiia.dao.NoticeDao;
import com.xiia.dao.NoticeSortDao;
import com.xiia.service.inter.INotice;
import com.xiia.util.Pager;
import com.xiia.vo.Check;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;

/**
 * 
 * @author zhouhao 2014-11-4 10:30
 * Notice的业务实现类
 */
public class NoticeService implements INotice{

	private NoticeDao noticeDao;
	
	public NoticeService(){
		noticeDao = new NoticeDao();
	}
	
	@Override
	public Pager listPublishedNotice(NoticeSort noticeSort, Pager page) {
		// TODO Auto-generated method stub
		return noticeDao.listPublishedNotice(noticeSort, page);
	}

	@Override
	public List<Notice> listPublishedNotice(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		return noticeDao.listPublishedNotice(noticeSort);
	}
	
	@Override
	public List<Notice> listAllNotice(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		if (noticeSort.getSortId() != 0){  //传入的公告分类有效，返回该分类下的所有公告
			return noticeDao.listNoticeBySortId(noticeSort);
		}
		else{   //传入的公告分类无效，返回所有的公告
			return noticeDao.listAllNotice();
		}
	}
	
	@Override
	public Notice showNotice(Notice notice) {
		// TODO Auto-generated method stub
		return noticeDao.showNoticeById(notice.getNoticeId());
	}

	@Override
	public boolean addNotice(Notice notice) {
		// TODO Auto-generated method stub
		return noticeDao.addNotice(notice);  //添加记录
		
	}

	@Override
	public int getNoticeSortNum() {
		// TODO Auto-generated method stub
		return noticeDao.getNoticeSortNum();
	}

	@Override
	public boolean deleteNotice(Notice notice) {
		// TODO Auto-generated method stub
		return noticeDao.deleteNotice(notice);  //删除记录
	}

	@Override
	public boolean updateNotice(Notice notice) {
		// TODO Auto-generated method stub
		return noticeDao.updateNotice(notice);
	}
	
	@Override
	public Notice getNoticeById(int noticeId) {
		// TODO Auto-generated method stub
		return noticeDao.showNoticeById(noticeId);
	}

	@Override
	public Notice getTopNotice(NoticeSort noticeSort){
		List<Notice> notices = noticeDao.listPublishedNotice(noticeSort);
		if (notices.size() != 0){
			return notices.get(0);
		}else{
			return null;
		}
	}

	@Override
	public int getFinishedNoticeNum() {
		// TODO Auto-generated method stub
		return noticeDao.getFinishedNoticeNum();
	}

	@Override
	public int getUnPassNoticeNum() {
		// TODO Auto-generated method stub
		return noticeDao.getUnPassNoticeNum();
	}

	@Override
	public int getRevokedNoticeNum() {
		// TODO Auto-generated method stub
		return noticeDao.getRevokedNoticeNum();
	}

	@Override
	public Pager getFinishedNotice(Pager page) {
		// TODO Auto-generated method stub
		return noticeDao.getFinishedNotice(page);
	}
	
	@Override
	public Pager getUnPassNotice(Pager page) {
		// TODO Auto-generated method stub
		return noticeDao.getUnPassNotice(page);
	}
	
	@Override
	public Pager getRevokedNotice(Pager page) {
		// TODO Auto-generated method stub
		return noticeDao.getRevokedNotice(page);
	}

	@Override
	public int getEditableNoticeNum() {
		// TODO Auto-generated method stub
		return noticeDao.getEditableNoticeNum();
	}

	@Override
	public int getEditableNoticeNum(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		return noticeDao.getEditableNoticeNum(noticeSort);
	}
	
	@Override
	public Pager getEditableNotice(Pager page) {
		// TODO Auto-generated method stub
		return noticeDao.getEditableNotice(page);
	}

	@Override
	public Pager getEditableNotice(NoticeSort noticeSort, Pager page) {
		// TODO Auto-generated method stub
		return noticeDao.getEditableNotice(noticeSort, page);
	}
	
	@Override
	public boolean sendCheck(Notice notice, Check check){
		if (noticeDao.isFirstSendCheck(check)){
			return noticeDao.firstSendCheck(notice, check);
		}else{
			return noticeDao.secondSendCheck(notice, check);
		}
	}

	@Override
	public List<Notice> listAllPublishedNotice() {
		// TODO Auto-generated method stub
		return noticeDao.listAllPublishedNotice();
	}
	
	@Override
	public boolean isExistTitle(String title){
		return noticeDao.isExistTitle(title);
	}
}
