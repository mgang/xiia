package com.xiia.servcie.imp;

import com.xiia.dao.ResourceDao;
import com.xiia.service.inter.IResource;
import com.xiia.util.Pager;
import com.xiia.vo.Resource;
/**
 * @author	王书恒--2014-11-4
 * ResourceService的实现
 */

public class ResourceService implements IResource {
	private static ResourceDao resourceDao;
	public ResourceService() {
		// TODO Auto-generated constructor stub
		resourceDao = new ResourceDao();
	}
	@Override
	public boolean addResource(Resource resource) {
		return resourceDao.addResource(resource);
	}

	@Override
	public boolean deleteResource(Resource resource) {
		return resourceDao.deleteResource(resource);
	}

	@Override
	public boolean updateResource(Resource resource) {
		return resourceDao.update(resource);
		
	}

	@Override
	public Resource getResourceById(int resId) {
		// TODO Auto-generated method stub
		return resourceDao.getResourceById(resId);
	}

	@Override
	public int countResource() {
		// TODO Auto-generated method stub
		return resourceDao.countResource();
	}

	@Override
	public Pager findResourcePage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		return resourceDao.findResourcePage(page,orderBy);
	}
	@Override
	public boolean askCheck(Resource resource) {
		// TODO Auto-generated method stub
		return resourceDao.askCheck(resource);
	}
	
	
}
