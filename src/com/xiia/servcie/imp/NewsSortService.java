package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.NewsSortDao;
import com.xiia.service.inter.INewsSort;
import com.xiia.util.Pager;
import com.xiia.vo.NewsSort;
/**
 * 2014 11 4 21:27
 * @author 陈强
 *新闻分类业务实体类
 */
public class NewsSortService implements INewsSort {
	NewsSortDao newsSortDao = new NewsSortDao();
	@Override
	public boolean createNewSort(NewsSort newsSort) {
		// TODO Auto-generated method stub
		return newsSortDao.createNewSort(newsSort);
	}

	@Override
	public boolean deleteSort(NewsSort newsSort) {
		// TODO Auto-generated method stub
		return newsSortDao.deleteSort(newsSort);
	}

	@Override
	public boolean updateSort(NewsSort newsSort) {
		// TODO Auto-generated method stub
		return newsSortDao.updateSort(newsSort);
	}

	@Override
	public List<NewsSort> findAllSort() {
		// TODO Auto-generated method stub
		return newsSortDao.findAllSort();
	}
	public boolean updateSortNewsNum(NewsSort newsSort){
		return newsSortDao.updateSortNewsNum(newsSort);
	}
	public boolean isExistSortName(String sortName){
		return newsSortDao.isExistSortName(sortName);
	}
	public NewsSort findNewsSortById(int sortId){
		return newsSortDao.findNewsSortById(sortId);
	}
	public Pager findNewsSortPage(Pager page ){
		return newsSortDao.findNewsSortPage(page);
	}
	public int getCount(){
		return newsSortDao.getCount();
	}
}
