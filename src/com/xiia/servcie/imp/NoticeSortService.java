package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.NoticeDao;
import com.xiia.dao.NoticeSortDao;
import com.xiia.service.inter.INoticeSort;
import com.xiia.util.Pager;
import com.xiia.vo.NoticeSort;
/**
 * 
 * @author zhouhao 2014-11-4 10:28
 * NoticeSort的业务实现类
 */
public class NoticeSortService implements INoticeSort {
	
	private NoticeSortDao noticeSortDao;
	private NoticeDao noticeDao;
	
	public NoticeSortService() {
		noticeSortDao = new NoticeSortDao();
		noticeDao = new NoticeDao();
	}

	@Override
	public List<NoticeSort> listNoticeSort() {
		// TODO Auto-generated method stub
		return noticeSortDao.listNoticeSort();
	}

	@Override
	public Pager listNoticeSort(Pager page) {
		// TODO Auto-generated method stub
		return noticeSortDao.listNoticeSort(page);
	}

	@Override
	public boolean addNoticeSort(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		return noticeSortDao.addNoticeSort(noticeSort);
	}

	@Override
	public boolean updateNoticeSort(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		return noticeSortDao.updateNoticeSort(noticeSort);
	}

	@Override
	public boolean deleteNoticeSort(NoticeSort noticeSort) {
		// TODO Auto-generated method stub	
		return noticeSortDao.deleteNoticeSort(noticeSort);
	}

	@Override
	public NoticeSort getNoticeSortById(int sortId) {
		// TODO Auto-generated method stub
		return noticeSortDao.showNoticeSortById(sortId);
	}
	
	@Override
	public List<NoticeSort> getTopXNoticeSort(int x){
		List<NoticeSort> noticeSorts = noticeSortDao.listNoticeSort();
		List<NoticeSort> topXSorts = noticeSorts.subList(0, x); //前x个公告分类
		if (topXSorts.isEmpty()){
			System.out.println("获取前" + x + "个公告分类出错");
			return null;
		}
		return topXSorts;
	}

	@Override
	public int getPublishedNoticeNum(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		if (noticeSort.getSortId() == 0){
			return noticeDao.getAllPublishedNoticeNum();
		}
		else{
			return noticeDao.getPublishedNoticeNum(noticeSort);
		}
	}
	@Override
	public boolean isExistSortName(String sortName){
		return noticeSortDao.isExistSortName(sortName);
	}
}
