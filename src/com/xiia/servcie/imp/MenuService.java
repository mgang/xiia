package com.xiia.servcie.imp;

import java.util.List;
import java.util.Map;

import com.xiia.dao.MenuDao;
import com.xiia.service.inter.IMenu;
import com.xiia.util.Pager;
import com.xiia.vo.Menu;
import com.xiia.vo.User;

/**
 * 2014 11/3 18:31
 * @author 李跃磊
 *
 */
public class MenuService implements IMenu{

	private MenuDao menuDao = new MenuDao();

	@Override
	public List<Menu> selectMenu(int status) {
		List<Menu> list = menuDao.select(status);
		if(list != null){
			return list;
		}
		return null;
	}

	@Override
	public List<Menu> selectMenu() {
		if(menuDao.selectAll() != null){
			return menuDao.selectAll();
		}
		return null;
	}
	
	@Override
	public Map<String, List<Menu>> selectMenuByOrder() {
		Map<String,List<Menu>> map = menuDao.selectByDisOrder();
		if(map != null){
			return map;
		}
		return null;
	}
	
	@Override
	public List<Menu> selectByPreMenuId(Menu menu) {
		if(menuDao.selectByPreMenuId(menu).isEmpty()){
			return null;
		}
		return menuDao.selectByPreMenuId(menu);
	}
	
	@Override
	public List<Menu> selectMenu(User user) {
		List<Menu> menus = menuDao.selectMenuByUser(user);
		if(menus != null){
			return menus;
		}
		return null;
	}
	
	@Override
	public Menu addMenu(Menu menu) {
		if(menuDao.add(menu) != null){
			return menu; 
		}
		return null;
	}
	
	@Override
	public boolean deleteMenu(List<Menu> menus) {
		if(menuDao.delete(menus) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateMenu(List<Menu> menus) {
		if(menuDao.update(menus) != null){
			return true;
		}
		return false;
	}

	@Override
	public boolean showOrUndoMenu(List<Menu> menus) {
		if(menuDao.updateStatusToShowOrUndo(menus) != null){
			return true;
		}
		return false;
	}

	@Override
	public List<Menu> selectMenuByRankAndStatus(int Rank, int status) {
		List<Menu> menus = menuDao.selectMenuByRankAndStatus(Rank, status);
		if( menus != null){
			return menus;
		}
		return null;		
	}

	@Override
	public boolean updateMenuDisplayOrder(List<Menu> menus) {
		if(menuDao.updatedisplayOrder(menus) != null){
			return true;
		}
		return false;
	}

	@Override
	public List<Menu> selectMenu(int i, User user) {
		List<Menu> menus = menuDao.selectMenuByUserAndStatus(i,user);
		if(menus != null){
			return menus;
		}
		return null;
	}

	@Override
	public boolean applyCheckMenu(List<Menu> menus) {
		List<Menu> menu = menuDao.updateStatus(menus);
		if(menu != null){
			return true;
		}
		return false;
	}

	@Override
	public int countMenu() {
		return menuDao.getCount();
	}

	@Override
	public Pager findMenuPage(Pager page) {
		return menuDao.selectMenuByPage(page);
	}

	@Override
	public int countMenuByStatus(int status) {
		return menuDao.getCount(status);
	}

	@Override
	public Pager findMenuPage(Pager page, int status) {
		return menuDao.selectMenuByPage(page, status);
	}

	@Override
	public int countCanUpdateMenu() {
		return menuDao.getCountCanUpdateMenu();
	}

	@Override
	public Pager findCanUpdateMenuPage(Pager page) {
		return menuDao.getCanUpdateMenu(page);
	}

}
