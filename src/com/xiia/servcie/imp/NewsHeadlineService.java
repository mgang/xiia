package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.NewsDao;
import com.xiia.dao.NewsHeadlineDao;
import com.xiia.service.inter.INewsHeadline;
import com.xiia.vo.NewsHeadline;
/**
 * 2014 11 4 21:25
 * @author 陈强
 * 新闻头条业务实现类
 */
public class NewsHeadlineService implements INewsHeadline {
	
	NewsHeadlineDao newsHeadlineDao = new NewsHeadlineDao();
	@Override
	public boolean addNewsHeadline(NewsHeadline newsHeadline) {
		// TODO Auto-generated method stub
		return newsHeadlineDao.addNewsHeadline(newsHeadline);
	}
	public NewsHeadline getHeadlineByNewsId(int newsId){
		return newsHeadlineDao.getHeadlineByNewsId(newsId);
	}
	public boolean updateTitle(NewsHeadline headline){
		return newsHeadlineDao.updateTitle(headline);
	}
	public boolean updateUrl(NewsHeadline headline){
		return newsHeadlineDao.updateUrl(headline);
	}
}
