package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.NewsDao;
import com.xiia.service.inter.INews;
import com.xiia.util.Pager;
import com.xiia.vo.Check;
import com.xiia.vo.News;
import com.xiia.vo.NewsSort;
/**
 * 2014 11 4 21:26
 * @author 陈强
 * 新闻业务实现类
 */
public class NewsService implements INews {
	NewsDao newsDao = new NewsDao();
	@Override
	public boolean addNews(News news) {
		// TODO Auto-generated method stub
		return newsDao.addNews(news);
	}
	@Override
	public boolean deleteNews(News news) {
		// TODO Auto-generated method stub
		return newsDao.deleteNews(news);
	}

	@Override
	public boolean updateNewsS(News news,NewsSort oldSort) {
		// TODO Auto-generated method stub
		return newsDao.updateNewsS(news,oldSort);
	}
	public boolean updateNews(News news) {
		// TODO Auto-generated method stub
		return newsDao.updateNews(news);
	}
	@Override
	public List<News> findNewsBySort(NewsSort newsSort) {
		// TODO Auto-generated method stub
		return newsDao.findNewsBySort(newsSort);
	}

	@Override
	public News findNews(int newsId) {
		// TODO Auto-generated method stub
		return newsDao.findNews(newsId);
	}

	@Override
	public boolean releaseNews(News news) {
		// TODO Auto-generated method stub
		return newsDao.releaseNews(news);
	}
	public List<News> getAllNewsByHeadline(){
		return newsDao.getAllNewsByHeadline();
	}
	public boolean updateNewsTopFlag(News news){
		return newsDao.updateNewsTopFlag(news);
	}
	public boolean deleteNewsTopFlag(News news){
		return newsDao.deleteNewsTopFlag(news);
	}
	public boolean updateNewsHeadlineFlag(News news){
		return newsDao.updateNewsHeadlineFlag(news);
	}
	public boolean deleteNewsHeadlineFlag(News news){
		return newsDao.deleteNewsHeadlineFlag(news);
	}
	public Pager findNewsPage(Pager page,NewsSort newsSort ){
		return newsDao.findNewsPage(newsSort,page);
	}
	public int getCount(NewsSort newsSort) {
		return newsDao.getCount(newsSort);
	}
	public boolean addCheck(News news){
		return newsDao.addCheck(news);
	}
	public int getCountNoPass(){
		return newsDao.getCountNoPass();
	}
	public Pager findNewsNoPassPage(Pager page ){
		return newsDao.findNewsNoPassPage(page);
	}
	public boolean updateCheck(News news){
		return newsDao.updateCheck(news);
	}
	public boolean findCheckByNewsId(News news){
		return newsDao.findCheckByNewsId(news);
	}
	public Check findCheckByNewsIdCheck(News news){
		return newsDao.findCheckByNewsIdCheck(news);
	}
	public Pager findReleaseNewsBySort(NewsSort newsSort,Pager page){
		return newsDao.findReleaseNewsBySort(newsSort,page);
	}
	public int getConuntRelease(NewsSort newsSort){
		return newsDao.getConuntRelease(newsSort);
	}
	public Pager findAllNewsPage(Pager page){
		return newsDao.findAllNewsPage(page);
	}
	public int getAllNewsCount(){
		return newsDao.getAllNewsCount();
	}
}
