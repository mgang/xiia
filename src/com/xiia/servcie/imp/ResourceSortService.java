package com.xiia.servcie.imp;

import java.util.List;

import com.xiia.dao.ResourceSortDao;
import com.xiia.service.inter.IResourceSort;
import com.xiia.util.Pager;
import com.xiia.vo.ResourceSort;
/**
 * @author--王书恒--2014-11-4
 * ResourceSortService的实现
 */

public class ResourceSortService implements IResourceSort {
	private static ResourceSortDao resourceSortDao;
	
	public ResourceSortService() {
		// TODO Auto-generated constructor stub
		resourceSortDao = new ResourceSortDao();
	}

	@Override
	public boolean addResourceSort(ResourceSort resourceSort) {
		if(resourceSortDao.addResourceSort(resourceSort) > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean deleteResourceSort(ResourceSort resourceSort) {
		return resourceSortDao.deleteResourceSort(resourceSort);	
	}

	@Override
	public boolean updateResourceSort(ResourceSort resourceSort) {
		if(resourceSortDao.update(resourceSort) > 0){
			return true;
		}else{
			return false;
		}
		
	}

	@Override
	public ResourceSort getResourceSortById(int sortId) {
		// TODO Auto-generated method stub
		return resourceSortDao.getResourceSortById(sortId);
	}

	@Override
	public int countResourceSort() {
		// TODO Auto-generated method stub
		return resourceSortDao.countResourceSort();
	}

	@Override
	public Pager findResourceSortPage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		return resourceSortDao.findResourceSortPage(page,orderBy);
	}

	@Override
	public List<ResourceSort> getAllResourceSort() {
		// TODO Auto-generated method stub
		return resourceSortDao.getAllResourceSort();
	}

}
