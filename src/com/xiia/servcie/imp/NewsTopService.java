package com.xiia.servcie.imp;

import com.xiia.dao.NewsDao;
import com.xiia.dao.NewsTopDao;
import com.xiia.service.inter.INewsTop;
import com.xiia.vo.News;
import com.xiia.vo.NewsTop;
/**
 * 2014 11 4 21:17
 * @author 陈强
 * 新闻置顶业务实现类
 */
public class NewsTopService implements INewsTop {
	NewsTopDao newsTopDao = new NewsTopDao();
	NewsDao newsDao = new NewsDao();
	@Override
	public boolean newsTop(NewsTop newsTop) {
		// TODO Auto-generated method stub
		boolean a = newsTopDao.newsTop(newsTop);
		News news = newsTop.getNews();
		boolean b = newsDao.updateNewsTopFlag(news);
		if(a&&b)
			return true;
		else
			return false;
	}

	@Override
	public boolean deleteNewsTop(NewsTop newsTop) {
		// TODO Auto-generated method stub
		boolean a = newsTopDao.deleteNewsTop(newsTop);
		News news = newsTop.getNews();
		boolean b = newsDao.deleteNewsTopFlag(news);
		if(a&&b)
			return true;
		else
			return false;
	}

}
