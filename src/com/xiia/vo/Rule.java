package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author 胡天天  2014-11-4
 * 政策法规实体
 *
 */
public class Rule {
	private int ruleId;
	private String title;
	private String link;
	private Date addTime;
	private int status;
	private int sortId;
	private int userId;
	private User user;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	private RuleSort ruleSort;

	public RuleSort getRuleSort() {
		return ruleSort;
	}
	public void setRuleSort(RuleSort ruleSort) {
		this.ruleSort = ruleSort;
	}
	public int getRuleId() {
		return ruleId;
	}
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
