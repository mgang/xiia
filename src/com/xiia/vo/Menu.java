package com.xiia.vo;

import java.util.Date;
import java.util.List;

/**
 * 2014 11/3 18:30
 * @author 李跃磊
 *
 */
public class Menu {
	private int menuId;//栏目编号
	private String menuName;//栏目名称
	private String link;//链接
	private Date createTime;//创建时间
	private Date lastUpdateTime;//最后一次修改时间
	private int menuRank;//栏目级别
	private int preMenuId;//上级栏目编号
	private int displayOrder;//显示顺序
	private int status;//是否在前台显示
	private int userId;//创建者编号
	private Menu menu;//上级栏目
	private List<Menu> childMenu;//子栏目（二级菜单）
	
	
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public int getMenuRank() {
		return menuRank;
	}
	public void setMenuRank(int menuRank) {
		this.menuRank = menuRank;
	}
	public int getPreMenuId() {
		return preMenuId;
	}
	public void setPreMenuId(int preMenuId) {
		this.preMenuId = preMenuId;
	}
	public int getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<Menu> getChildMenu() {
		return childMenu;
	}
	public void setChildMenu(List<Menu> childMenu) {
		this.childMenu = childMenu;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	@Override
	public String toString() {
		return "Menu [menuId=" + menuId + ", menuName=" + menuName + ", link="
				+ link + ", createTime=" + createTime + ", updateTime="
				+ lastUpdateTime + ", menuRank=" + menuRank + ", preMenuId="
				+ preMenuId + ", displayOrder=" + displayOrder + ", status="
				+ status + ", userId=" + userId + "]";
	}
}
