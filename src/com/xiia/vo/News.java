package com.xiia.vo;

import java.util.Date;


/**
 * 2014-11-4 15:15
 * @author 陈强
 *新闻实体类
 */
public class News {
	private int newsId;
	private String title;
	private String content;
	private Date addTime;
	private Date lastUpdateTime;
	private int status;
	private NewsSort newsSort;
	private int sortId;
	private int topFlag;
	private int headlineFlag;
	private String mes;
	private int userId;
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public int getHeadlineFlag() {
		return headlineFlag;
	}
	public void setHeadlineFlag(int headlineFlag) {
		this.headlineFlag = headlineFlag;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getTopFlag() {
		return topFlag;
	}
	public void setTopFlag(int topFlag) {
		this.topFlag = topFlag;
	}
	public NewsSort getNewsSort() {
		return newsSort;
	}
	public void setNewsSort(NewsSort newsSort) {
		this.newsSort = newsSort;
	}
	public int getNewsId() {
		return newsId;
	}
	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setlastUpdateTime(Date updateTime) {
		this.lastUpdateTime = updateTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
