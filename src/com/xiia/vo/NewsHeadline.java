package com.xiia.vo;

import java.util.Date;


/**
 * 2014-11-4 15:15
 * @author 陈强
 *新闻头条实体类
 */
public class NewsHeadline {
	private int headlineId;
	private String headTitle;
	private String headNote;
	public String getHeadTitle() {
		return headTitle;
	}
	public void setHeadTitle(String headTitle) {
		this.headTitle = headTitle;
	}
	public String getHeadNote() {
		return headNote;
	}
	public void setHeadNote(String headNote) {
		this.headNote = headNote;
	}
	private Date addTime;
	private String headUrl;
	private Date lastUpdateTime;
	private News news;
	private User user;
	public int getHeadlineId() {
		return headlineId;
	}
	public void setHeadlineId(int headlineId) {
		this.headlineId = headlineId;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public String getHeadUrl() {
		return headUrl;
	}
	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public News getNews() {
		return news;
	}
	public void setNews(News news) {
		this.news = news;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
