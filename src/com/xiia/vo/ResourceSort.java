package com.xiia.vo;

import java.util.Date;

/**
 * @author--王书恒--2014-11-3
 * 资源分类
 */
public class ResourceSort {
	
	private int sortId;
	private String sortName;
	private int resNum;	//该分类下的资源数
	private Date addTime;
	private int userId;
	private User user;
	
		
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public int getResNum() {
		return resNum;
	}
	public void setResNum(int resNum) {
		this.resNum = resNum;
	}
	
	
	
}
