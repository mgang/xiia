package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author meigang 2014-11-10
 * 发布实体
 */
public class Publish {
	/**
	 * 
	FieldTypeComment
	publishIdint(11) NOT NULL
	publishedIdint(11) NULL
	typevarchar(16) NULL
	lastPublishTimedatetime NULL
	userIdint(11) NULL
	 */
	private int publishId;
	private int publishedId;
	private String type;
	private int flag;
	private Date lastPublishTime;
	private int userId;
	private User user;
	private Object obj;

	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public int getPublishId() {
		return publishId;
	}
	public void setPublishId(int publishId) {
		this.publishId = publishId;
	}
	public int getPublishedId() {
		return publishedId;
	}
	public void setPublishedId(int publishedId) {
		this.publishedId = publishedId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getLastPublishTime() {
		return lastPublishTime;
	}
	public void setLastPublishTime(Date lastPublishTime) {
		this.lastPublishTime = lastPublishTime;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
