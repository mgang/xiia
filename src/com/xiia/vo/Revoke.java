package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author meigang 2014-11-10 17:07
 * 撤销实体
 */
public class Revoke {
	/**
	 * 
	FieldTypeComment
	revokeIdint(11) NOT NULL
	revokedIdint(11) NULL
	typevarchar(16) NULL
	lastRevokeTimedatetime NULL
	userIdint(11) NULL
	 */
	private int revokeId;
	private int revokedId;
	private String type;
	private int flag;
	private Date lastRevokeTime;
	private int userId;
	private User user;
	private Object obj;
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public int getRevokeId() {
		return revokeId;
	}
	public void setRevokeId(int revokeId) {
		this.revokeId = revokeId;
	}
	public int getRevokedId() {
		return revokedId;
	}
	public void setRevokedId(int revokedId) {
		this.revokedId = revokedId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getLastRevokeTime() {
		return lastRevokeTime;
	}
	public void setLastRevokeTime(Date lastRevokeTime) {
		this.lastRevokeTime = lastRevokeTime;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
