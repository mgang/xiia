package com.xiia.vo;

import java.util.Date;

/**
 *@author --王书恒--2014-11-3
 * 资源
 */
public class Resource {
	
	
	private int resourceId;
	
	
	private ResourceSort resourceSort;
	private User user;
	
	private String fileName;		//文件名
	private double fileSize;		//文件大小，以KB为单位
	private int downloadCount;		//下载次数
	private String filePath;		//对应服务器路径
	private Date addTime;
	
	private int resScore;		//资源所需积分
	/**
	 * 
		资源状态：
		1.待审核
		2.审核通过
		3.审核不通过
	 */
	private int status;
	private int sortId;
	private int userId;
	
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	public int getResourceId() {
		return resourceId;
	}
	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}
	public ResourceSort getResourceSort() {
		return resourceSort;
	}
	public void setResourceSort(ResourceSort resourceSort) {
		this.resourceSort = resourceSort;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public double getFileSize() {
		return fileSize;
	}
	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}
	public int getDownloadCount() {
		return downloadCount;
	}
	public void setDownloadCount(int downloadCount) {
		this.downloadCount = downloadCount;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public int getResScore() {
		return resScore;
	}
	public void setResScore(int resScore) {
		this.resScore = resScore;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
}
