package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author 胡天天  2014-11-4
 *
 */
public class Org {
	private int orgId;
	private String orgName;
	/**
	 * 机构连接
	 */
	private String link;
	private Date addTime;
	private Date LastUpdateTime;
	private int status;
	/**
	 * 机构的添加者id
	 */
	private int userId;
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getOrgId() {
		return orgId;
	}
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getLastUpdateTime() {
		return LastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		LastUpdateTime = lastUpdateTime;
	}
}
