package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author zhouhao 2014-11-4 9:46
 *
 */
public class NoticeSort {
	/**
	 * 
	 FieldTypeComment
	 sortId int(11) NOT NULL
  	 sortName varchar(32) NULL
     noticeNum int(11) NULL
     note varchar(256) NULL
 	 createTime datetime NULL
     lastUpdateTime datetime NULL
     userId int(11) NULL
	 */
	private int sortId;
	private String sortName;
	/**
	 * noticeNum
	 * 公告分类所拥有的公告数量
	 */
	private int noticeNum;
	/**
	 * note
	 * 描述信息
	 */
	private String note;
	/**
	 * createTime
	 * 公告分类创建时间
	 */
	private Date createTime;
	/**
	 * lastUpdateTime
	 * 最后修改时间
	 */
	private Date lastUpdateTime;
	/**
	 * userId
	 * 创建者id
	 */
	private int userId;
	
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public int getNoticeNum() {
		return noticeNum;
	}
	public void setNoticeNum(int noticeNum) {
		this.noticeNum = noticeNum;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
}
