package com.xiia.vo;
/**
 * @author--王书恒--2014-11-3
 * 系统参数
 */
public class Setting {

	private int settingId;		
	
	private int headlineNewsNum;
	private int menuMaxCount;
	private int menmberScore;
	private int adminScore;
	private int uploadResourceSize;
	private String acceptUploadFileType;
	public int getSettingId() {
		return settingId;
	}
	public void setSettingId(int settingId) {
		this.settingId = settingId;
	}
	public int getHeadlineNewsNum() {
		return headlineNewsNum;
	}
	public void setHeadlineNewsNum(int headlineNewsNum) {
		this.headlineNewsNum = headlineNewsNum;
	}
	public int getMenuMaxCount() {
		return menuMaxCount;
	}
	public void setMenuMaxCount(int menuMaxCount) {
		this.menuMaxCount = menuMaxCount;
	}
	public int getMenmberScore() {
		return menmberScore;
	}
	public void setMenmberScore(int menmberScore) {
		this.menmberScore = menmberScore;
	}
	public int getAdminScore() {
		return adminScore;
	}
	public void setAdminScore(int adminScore) {
		this.adminScore = adminScore;
	}
	public int getUploadResourceSize() {
		return uploadResourceSize;
	}
	public void setUploadResourceSize(int uploadReourceSize) {
		this.uploadResourceSize = uploadReourceSize;
	}
	public String getAcceptUploadFileType() {
		return acceptUploadFileType;
	}
	public void setAcceptUploadFileType(String acceptUploadFileType) {
		this.acceptUploadFileType = acceptUploadFileType;
	}
	
	
	
	
	
	
}
