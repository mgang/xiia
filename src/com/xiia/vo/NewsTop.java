package com.xiia.vo;

import java.util.Date;


/**
 * 2014-11-4 15:15
 * @author 陈强
 *新闻置顶实体类
 */
public class NewsTop {
	private int newsTopId;
	private Date topTime;
	private News news;
	private User user;
	public int getNewsTopId() {
		return newsTopId;
	}
	public void setNewsTopId(int newsTopId) {
		this.newsTopId = newsTopId;
	}
	public Date getTopTime() {
		return topTime;
	}
	public void setTopTime(Date topTime) {
		this.topTime = topTime;
	}
	public News getNews() {
		return news;
	}
	public void setNews(News news) {
		this.news = news;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
