package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author meigang 2014-11-10 17:02
 * 审核实体
 */
public class Check {
	/**
	 * 
	FieldTypeComment
	checkIdint(11) NOT NULL
	checkedIdint(11) NULL
	typevarchar(16) NULL
	msgvarchar(255) NULL
	lastCheckTimedatetime NULL
	userIdint(11) NULL
	 */
	private int checkId;
	private int checkedId;
	private String type;
	private String msg;
	private int flag;
	private Date lastCheckTime;
	private int userId;
	private User user;
	/**
	 * 存放审核的对象
	 */
	private Object obj;
	
	
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public int getCheckId() {
		return checkId;
	}
	public void setCheckId(int checkId) {
		this.checkId = checkId;
	}
	public int getCheckedId() {
		return checkedId;
	}
	public void setCheckedId(int checkedId) {
		this.checkedId = checkedId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Date getLastCheckTime() {
		return lastCheckTime;
	}
	public void setLastCheckTime(Date lastCheckTime) {
		this.lastCheckTime = lastCheckTime;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
