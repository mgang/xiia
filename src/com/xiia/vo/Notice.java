package com.xiia.vo;

import java.util.Date;

import java.lang.Comparable;
/**
 * 
 * @author zhouhao 2014-11-4 10:11
 *
 */
public class Notice implements Comparable{
	/**
	 * FileTypeComment
	 noticeId int(11) NOT NULL
     title varchar(32) NULL
     content text
     addTime datetime NULL
     lastUpdateTime datetime NULL
     status tinyint(4) NULL
     sortId int(11) NULL
     userId int(11) NULL
	 */
	private int noticeId;
	/**
	 * title
	 * 公告标题
	 */
	private String title;
	/**
	 * content
	 * 公告内容
	 */
	private String content;
	/**
	 * addTime
	 * 公告录入时间
	 */
	private Date addTime;
	/**
	 * lastUpdateTime
	 * 最后修改时间
	 */
	private Date lastUpdateTime;
	/**
	 * status
	 * 状态（撰写完成，待审核，审核通过，审核不通过，已发布）
	 */
	private int status;
	/**
	 * sortId
	 * 所属公告分类id
	 */
	private int sortId;
	/**
	 * userId
	 * 创建者id
	 */
	
	private int userId;
	public int getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * 根据创建时间降序、最后更改时间降序、分组编号升序、编号升序、标题升序、状态升序（1:撰写完成4:审核不通过6:已撤销）、用户编号升序
	 */
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Notice obj = (Notice)o;
		if (addTime.compareTo(obj.getAddTime()) == 0){
			if (lastUpdateTime.compareTo(obj.getLastUpdateTime()) == 0){
				if (sortId == obj.getSortId()){
					if (noticeId == obj.getNoticeId()){
						if (title.equals(obj.getTitle())){
							if (status == obj.getStatus()){
								return new Integer(userId).compareTo(obj.getUserId());
								//按操作者编号升序
							}else{
								return new Integer(status).compareTo(obj.getStatus());
								//按状态标识升序排列
							}
						}else{
							return title.compareTo(obj.getTitle());//按标题升序
						}
					}else{
						return new Integer(noticeId).compareTo(obj.getNoticeId());//按编号升序排列
					}
				}else{
					return new Integer(sortId).compareTo(obj.getSortId());//按分组编号升序排列
				}
			}else{
				return obj.getLastUpdateTime().compareTo(lastUpdateTime);  //按更新时间降序排列
			}
		}else{
			return obj.getAddTime().compareTo(addTime);  //按加入时间降序排列
		}
			
	}
	public String toString(){
		return "["+noticeId+","+title+","+addTime+","+status+"]";
	}
}
