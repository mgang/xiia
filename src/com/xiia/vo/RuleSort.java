package com.xiia.vo;

import java.util.Date;

/**
 * 
 * @author 胡天天  2014-11-4
 * 法规分类实体
 *
 */
public class RuleSort {
	private int sortId;
	private String sortName;
	private int ruleNum;
	private Date createTime;
	private Date lastUpdateTime;
	/**
	 * 分类的描述信息
	 */
	private String note;
	private User user;
	private int userId;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public int getRuleNum() {
		return ruleNum;
	}
	public void setRuleNum(int ruleNum) {
		this.ruleNum = ruleNum;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
}	
	