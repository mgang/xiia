package com.xiia.web.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NewsService;
import com.xiia.servcie.imp.NewsSortService;
import com.xiia.service.inter.INews;
import com.xiia.service.inter.INewsSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.News;
import com.xiia.vo.NewsSort;

/**
 * 陈强前台新闻servlet
 * @author Administrator
 *
 */
@WebServlet("/newsSort.htm")
public class FNewsSortServlet extends FrameworkServlet{
	private static final long serialVersionUID = 1L;
	private static INewsSort iNewsSort = new NewsSortService();
	private static INews iNews = new NewsService();
	private static Pager page = new Pager();
	public FNewsSortServlet(){
		iNewsSort = new NewsSortService();
		iNews = new NewsService();
		page = new Pager();
	}
	/**
	 * 拿到所有新闻分类的集合并封装到Pager
	 * @param req
	 * @param res
	 * @return String
	 */
	public String newsSortListFclient(HttpServletRequest req,
			HttpServletResponse res){
		List<NewsSort> newsSortList = new ArrayList<NewsSort>();
		newsSortList = iNewsSort.findAllSort();
		req.setAttribute("newsSortList", newsSortList);
		if(null==req.getParameter("clickSort")){
			NewsSort firstSort = new NewsSort();
			if(newsSortList.size()!=0){
				firstSort = newsSortList.get(0);
				//分页
				int currentPage=1;
				if(req.getParameter("currentPage")!=null){
					currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
				}
				//得到News的总行数
				int totalCount=iNews.getConuntRelease(firstSort);
				page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
				page =iNews.findReleaseNewsBySort(firstSort, page);
				req.setAttribute("firstSort", firstSort);
				req.setAttribute("page", page);
			}
			else{
				req.setAttribute("firstSort", "123");
				req.setAttribute("page","123");
			}
		}
		else{
			NewsSort clickSort = new NewsSort();
			int clickSortId = Integer.parseInt(req.getParameter("clickSort"));
			clickSort = iNewsSort.findNewsSortById(clickSortId);
			req.setAttribute("firstSort", clickSort);
			//分页
			int currentPage=1;
			if(req.getParameter("currentPage")!=null){
				currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
			}
			//得到News的总行数
			int totalCount=iNews.getConuntRelease(clickSort);
			page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
			page =iNews.findReleaseNewsBySort(clickSort, page);
			req.setAttribute("firstSort", clickSort);
			req.setAttribute("page", page);
		}
		if(null!=req.getParameter("sortId")){
			int sortId = Integer.parseInt(req.getParameter("sortId"));
			NewsSort sort = iNewsSort.findNewsSortById(sortId);
			req.setAttribute("sort", sort);
		}
		if(null!=req.getParameter("clickNews")){
			int newsId = Integer.parseInt(req.getParameter("clickNews"));
			News news = iNews.findNews(newsId) ;
			req.setAttribute("news", news);
		}
		return "fclient/news/newsMain.jsp";
	}
	/**
	 * 拿到新闻内容并输出到前台页面
	 * @param req
	 * @param res
	 * @return
	 */
	public String newsSortListContent(HttpServletRequest req,
			HttpServletResponse res){
		List<NewsSort> newsSortList = new ArrayList<NewsSort>();
		newsSortList = iNewsSort.findAllSort();
		req.setAttribute("newsSortList", newsSortList);
		if(null==req.getParameter("clickSort")){
			NewsSort firstSort = new NewsSort();
			if(newsSortList.size()!=0){
				firstSort = newsSortList.get(0);
				//分页
				int currentPage=1;
				if(req.getParameter("currentPage")!=null){
					currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
				}
				//得到News的总行数
				int totalCount=iNews.getCountNoPass();
				page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
				page =iNews.findNewsNoPassPage(page);
				List<News> newsList = page.getList();
				req.setAttribute("firstSort", firstSort);
				req.setAttribute("page", page);
			}
			else{
				req.setAttribute("firstSort", "123");
				req.setAttribute("page","123");
			}
		}
		else{
			NewsSort clickSort = new NewsSort();
			int clickSortId = Integer.parseInt(req.getParameter("clickSort"));
			clickSort = iNewsSort.findNewsSortById(clickSortId);
			req.setAttribute("firstSort", clickSort);
			//分页
			int currentPage=1;
			if(req.getParameter("currentPage")!=null){
				currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
			}
			//得到News的总行数
			int totalCount=iNews.getConuntRelease(clickSort);
			page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
			page =iNews.findReleaseNewsBySort(clickSort, page);
			req.setAttribute("firstSort", clickSort);
			req.setAttribute("page", page);
		}
		if(null!=req.getParameter("sortId")){
			int sortId = Integer.parseInt(req.getParameter("sortId"));
			NewsSort sort = iNewsSort.findNewsSortById(sortId);
			req.setAttribute("sort", sort);
		}
		if(null!=req.getParameter("clickNews")){
			int newsId = Integer.parseInt(req.getParameter("clickNews"));
			News news = iNews.findNews(newsId) ;
			req.setAttribute("news", news);
		}
		return "fclient/news/news.jsp";
	}
}
