package com.xiia.web.servlet;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NoticeService;
import com.xiia.servcie.imp.NoticeSortService;
import com.xiia.service.inter.INotice;
import com.xiia.service.inter.INoticeSort;
import com.xiia.util.Pager;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;
/**
 * 
 * @author zhouhao 2014-11-4 10:54
 * NoticeSort的前台servlet
 */
@WebServlet("/noticeSort.htm")
public class FNoticeSortServlet extends FrameworkServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private INoticeSort iNoticeSort;
	private INotice iNotice;
	private Pager page;
	
	public FNoticeSortServlet(){
		iNoticeSort = new NoticeSortService();
		iNotice = new NoticeService();
		page = new Pager();
	}
	/**
	 * 展示所有的公告分类列表
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String showNoticeSortList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<NoticeSort> noticeSorts = iNoticeSort.listNoticeSort();
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		if (noticeSorts != null){
			//至少存在一个公告分类
			int totalCount = iNoticeSort.getPublishedNoticeNum(noticeSorts.get(0));
			page.paging(currentPage, 5, totalCount);
			page = iNotice.listPublishedNotice(noticeSorts.get(0), page);
			request.setAttribute("noticeSorts", noticeSorts);
			request.setAttribute("noticeSort", noticeSorts.get(0));
		}
		//同时显示第一个公告分类里的所有公告列表 
		request.setAttribute("page", page);	
		return "fclient/notice/noticeMain.jsp";
	}
}
