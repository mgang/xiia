package com.xiia.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.MenuService;
import com.xiia.servcie.imp.NoticeService;
import com.xiia.servcie.imp.NoticeSortService;
import com.xiia.service.inter.IMenu;
import com.xiia.service.inter.INotice;
import com.xiia.service.inter.INoticeSort;
import com.xiia.vo.Menu;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;

/**
 * Servlet implementation class FMenuServlet
 */
@WebServlet("/menu.htm")
public class FMenuServlet extends FrameworkServlet {
	private static final long serialVersionUID = 1L;
	private IMenu menuService = new MenuService();   

	/**
	 * 根据状态查询栏目（注：适用前台查询已发布的栏目）
	 * @param request
	 * @param response
	 * @return 栏目对象集合
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public String searchShowForntMenu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		showIndexNotice(request, response);  //展示首页公告
		Map<String, List<Menu>> map = menuService.selectMenuByOrder();
		List<Menu> fatherMenu = new ArrayList<Menu>();
		if(map == null){
			request.setAttribute("fatherMenu",null);
			return "fclient/findex.jsp";
		}
		fatherMenu = map.get("fatherMenu");
		List<Menu> fatherMenu1 = new ArrayList<Menu>();
		for(Menu menu: fatherMenu){
			menu.setChildMenu(map.get(menu.getMenuName()));
			fatherMenu1.add(menu);
		}
		request.setAttribute("fatherMenu",fatherMenu1);
		
		return "fclient/findex.jsp";	
	}
	/**
	 * 展示前台主页的公告链接（只展示前5个公告分类的第一条公告：
	 * 1.若存在5个公告分类但有空的公告分类，只显示前5个非空公告分类中第一条公告；
	 * 2.若不存在5个公告分类，则显示数据库中的前5条公告）
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean showIndexNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		INotice iNotice = new NoticeService();
		INoticeSort iNoticeSort = new NoticeSortService();
		List<Notice> allNotices = iNotice.listAllPublishedNotice();
		/*List<Notice> topXNotices;
		if (allNotices.size() != 0){
			if (allNotices.size() > 5){
				topXNotices = allNotices.subList(0, 5);
			}else{
				topXNotices = allNotices;
			} 
		}else{
			topXNotices = null;
		}*/
		List<Notice> topXNotices = new ArrayList<Notice>();
		if ( allNotices.size() != 0){  //至少存在一条公告
			if( iNotice.getNoticeSortNum() >= 5){
				List<NoticeSort> top5NoticeSorts = iNoticeSort.getTopXNoticeSort(5);
					//获取数据库中前5个公告分类
				Iterator<NoticeSort> it = top5NoticeSorts.iterator();
				while(it.hasNext()){
					Notice notice = iNotice.getTopNotice(it.next());
					if (notice != null){
						topXNotices.add(notice);
					}
				}
				if (topXNotices.size() == 0){
					if (allNotices.size() > 5){
						topXNotices = allNotices.subList(0, 5);
					}else{
						topXNotices = allNotices;
					}
				}
			}else{
				if (allNotices.size() > 5){
					topXNotices = allNotices.subList(0, 5);
				}else{
					topXNotices = allNotices;
				}
			}
		}else{  //没有任何公告
			topXNotices = null;
		}
		request.setAttribute("topXNotices", topXNotices);
		return true;
	}
}
