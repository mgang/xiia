package com.xiia.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NoticeService;
import com.xiia.servcie.imp.NoticeSortService;
import com.xiia.service.inter.INotice;
import com.xiia.service.inter.INoticeSort;
import com.xiia.util.Pager;
import com.xiia.util.StringUtil;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;
/**
 * 
 * @author zhouhao 2014-11-4 10:52
 * Notice的前台servlet
 */
@WebServlet("/notice.htm")
public class FNoticeServlet extends FrameworkServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private INoticeSort iNoticeSort;
	private INotice iNotice;
	private Pager page;
	
	public FNoticeServlet() {
		iNoticeSort = new NoticeSortService();
		iNotice =  new NoticeService();
		page = new Pager();
	}
	/**
	 * 展示公告分类下的所有已发布公告列表
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String showNoticeList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNoticeSort.getPublishedNoticeNum(noticeSort);
		page.paging(currentPage, 5, totalCount);
		page = iNotice.listPublishedNotice(noticeSort, page);
		request.setAttribute("page", page);
		noticeSort = iNoticeSort.getNoticeSortById(noticeSort.getSortId());
		request.setAttribute("noticeSort", noticeSort);
		List<NoticeSort> noticeSorts = iNoticeSort.listNoticeSort();//同时展示公告分类列表
		if (noticeSorts != null){
			request.setAttribute("noticeSorts", noticeSorts);
		}
		return "fclient/notice/noticeList.jsp";
	}
	/**
	 * 展示某个公告的详细内容(已加入上一条与下一条的链接)
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String showNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Notice notice = getNoticeFromRequest(request);
		notice = iNotice.showNotice(notice);
		if (notice != null){
			List<NoticeSort> noticeSorts = iNoticeSort.listNoticeSort();//同时展示公告分类列表
			if (noticeSorts != null){
				request.setAttribute("noticeSorts", noticeSorts);
			}
			request.setAttribute("notice", notice);
			NoticeSort noticeSort = iNoticeSort.getNoticeSortById(notice.getSortId());
				//根据公告id获取所在公告分类
			List<Notice> notices = iNotice.listPublishedNotice(noticeSort); 
			int index = -1;   //获取当前公告在集合对象中的索引
			Iterator<Notice> it = notices.iterator(); 
			while (it.hasNext()){
				index++;
				if (it.next().getNoticeId() == notice.getNoticeId()){
					break;
				}
			}
			Notice preNotice = null;   //前一条公告
			Notice nextNotice = null;  //后一条公告
			if (index - 1 >= 0){//前一条公告存在
				preNotice = notices.get(index - 1);
			}
			if (index + 1 < notices.size()){//后一条公告存在
				nextNotice = notices.get(index + 1);
			}
			request.setAttribute("preNotice", preNotice);
			request.setAttribute("nextNotice", nextNotice);
			return "fclient/notice/notice.jsp";
		}
		return null;
	}
	/**
	 * 展示前台主页的公告链接（只展示前5个公告分类的第一条公告：
	 * 1.若存在5个公告分类但有空的公告分类，只显示前5个非空公告分类中第一条公告；
	 * 2.若不存在5个公告分类，则显示数据库中的前5条公告）
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean showIndexNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Notice> allNotices = iNotice.listAllPublishedNotice();
		List<Notice> topXNotices = new ArrayList<Notice>();
		if ( allNotices != null){  //至少存在一条公告
			if( iNotice.getNoticeSortNum() >= 5){
				List<NoticeSort> top5NoticeSorts = iNoticeSort.getTopXNoticeSort(5);
					//获取数据库中前5个公告分类
				Iterator<NoticeSort> it = top5NoticeSorts.iterator();
				while(it.hasNext()){
					Notice notice = iNotice.getTopNotice(it.next());
					if (notice != null){
						topXNotices.add(notice);
					}
				}
				if (topXNotices.size() == 0){
					if (allNotices.size() > 5){
						topXNotices = allNotices.subList(0, 5);
					}else{
						topXNotices = allNotices;
					}
				}
			}else{
				if (allNotices.size() > 5){
					topXNotices = allNotices.subList(0, 5);
				}else{
					topXNotices = allNotices;
				}
			}
		}else{  //没有任何公告
			topXNotices = null;
		}
		request.setAttribute("topXNotices", topXNotices);
		return true;
	}
	/**
	 * 从http请求中创建NoticeSort对象
	 * @param request
	 * @return 返回NoticeSort对象
	 * @throws ServletException
	 * @throws IOException
	 */
	private NoticeSort getNoticeSortFromRequest(HttpServletRequest request) throws ServletException, IOException {
		NoticeSort noticeSort = new NoticeSort();
		if (request.getParameter("sortId") != null){
			noticeSort.setSortId(Integer.parseInt(request.getParameter("sortId")));
		}if (request.getParameter("sortName") != null){
			noticeSort.setSortName(request.getParameter("sortName").trim());
		}if (request.getParameter("noticeNum") != null){
			noticeSort.setNoticeNum(Integer.parseInt(request.getParameter("noticeNum")));
		}if (request.getParameter("note") != null){
			noticeSort.setNote(request.getParameter("note").trim());
		}if (request.getParameter("createTime") != null){
			noticeSort.setCreateTime(StringUtil.toDate(request.getParameter("createTime")));
		}if (request.getParameter("lastUpdateTime") != null){
			noticeSort.setLastUpdateTime(StringUtil.toDate(request.getParameter("lastUpdateTime").trim()));
		}if (request.getParameter("userId") != null){
			noticeSort.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return noticeSort;
	}
	/**
	 * 从http请求中创建Notice对象
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private Notice getNoticeFromRequest(HttpServletRequest request) throws ServletException, IOException {
		Notice notice = new Notice();
		if (request.getParameter("noticeId") != null){
			notice.setNoticeId(Integer.parseInt(request.getParameter("noticeId")));
		}if (request.getParameter("title") != null){
			notice.setTitle(request.getParameter("title").trim());
		}if (request.getParameter("content") != null){
			notice.setContent(request.getParameter("content").trim());
		}if (request.getParameter("addTime") != null){
			notice.setAddTime(StringUtil.toDate(request.getParameter("addTime")));
		}if (request.getParameter("lastUpdateTime") != null){
			notice.setLastUpdateTime(StringUtil.toDate(request.getParameter("lastUpdateTime").trim()));
		}if (request.getParameter("status") != null){
			notice.setStatus(Integer.parseInt(request.getParameter("status")));
		}if (request.getParameter("sortId") != null){
			notice.setSortId(Integer.parseInt(request.getParameter("sortId")));
		}if (request.getParameter("userId") != null){
			notice.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return notice;
	}
}