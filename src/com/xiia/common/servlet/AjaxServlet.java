package com.xiia.common.servlet;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.servcie.imp.NewsSortService;
import com.xiia.servcie.imp.NoticeService;
import com.xiia.servcie.imp.NoticeSortService;
import com.xiia.servcie.imp.OrgService;
import com.xiia.servcie.imp.RuleService;
import com.xiia.servcie.imp.RuleSortService;
import com.xiia.service.inter.INewsSort;
import com.xiia.service.inter.INotice;
import com.xiia.service.inter.INoticeSort;
import com.xiia.service.inter.IOrg;
import com.xiia.service.inter.IRule;
import com.xiia.service.inter.IRuleSort;
/**
 * 2014 11 5 17:03
 * @author 陈强
 * ajax验证
 */
@SuppressWarnings("serial")
@WebServlet("/ajaxServlet")
public class AjaxServlet extends FrameworkServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private INewsSort iNewsSort;
	private INoticeSort iNoticeSort;
	private INotice iNotice;
	private IOrg iOrg;
	private IRule iRule;
	private IRuleSort iRuleSort;
	public AjaxServlet(){
		iNewsSort = new NewsSortService();
		iNoticeSort = new NoticeSortService();
		iNotice = new NoticeService();
		iOrg = new OrgService();
		iRule = new RuleService();
		iRuleSort = new RuleSortService();
	}
	public String checkSortName(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String sortName = req.getParameter("sortName");
		PrintWriter out =res.getWriter();
		String data = "";
		if(iNewsSort.isExistSortName(sortName))
			data="{\"message\":\"T\"}";
		else
			data="{\"message\":\"F\"}";
		out.print(data);
		out.close();
		return null;
	}
	public String isExistSortName(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String sortName = req.getParameter("sortName");
		PrintWriter out =res.getWriter();
		String data = "";
		if(iNoticeSort.isExistSortName(sortName)){
			data="{\"msg\":\"T\"}";
		}else{
			data="{\"msg\":\"F\"}";
		}
		out.print(data);
		out.close();
		return null;
	}
	public String isExistTitle(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String title = req.getParameter("title");
		PrintWriter out =res.getWriter();
		String data = "";
		if(iNotice.isExistTitle(title)){
			data="{\"msg\":\"T\"}";
		}else{
			data="{\"msg\":\"F\"}";
		}
		out.print(data);
		out.close();
		return null;
	}
	/**
	 * ajax验证注册的orgName是否存在
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException 
	 */
	public String orgName(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String name = req.getParameter("name");
		PrintWriter out = res.getWriter();
		String data = "";
		if(iOrg.isExist(name)){
			data="{\"msg\":\"T\"}";
		}else{
			data="{\"msg\":\"F\"}";
		}
		out.print(data);
		out.close();
		return null;
	}
	/**
	 * ajax验证ruleName是否存在
	 * @param req
	 * @param res
	 * @throws IOException 
	 */
	public void ruleName(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String name = req.getParameter("name");
		PrintWriter out = res.getWriter();
		String data = "";
		if(iRule.ruleName(name)){
			data="{\"msg\":\"T\"}";
		}else{
			data="{\"msg\":\"F\"}";
		}
		out.print(data);
		out.close();
	}
	/**
	 * ajax验证送人头Name是否存在
	 * @param req
	 * @param res
	 * @throws IOException 
	 */
	public void sortName(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String name = req.getParameter("name");
		PrintWriter out = res.getWriter();
		String data = "";
		if(iRuleSort.sortName(name)){
			data = "{\"msg\":\"T\"}";
		}else{
			data = "{\"msg\":\"F\"}";
		}
		out.print(data);
		out.close();
	}
}
