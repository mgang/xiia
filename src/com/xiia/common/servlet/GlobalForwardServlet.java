package com.xiia.common.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.servcie.imp.UserService;
import com.xiia.service.inter.IUser;
import com.xiia.util.MD5;
import com.xiia.util.ParamUtil;
import com.xiia.util.ValidateCode;
import com.xiia.vo.User;

/**
 * 
 * @author meigang 2014-11-4 10:20
 * 全局跳转（不用权限拦截的跳转）
 */
@WebServlet("/gf")
public class GlobalForwardServlet extends FrameworkServlet{
	private static IUser iUser;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public GlobalForwardServlet(){
		iUser = new UserService();
	}

	
	
	/**
	 * 跳转到后台的登陆界面
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String toIndex(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		return "bclient/login.jsp";
	}
	
	/**
	 * 后台管理员登陆
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String login(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		User u = doForm(request);
		//System.out.println(u.getUserName()+":"+u.getPassword()+":"+request.getParameter("inCode"));
		String checkCode = request.getParameter("inCode");
		if(ValidateCode.checkInCode(request, checkCode)){
			//验证码正确
			//将password加密MD5
			u.setPassword(MD5.getMD5(u.getPassword()));
			u = iUser.loginUser(u);
			if(null != u){
				//该用户是数据库中的存在用户
				//匹配用户有没有后台登陆权限
				boolean haveFun = checkFunctionByUser(u,REQUEST_URL);
				if(haveFun){
					//有后台登陆权限
					//该用户是否是管理员级别
					if(u.getUserType()>2){
						resString = "该用户不是管理员";
					}else{
						//是管理员，是否是可用的管理员
						if(u.getStatus()==0){
							resString = "该管理员账号被禁用";
						}else{
							//登陆成功
							request.getSession().setAttribute(ParamUtil.LOGIN_USER, u);
							return "bclient/index.jsp";
						}
					}
				}else{
					resString = "该用户无操作权限";
				}
			}else{
				//登陆失败
				resString = "用户名或密码错误";
			}
		}else{
			resString = "验证码错误";
		}
		request.setAttribute("resString", resString);
		request.setAttribute("resUser", u);
		return "bclient/login.jsp";
	}
	/**
	 * 管理员注销
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String logout(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//删除登陆用户的session
		request.getSession().removeAttribute(ParamUtil.LOGIN_USER);
		//清空session
		request.getSession().invalidate();
		return "bclient/login.jsp";
	}
	/**
	 * 封装表单
	 * @param r
	 * @return
	 */
	private User doForm(HttpServletRequest r){
		User u = new User();
		if(null != r.getParameter("userId"))
			u.setUserId(Integer.parseInt(r.getParameter("userId").trim()));
		if(null != r.getParameter("userName"))
			u.setUserName(r.getParameter("userName"));
		if(null != r.getParameter("password"))
			u.setPassword(r.getParameter("password"));
		if(null != r.getParameter("userType"))
			u.setUserType(Integer.parseInt(r.getParameter("userType")));
		return u;
	}
}
