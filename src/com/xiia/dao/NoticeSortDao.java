package com.xiia.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.xiia.util.DBConnect;
import com.xiia.util.Pager;
import com.xiia.vo.NoticeSort;

/**
 * 
 * @author zhouhao 2014-11-4 10:34
 * NoticeSort的dao
 */
public class NoticeSortDao extends BaseDao {

	public NoticeSort showNoticeSortById(int sortId){
		String sql = "select * from t_notice_sort where sortId = ?";
		Object[] params = {sortId};
		NoticeSort noticeSort = (NoticeSort)findObject(sql, params, NoticeSort.class);
		if (noticeSort != null){
			return noticeSort;
		}
		System.out.println("通过id查询公告分类出错");
		return null; 
	}
	/**
	 * 列出所有的公告分类
	 * @return
	 */
	public List<NoticeSort> listNoticeSort() {
		// TODO Auto-generated method stub
		String sql = "select * from t_notice_sort";
		List<NoticeSort> noticeSorts = find(sql, null, NoticeSort.class);
		if (noticeSorts.size() > 0){
			return noticeSorts;
		}
		System.out.println("查询公告分类出错");
		return null;
	}
	public Pager listNoticeSort(Pager page) {
		// TODO Auto-generated method stub
		String sql = "select * from t_notice_sort order by "
				+ "lastUpdateTime desc limit ?, ?";
		Object[] params = {(page.getCurrentPage() - 1) * page.getPageSize(),
							page.getPageSize()};
		List<NoticeSort> noticeSorts = find(sql, params, NoticeSort.class);
		if ( noticeSorts != null){
			//Collections.sort(noticeSorts);  //按照内部比较器排序
			page.setList(noticeSorts);
			return page;
		}
		System.out.println("分页查询所有公告分类出错");
		return null;
	}
	/**
	 * 
	 * @param noticeSort
	 * @return
	 */
	public boolean addNoticeSort(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		String sql = "insert into t_notice_sort(sortName, noticeNum, note, "
				+ "createTime, lastUpdateTime, userId) values(?, ?, ?, ?, ?, ?)";
		Object[] params = {noticeSort.getSortName(),
							noticeSort.getNoticeNum(),
							noticeSort.getNote(),
							noticeSort.getCreateTime(),
							noticeSort.getLastUpdateTime(),
							noticeSort.getUserId()};
		int c = update(sql, params);
		if (c > 0){
			return true;
		}
		System.out.println("插入公告分类出错");
		return false;
	}
	/**
	 * 	
	 * @param noticeSort
	 * @return
	 */
	public boolean updateNoticeSort(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		String sql = "update t_notice_sort set sortName = ?, note = ?, "
				+ "lastUpdateTime = ? where sortId = ?";
		Object[] params = {noticeSort.getSortName(),
							noticeSort.getNote(),
							noticeSort.getLastUpdateTime(),
							noticeSort.getSortId()};
		int c = update(sql, params);
		if (c > 0){
			return true;
		}
		System.out.println("更新公告分类出错");
		return false;
	}
	public boolean deleteNoticeSort(NoticeSort noticeSort) {
		// TODO Auto-generated method stub
		String sql = "delete from t_notice_sort where sortId = ?";
		Object[] params = {noticeSort.getSortId()};
		try {
			conn = DBConnect.getConnect();
			conn.setAutoCommit(false);
			QueryRunner query = new QueryRunner();
			if (noticeSort.getNoticeNum() > 0) { //若分类的公告数量不为0，则先删除所有公告
				String sql1 = "delete from t_notice where sortId = ?";
				query.update(conn, sql1, params);
			}
			query.update(conn, sql, params);  //删除公告分类
			conn.commit();  //提交
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			try {
				conn.rollback(); //回滚
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("删除公告分类出错");
			return false;
		} finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public int getNoticeNum(NoticeSort noticeSort){
		String sql = "select count(*) count from t_notice where sortId = ?";
		Object[] params = {noticeSort.getSortId()};
		return getCountFromTable(sql, params);
	}
	public boolean isExistSortName(String sortName) {
		// TODO Auto-generated method stub
		String sql = "select count(*) count from t_notice_sort where sortName = ?";
		Object[] params = {sortName};
		int num = getCountFromTable(sql, params);
		if (num > 0){
			return true;
		}
		return false;
	}
}
