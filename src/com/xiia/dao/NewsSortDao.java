package com.xiia.dao;



import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.xiia.util.DBConnect;
import com.xiia.util.Pager;
import com.xiia.vo.News;
import com.xiia.vo.NewsSort;
import com.xiia.vo.NoticeSort;

/**
 * 2014-11-4 15:15
 * @author 陈强
 *新闻分类数据层
 */
public class NewsSortDao extends BaseDao{
	private static UserDao userDao;
	public NewsSortDao(){
		userDao = new UserDao();
	}
	/**
	 * 新建新闻分类
	 * param NewsSort
	 * @return boolean
	 */
	public boolean createNewSort(NewsSort newsSort){
		String sql="insert into t_news_sort(sortName,newsNum,createTime,updateTime,note,userId) values"+
				"(?,?,?,?,?,?)";
		Object[] params={newsSort.getSortName(),0,new Date(),new Date(),newsSort.getNote(),newsSort.getUser().getUserId()};
		int c = update(sql, params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 通过新闻分类id来删除新闻分类
	 * @param newsSort
	 * @return boolean
	 */
	public boolean deleteSort(NewsSort newsSort){
		String sql="delete from t_news_sort where sortId=?";
		Object[] params ={newsSort.getSortId()};
		try {
			conn = DBConnect.getConnect();
			conn.setAutoCommit(false);
			QueryRunner query = new QueryRunner();
			if (newsSort.getNewsNum() > 0) { //若分类的公告数量不为0，则先删除所有公告
				String sql1 = "delete from t_news where sortId = ?";
				query.update(conn, sql1, params);
			}
			query.update(conn, sql, params);  //删除公告分类
			conn.commit();  //提交
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			try {
				conn.rollback(); //回滚
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		} finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}
	/**
	 * 修改新闻分类的分类 名和创建人
	 * @param newsSort
	 * @return boolean
	 */
	public boolean updateSort(NewsSort newsSort){
		String sql="update t_news_sort set sortName=?,userId=?,note=? where sortId=?";
		Object[] params={newsSort.getSortName(),newsSort.getUser().getUserId(),newsSort.getNote(),newsSort.getSortId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 查找所有的新闻分类
	 * @return List<NewsSort>
	 */
	public List<NewsSort> findAllSort(){
		String sql="select * from t_news_sort order by createTime asc";
		Object[] params={};
		List<NewsSort> newsSorts = new ArrayList<NewsSort>();
		Class<NewsSort> newsSort =NewsSort.class;
		newsSorts = find(sql,params,newsSort);
		if(null != newsSort){
			for(NewsSort n : newsSorts){
				n.setUser(userDao.getUserById(n.getUserId()));
			}
		}
		return newsSorts;
	}
	/**
	 * 得到t_news_sort的总行数
	 * @return
	 */
	public int getCount() {
		String sql = "select count(*) count from t_news_sort ";
		return getCountFromTable(sql,null);
	}
	/**
	 * 得到带有数据的pege对象
	 * @param page
	 * @return 返回带有NewsSort集合的page
	 */
	public Pager findNewsSortPage(Pager page ) {
		// TODO Auto-generated method stub
		
		String sql = "select * from t_news_sort order by createTime asc  limit ?,?";
		Object[] params = {(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()
				};
		List<NewsSort> newsSorts =find(sql,params,NewsSort.class);
		if(null != newsSorts){
			for(NewsSort n : newsSorts){
				n.setUser(userDao.getUserById(n.getUserId()));
			}
		}
		page.setList(newsSorts);
		return page;
	}
	/**
	 * 修改新闻分类下的新闻数量
	 * @param newsSort
	 * @return boolean
	 */
	public boolean updateSortNewsNum(NewsSort newsSort){
		String sql="update t_news_sort set newsNum=? where sortId=?";
		Object[] params={newsSort.getNewsNum(),newsSort.getSortId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 查看新闻是否存在
	 * @param sortName
	 * @return
	 */
	public boolean isExistSortName(String sortName){
		String sql = "select * from t_news_sort where sortName=?";
		Object[] params={sortName};
		Object newsSort = new NewsSort();
		Class<NewsSort> newsSortClass = NewsSort.class;
		newsSort = findObject(sql,params,newsSortClass);
		if(newsSort==null)
			return true;
		else
			return false;
	}
	/**
	 * 通过id来找新闻分类
	 * @param sortId
	 * @return 新闻分类
	 */
	public NewsSort findNewsSortById(int sortId){
		String sql = "select * from t_news_sort where sortId=?";
		Object[] params={sortId};
		NewsSort newsSort = new NewsSort();
		Class<NewsSort> newsSortClass = NewsSort.class;
		newsSort = (NewsSort)findObject(sql,params,newsSortClass);
		newsSort.setUser(userDao.getUserById(newsSort.getUserId()));
		return newsSort;
	}
}
