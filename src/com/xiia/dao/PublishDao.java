package com.xiia.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.xiia.util.CheckConstant;
import com.xiia.util.DBConnect;
import com.xiia.util.Pager;
import com.xiia.vo.Ad;
import com.xiia.vo.Check;
import com.xiia.vo.Menu;
import com.xiia.vo.News;
import com.xiia.vo.Notice;
import com.xiia.vo.Org;
import com.xiia.vo.Project;
import com.xiia.vo.Publish;
import com.xiia.vo.Resource;
import com.xiia.vo.Revoke;
import com.xiia.vo.Rule;

/**
 * 
 * @author meigang 2014-11-10 17:45
 *
 */
public class PublishDao extends BaseDao{
	private static UserDao userDao;
	public PublishDao(){
		userDao = new UserDao();
	}
	/**
	 * 发布
	 * @param p 要发布的实体
	 * @return 发布成功返回ture,反之返回false.
	 */
	public boolean publish(Publish p) {
		// TODO Auto-generated method stub
		//开事务
		boolean b = true;
		try {
			conn = DBConnect.getConnect();
			conn.setAutoCommit(false);
			QueryRunner query = new QueryRunner();
			//1.将实体的status=5
			String sql = "update t_"+p.getType()+" set status=? where "+p.getType()+"Id=?";
			Object[] p1 = {CheckConstant.STATUS_PUBLISH_DONE,
						p.getPublishedId()};
			query.update(conn,sql,p1);
			
			//2.加入到t_publish表中
			sql = "update t_publish set lastPublishTime=?,flag=?,userId=? where publishedId=? and type=?";
			Object[] p2 = {p.getLastPublishTime(),
					p.getFlag(),
					p.getUser().getUserId(),
					p.getPublishedId(),
					p.getType()};
			query.update(conn,sql,p2);
			
			if(p.getFlag() == CheckConstant.FLAG_YES_PUBLISH){
				//3.成功发布，加入到t_revoke
				//3.1.先查询得到t_revoke表中有没有revokedId,type的
				sql = "select * from t_revoke where revokedId=? and type=?";
				Object[] p3 = {p.getPublishedId(),p.getType()};
				Revoke revoke = (Revoke) query.query(conn,sql,p3,new BeanHandler(Revoke.class));
				if(null != revoke){
					//如果已经被撤销过
					sql = "update t_revoke set flag=? where revokedId=? and type=?";
					Object[] p4 = {CheckConstant.FLAG_NO_REVOKE,
							p.getPublishedId(),
							p.getType()};
					query.update(conn,sql,p4);
				}else{
					//第一次发布成功
					sql = "insert into t_revoke(revokedId,type,flag) values(?,?,?)";
					Object[] p4 = {p.getPublishedId(),
							p.getType(),
							CheckConstant.FLAG_NO_REVOKE};
					query.update(conn,sql,p4);
				}
				
			}
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				b = false;
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return b;
	}
	/**
	 * 得到发布了的实体的行数
	 * @return
	 */
	public int count() {
		// TODO Auto-generated method stub
		String sql = "select count(publishId) count from t_publish where flag=?";
		Object[] p = {CheckConstant.FLAG_YES_PUBLISH};
		return getCountFromTable(sql, p);
	}
	/**
	 * 得到发布了的实体的分页数据
	 * @param page
	 * @param orderBy
	 * @return
	 */
	public Pager findAllPublishPage(Pager page, String orderBy) {
		String sql = "select * from t_publish where flag=? order by ? asc limit ?,?";
		Object[] params = {CheckConstant.FLAG_YES_PUBLISH,
				orderBy,
				(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()
				};
		List<Publish> ps = find(sql, params, Publish.class);
		if(null != ps){
			for(Publish p : ps){
				Class clazz = getClassByPublish(p);
				sql = "select * from t_"+p.getType()+" where "+p.getType()+"Id=?";
				Object[] p1 = {p.getPublishedId()};
				p.setObj(findObject(sql, p1, clazz));
				//封装用户
				p.setUser(userDao.getUserById(p.getUserId()));
			}
		}
		page.setList(ps);
		return page;
	}
	/**
	 * 
	 * @param c
	 * @return
	 */
	private Class getClassByPublish(Publish c){
		Class clazz = null;
		if(CheckConstant.TYPE_AD.equals(c.getType())){
			//广告
			clazz = Ad.class;
		}else if(CheckConstant.TYPE_MENU.equals(c.getType())){
			clazz = Menu.class;
		}else if(CheckConstant.TYPE_NEWS.equals(c.getType())){
			clazz = News.class;
		}else if(CheckConstant.TYPE_RESOURCE.equals(c.getType())){
			clazz = Resource.class;
		}else if(CheckConstant.TYPE_RULE.equals(c.getType())){
			clazz = Rule.class;
		}else if(CheckConstant.TYPE_NOTICE.equals(c.getType())){
			clazz = Notice.class;
		}else if(CheckConstant.TYPE_ORG.equals(c.getType())){
			clazz = Org.class;
		}else if(CheckConstant.TYPE_PROJECT.equals(c.getType())){
			clazz = Project.class;
		}
		return clazz;
	}

}
