package com.xiia.dao;

import java.util.List;

import com.xiia.util.CheckConstant;
import com.xiia.util.Pager;
import com.xiia.vo.Ad;

/**
 * 
 * @author Administrator
 *
 *
 */
public class AdDao extends BaseDao{
	/**
	 * 获取数据库数据总数
	 * @return
	 */
	public int getAdNum(){
		String sql="select count(*) count from t_ad where status in(?, ?, ?)";
		Object[] params={CheckConstant.STATUS_WRITE_DONE,CheckConstant.STATUS_CHECK_NO_PASS,CheckConstant.STATUS_REVOKE_DONE};
	    int num=getCountFromTable(sql,params);
	    return num;
	}
   /**
    * 根据id查询
    * @param ad
    * @return
    */
	public List<Ad> selectAd( Ad ad){
		String sql = "select * from t_ad where adId=?&adNa";
		Object[] params={ad.getAdId()};
		List<Ad> s = find(sql,params,Ad.class);
		return s;
	}
	/**
	 * 多状态查询
	 * @param ad
	 * @return
	 */
	public Pager selectAd1(Pager page){
		String sql="select * from t_ad where status in(?,?,?) order by lastUpdateTime desc limit ?, ?";
		Object[] params={CheckConstant.STATUS_WRITE_DONE,CheckConstant.STATUS_CHECK_NO_PASS,
				CheckConstant.STATUS_REVOKE_DONE,(page.getCurrentPage() - 1) * page.getPageSize(),
				page.getPageSize()};
		@SuppressWarnings("unchecked")
		List<Ad> s=find(sql,params,Ad.class);
		System.out.println(s);
		if(s!=null){
			page.setList(s);
		    return page;
		}
		return null;
	}
	
	/**
	 * 更新链接
	 * @param ad
	 * @return
	 */
    public boolean update(Ad ad){
    	String sql="update t_ad set adPath=?,lastUpdateTime=? where adId=?";
    	Object[] params={ad.getAdPath(),ad.getLastUpdateTime(),ad.getAdId()};
    	int s=update(sql,params);
    	if(s!=0){
    		return true;
    	}
    	else{
    		return false;
    	}	
    }
    /**
     * 删除广告
     * @param ad
     * @return
     */
    public boolean delete(Ad ad){
    	String sql="delete from t_ad where adId=?";
    	Object[] params={ad.getAdId()};
    	int s=update(sql,params);
    	if(s==0){
    		return true;
    	}
    	else{
    		return false;
    	}	
    }
    /**
     * 更改状态
     * @param ad
     * @return
     */
    public boolean updateStatus(Ad ad){
    	String sql="update t_ad set status=? where adId=?";
    	Object[] params={ad.getStatus(),ad.getAdId()};
    	int s=update(sql,params);
    	if(s==0){
    		return true;
    	}
    	else{
    		return false;
    	}	
    }
 
    /**
     * 向数据库中插入数据
     * @param ad
     * @return
     */
    public int add(Ad ad){
    	String sql="insert into t_ad(adName,adType,content,adPath,dbPath,status,createTime,lastUpdateTime,userId) values"
    			+ "(?,?,?,?,?,?,?,?,?)";
    	Object[] params={ad.getAdName(),
    			ad.getAdType(),
    			ad.getContent(),
    			ad.getAdPath(),
    			ad.getDbPath(),
    			ad.getStatus(),
    			ad.getCreateTime(),
    			ad.getLastUpdateTime(),
    			ad.getUserId()};
    	System.out.println("add ad");
    	return update(sql,params);
    		
    }
    /**
     * 向数据库中check表中插入数据
     * @param ad
     * @return
     */
    public boolean add_check(Ad ad){
    	String sql="insert into t_check(checkedId,type,flag,userId) values(?,?,?,?)";
    	Object[] params={ad.getAdId(),"ad",0,ad.getUserId()};
    	int s=update(sql,params);
    	if(s!=0){
    		return true;
    	}
        return false;
    }
}
