package com.xiia.dao;


import java.util.Date;
import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.RuleSort;

/**
 * 
 * @author 胡天天 2014-11-4
 * RuleSort的dao
 *
 */
public class RuleSortDao extends BaseDao{
	private  static UserDao userDao;
	public RuleSortDao(){
		userDao = new UserDao();
	}
	/**
	 * 新建rulesort
	 * @param rulesort
	 * @return boolean
	 */
	public boolean createRuleSort(RuleSort rulesort){
		String sql="insert into t_rule_sort(sortName,note,createTime,"
				    +"lastUpdateTime,userId)values"+"(?,?,?,?,?)";
		Object[] params={rulesort.getSortName(),rulesort.getNote(),
				new Date(),new Date(),rulesort.getUser().getUserId()};
		int c=update(sql,params);
		if(c>0)
			return true;
		else 
			return false;
	}
	/**
	 * 通过rulesort的Id删除rulesort
	 * @param rulesort
	 * @return boolean
	 */
	public boolean deleteRuleSort(RuleSort rulesort){
		String sql="delete from t_rule_sort where sortId=?";
		Object[] params ={rulesort.getSortId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;	
	}
	/**
	 * 更改rulesort的name,sort和userId
	 * @param rulesort
	 * @return boolean
	 */
	public boolean updateRuleSort(RuleSort rulesort){
		String sql="update t_rule_sort set sortName=?,note=?,lastUpdateTime=?,userId=? "
					+ "where sortId=?";
		Object[] params ={rulesort.getSortName(),rulesort.getNote(),new Date(),
						  rulesort.getUser().getUserId(),rulesort.getSortId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;	
	}
	/**
	 * 查找所有的rulesort
	 * @return Pager对象
	 */
	@SuppressWarnings("unchecked")
	public Pager findAllRuleSortP(Pager page){
		String sql="select * from t_rule_sort  limit ?,?";
		Object[] param = {(page.getCurrentPage()-1)*page.getPageSize(),page.getPageSize()};
		List<RuleSort> ruleSorts = find(sql,param,RuleSort.class);
		for(RuleSort r:ruleSorts){
			r.setUser(userDao.getUserById(r.getUserId()));
		}
		page.setList(ruleSorts);
		return page;
	}
	/**
	 * 查找所有的rulesort
	 * @return rulesort的list集合
	 */
	public List<RuleSort> findAllRuleSort(){
		String sql = "select * from t_rule_sort ";
		List<RuleSort> ruleSort = find(sql, null, RuleSort.class);
		for(RuleSort r:ruleSort){
			r.setUser(userDao.getUserById(r.getUserId()));
		}
		return ruleSort;
	}
	/**
	 * 修改rulesort下的rule数量
	 * @return boolean
	 */
	public boolean updateRuleSortNum(RuleSort rulesort){
		String sql="update t_rule_sort set ruleNum=? where sortId=?";
		Object[] params={rulesort.getRuleNum(),rulesort.getSortId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 通过sortid查找改rulesort
	 * @param sortId
	 * @return 法规分类
	 */
	public RuleSort findRuleSortById(RuleSort rulesort){
		String sql = "select * from t_rule_sort where sortId=?";
		Object[] params={rulesort.getSortId()};
		rulesort = (RuleSort)findObject(sql,params,RuleSort.class);
		return rulesort;
	}
	/**
	 * 通过sortId来查询得到RuleSort对象
	 * @param sortId
	 * @return
	 */
	public RuleSort getRuleSortById(int sortId) {
		// TODO Auto-generated method stub
		String sql = "select * from t_rule_sort where sortId=?";
		Object[] p = {sortId};
		return (RuleSort) findObject(sql, p, RuleSort.class);
	}
	/**
	 * 查询数据库中是否有该sortName
	 * @param name
	 * @return
	 */
	public boolean sortName(String name){
		String sql = "select * from t_rule_sort where sortName=?";
		Object[] param = {name};
		RuleSort ruleSort = (RuleSort)findObject(sql, param, RuleSort.class);
		if(ruleSort==null){
			return false;
		}
		else{
			return true;
		}
	}
	public int getSortCount(){
		String sql = "select count(*) count from t_rule_sort";
		int c = getCountFromTable(sql, null);
		return c;
	}
}
