package com.xiia.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xiia.vo.NewsHeadline;
/**
 * 2014 11 4 21:20
 * @author 陈强
 * 新闻头条数据层
 */
public class NewsHeadlineDao extends BaseDao{
	/**
	 * 添加头条
	 * @param newsHeadline
	 * @return boolean
	 */
	public boolean addNewsHeadline(NewsHeadline  newsHeadline){
		String sql = "insert into t_news_headline(headTitle,headNote,addTime,headUrl,lastUpdateTime,userId,newsId)"+
				"values(?,?,?,?,?,?,?)";
		Object[] params = {newsHeadline.getHeadTitle(),newsHeadline.getHeadNote(),new Date(),newsHeadline.getHeadUrl(),new Date(),newsHeadline.getUser().getUserId(),newsHeadline.getNews().getNewsId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 通过新闻Id来查询头条
	 * @param newsId
	 * @return
	 */
	public NewsHeadline getHeadlineByNewsId(int newsId){
		String sql="select * from t_news_headline where newsId=?";
		Object[] params={newsId};
		Class<NewsHeadline> newsHeadlineClass = NewsHeadline.class;
		NewsHeadline newsHeadline = new NewsHeadline();
		newsHeadline = (NewsHeadline)findObject(sql,params,newsHeadlineClass);
		return newsHeadline;
	}
	/**
	 * 更新头条标题和描述
	 */
	public boolean updateTitle(NewsHeadline headline){
		String sql = "update t_news_headline set headTitle=? ,headNote=? where headlineId=?";
		Object[] params = {headline.getHeadTitle(),headline.getHeadNote(),headline.getHeadlineId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 修改头条照片的路径
	 * @param headline
	 * @return boolean
	 */
	public boolean updateUrl(NewsHeadline headline){
		String sql="update t_news_headline set headUrl=? where headlineId=?";
		Object[] params = {headline.getHeadUrl(),headline.getHeadlineId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
}
