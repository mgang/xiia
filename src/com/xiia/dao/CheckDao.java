package com.xiia.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;

import com.xiia.util.CheckConstant;
import com.xiia.util.DBConnect;
import com.xiia.util.Pager;
import com.xiia.vo.Ad;
import com.xiia.vo.Check;
import com.xiia.vo.Menu;
import com.xiia.vo.News;
import com.xiia.vo.Notice;
import com.xiia.vo.Org;
import com.xiia.vo.Project;
import com.xiia.vo.Resource;
import com.xiia.vo.Rule;

/**
 * 
 * @author meigang 2014-11-10 17:45
 *
 */
public class CheckDao extends BaseDao{
	private static UserDao userDao;
	public CheckDao(){
		userDao = new UserDao();
	}
	
	/**
	 * 通过type查询t_check中的总行数
	 * @param type 类型
	 * @return 返回查询得到的行数
	 */
	public int countByType(String type) {
		// TODO Auto-generated method stub
		String sql = "select count(checkId) count from t_check where type=?";
		Object[] params = {type};
		return getCountFromTable(sql, params);
	}
	/**
	 * 返回带有审核数据的page对象
	 * @param page
	 * @param orderBy 按orderBy字段排序，默认desc
	 * @param c 查询的审核数据类型c.getType()，如rule,org,project,menu,news,notice
	 * @return
	 */
	public Pager findCheckPage(Pager page, String orderBy,Check c) {
		// TODO Auto-generated method stub
		String sql = "select * from t_check where type=? order by ? desc limit ?,?";
		Object[] p = {c.getType(),
				orderBy,
				(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()
				};
		List<Check> checkList = find(sql, p, Check.class);
		//判断审核实体类型type和表名t_的关系
		Class clazz = getClassByCheck(c);
		//将审核的实体封装到checkList中
		if(null != checkList && null != clazz){
			for(Check ch : checkList){
				sql = "select * from t_"+c.getType()+" where "+c.getType()+"Id=?";
				Object[] p1 = {ch.getCheckedId()};
				ch.setObj(findObject(sql, p1, clazz));
			}
		}
		page.setList(checkList);
		return page;
	}
	/**
	 * 开始审核
	 * @param c
	 * @return
	 */
	public boolean startCheck(Check c,int status) {
		// TODO Auto-generated method stub
		return doCheck(c, status);
	}
	/**
	 * 通过check对象中的type字段来得到要封装的实体类型
	 * @param c
	 * @return 返回要封装的实体Class
	 */
	private Class getClassByCheck(Check c){
		Class clazz = null;
		if(CheckConstant.TYPE_AD.equals(c.getType())){
			//广告
			clazz = Ad.class;
		}else if(CheckConstant.TYPE_MENU.equals(c.getType())){
			clazz = Menu.class;
		}else if(CheckConstant.TYPE_NEWS.equals(c.getType())){
			clazz = News.class;
		}else if(CheckConstant.TYPE_RESOURCE.equals(c.getType())){
			clazz = Resource.class;
		}else if(CheckConstant.TYPE_RULE.equals(c.getType())){
			clazz = Rule.class;
		}else if(CheckConstant.TYPE_NOTICE.equals(c.getType())){
			clazz = Notice.class;
		}else if(CheckConstant.TYPE_ORG.equals(c.getType())){
			clazz = Org.class;
		}else if(CheckConstant.TYPE_PROJECT.equals(c.getType())){
			clazz = Project.class;
		}
		return clazz;
	}
	/**
	 * 做审核
	 * @param c
	 * @param status 3，表示审核通过，4表示审核不通过
	 * @return
	 */
	private boolean doCheck(Check c,int status){
		boolean b = true;
		/**
		 * 如果审核通过的是政策法规
		 * 政策法规的status变成3，并修改到t_check表中 checkedId=1 and type='rule' 
			and flag=1[1:已审核]
		 */
		try {
			//开启一个事务
			conn = DBConnect.getConnect();
			conn.setAutoCommit(false);
			QueryRunner query = new QueryRunner();
			//1,将实体表的status修改为3
			String sql = "update t_"+c.getType()+" set status=? where "+c.getType()+"Id=?";
			Object[] p = {status,
					c.getCheckedId()};
			query.update(conn,sql,p);
			//2.t_check表中的flag=1，设置msg信息
			sql = "update t_check set flag=?,msg=?,lastCheckTime=?,userId=? where checkedId=? and type=?";
			Object[] p1 = {c.getFlag(),
					c.getMsg(),
					c.getLastCheckTime(),
					c.getUser().getUserId(),
					c.getCheckedId(),
					c.getType()};
			query.update(conn,sql,p1);
			if(c.getFlag() ==  CheckConstant.FLAG_YES_CHECK){
				//3.加入到t_publish
				sql = "insert into t_publish(publishedId,type,flag) values(?,?,?)";
				Object[] p2 = {c.getCheckedId(),
						c.getType(),
						CheckConstant.FLAG_NO_PUBLISH};
				query.update(conn,sql,p2);
			}
			
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				//回滚
				b = false;
				conn.rollback();
				
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return b;
	}
	/**
	 * 查询得到所有的check记录总数
	 * @return
	 */
	public int count() {
		// TODO Auto-generated method stub
		String sql = "select count(checkId) count from t_check where flag=?";
		Object[] p = {CheckConstant.FLAG_YES_CHECK};
		return getCountFromTable(sql, p);
	}
	/**
	 * 查询得到所有的check的page数据
	 * @param page
	 * @return
	 */
	public Pager findAllCheckPage(Pager page,String orderBy) {
		// TODO Auto-generated method stub
		String sql = "select * from t_check where flag=? order by ? asc limit ?,?";
		Object[] params = {CheckConstant.FLAG_YES_CHECK,
				orderBy,
				(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()
				};
		List<Check> cs = find(sql, params, Check.class);
		if(null != cs){
			for(Check c : cs){
				Class clazz = getClassByCheck(c);
				sql = "select * from t_"+c.getType()+" where "+c.getType()+"Id=?";
				Object[] p = {c.getCheckedId()};
				c.setObj(findObject(sql, p, clazz));
				//封装用户
				c.setUser(userDao.getUserById(c.getUserId()));
			}
		}
		page.setList(cs);
		return page;
	}

}
