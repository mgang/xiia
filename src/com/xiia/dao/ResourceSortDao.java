package com.xiia.dao;


import java.util.List;





import com.xiia.util.Pager;
import com.xiia.vo.ResourceSort;


/**
 * 
 * @author 王书恒 2014-11-4
 *ResourceSort的Dao
 */
public class ResourceSortDao extends BaseDao {
	private static UserDao userDao;
	public ResourceSortDao(){
		userDao = new UserDao();
	}
	 
	
	
	
	/**
	 * 增加资源分类
	 * @param resourceSort 资源分类对象
	 * @return int 返回影响数据库的行数
	 */
	public int addResourceSort(ResourceSort resourceSort) {
		String sql = "insert into t_resource_sort(sortName,resNum,addTime,userId) "
				+ "values(?,?,?,?)";
		Object[] params = {resourceSort.getSortName(),
				0,
				resourceSort.getAddTime(),
				resourceSort.getUser().getUserId()};
		return update(sql, params);
	}
	
	/**
	 * 删除资源分类
	 * @param resourceSorts 资源分类集合
	 * @return boolean
	 */
	public boolean deleteResourceSort(ResourceSort resourceSort) {
		String sql = "delete from t_resource_sort where sortId = ?";
		Object[] params = {resourceSort.getSortId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else 
			return false;
	}
	
	/**
	 * 修改资源分类的名称
	 * @param resourceSort
	 * @return boolean
	 */
	public int update(ResourceSort resourceSort) {
		String sql = "update t_resource_sort set sortName=? where sortId=?";
		Object[] params = {resourceSort.getSortName(),
				resourceSort.getSortId()};
		return update(sql,params);
	}
	/**
	 * 通过sortId得到resourceSort对象
	 * @param sortId
	 * @return 返回resourceSort对象
	 */
	public ResourceSort getResourceSortById(int sortId) {
		// TODO Auto-generated method stub
		String sql = "select * from t_resource_sort where sortId=?";
		Object[] params = {sortId};
		ResourceSort r = (ResourceSort) findObject(sql, params, ResourceSort.class);
		
		return r;
				
	}
	/**
	 * 查询得到资源分类的行数
	 * @return
	 */
	public int countResourceSort() {
		// TODO Auto-generated method stub
		String sql = "select count(sortId) count from t_resource_sort";
		return getCountFromTable(sql, null);
	}
	/**
	 * 查询得到资源分类的分页数据
	 * @param page
	 * @param orderBy
	 * @return
	 */
	public Pager findResourceSortPage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		String sql = "select * from t_resource_sort order by ? limit ?,?";
		Object[] params = {orderBy,
				(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()};
		List<ResourceSort> rsList = find(sql, params, ResourceSort.class);
		if(null != rsList){
			for(ResourceSort rs : rsList){
				rs.setUser(userDao.getUserById(rs.getUserId()));
			}
		}
		page.setList(rsList);
		return page;
	}
	/**
	 * 查询得到所有的资源分类
	 * @return
	 */
	public List<ResourceSort> getAllResourceSort() {
		String sql = "select * from t_resource_sort order by sortId"; 
		return find(sql,null,ResourceSort.class);
	}
}
