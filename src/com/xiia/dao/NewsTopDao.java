package com.xiia.dao;

import com.xiia.vo.NewsTop;
/**
 * 2014 11 4 21:20
 * @author 陈强
 * 新闻置顶数据层
 */
public class NewsTopDao extends BaseDao {
	/**
	 * 将置顶新闻添加到置顶表中
	 * @param newsTop
	 * @return boolean
	 */
	public boolean newsTop(NewsTop newsTop){
		String sql = "insert into t_news_top(topTime,userId,newsId) "+
				"values(?,?,?)";
		Object[] params = {newsTop.getTopTime(),newsTop.getUser().getUserId(),newsTop.getNews().getNewsId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 通过新闻id来取消置顶
	 * @param newsTop
	 * @return boolean
	 */
	public boolean deleteNewsTop(NewsTop newsTop){
		String sql = "delete from t_news_top where newsTopId=?";
		Object[] params = {newsTop.getNewsTopId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
}
