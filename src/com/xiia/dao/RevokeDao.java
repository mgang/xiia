package com.xiia.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;

import com.xiia.util.CheckConstant;
import com.xiia.util.DBConnect;
import com.xiia.util.Pager;
import com.xiia.vo.Ad;
import com.xiia.vo.Check;
import com.xiia.vo.Menu;
import com.xiia.vo.News;
import com.xiia.vo.Notice;
import com.xiia.vo.Org;
import com.xiia.vo.Project;
import com.xiia.vo.Publish;
import com.xiia.vo.Resource;
import com.xiia.vo.Revoke;
import com.xiia.vo.Rule;

/**
 * 
 * @author meigang 2014-11-10 17:46
 *
 */
public class RevokeDao extends BaseDao{
	private static UserDao userDao;
	public RevokeDao(){
		userDao = new UserDao();
	}
	/**
	 * 撤销
	 * @param p 要撤销的实体
	 * @return 撤销成功返回ture,反之返回false.
	 */
	public boolean revoke(Revoke r) {
		// TODO Auto-generated method stub
		//开事务
		boolean b = true;
		try {
			conn = DBConnect.getConnect();
			conn.setAutoCommit(false);
			QueryRunner query = new QueryRunner();
			//1.将实体的status=6
			String sql = "update t_"+r.getType()+" set status=? where "+r.getType()+"Id=?";
			Object[] p1 = {CheckConstant.STATUS_REVOKE_DONE,
						r.getRevokedId()};
			query.update(conn,sql,p1);
			//2.更新t_publish表中
			sql = "update t_publish set flag=? where publishedId=? and type=?";
			Object[] p2 = {CheckConstant.FLAG_NO_PUBLISH,
					r.getRevokedId(),
					r.getType()};
			query.update(conn,sql,p2);
			//3.加入到t_revoke
			sql = "update t_revoke set flag=?,lastRevokeTime=?,userId=? where revokedId=? and type=?";
			Object[] p3 = {r.getFlag(),
					r.getLastRevokeTime(),
					r.getUser().getUserId(),
					r.getRevokedId(),
					r.getType()};
			query.update(conn,sql,p3);
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				b = false;
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return b;
	}
	/**
	 * 得到撤销了的行数
	 * @return
	 */
	public int count() {
		String sql = "select count(revokeId) count from t_revoke where flag=?";
		Object[] p = {CheckConstant.FALG_YES_REVOKE};
		return getCountFromTable(sql, p);
	}

	public Pager findAllRevokePage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		String sql = "select * from t_revoke where flag=? order by ? asc limit ?,?";
		Object[] params = {CheckConstant.FALG_YES_REVOKE,
				orderBy,
				(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()
				};
		List<Revoke> rs = find(sql, params, Revoke.class);
		if(null != rs){
			for(Revoke c : rs){
				Class clazz = getClassByRevoke(c);
				sql = "select * from t_"+c.getType()+" where "+c.getType()+"Id=?";
				Object[] p = {c.getRevokedId()};
				c.setObj(findObject(sql, p, clazz));
				//封装用户
				c.setUser(userDao.getUserById(c.getUserId()));
			}
		}
		page.setList(rs);
		return page;
	}
	
	/**
	 * 通过Revoke对象中的type字段来得到要封装的实体类型
	 * @param c
	 * @return 返回要封装的实体Class
	 */
	private Class getClassByRevoke(Revoke c){
		Class clazz = null;
		if(CheckConstant.TYPE_AD.equals(c.getType())){
			//广告
			clazz = Ad.class;
		}else if(CheckConstant.TYPE_MENU.equals(c.getType())){
			clazz = Menu.class;
		}else if(CheckConstant.TYPE_NEWS.equals(c.getType())){
			clazz = News.class;
		}else if(CheckConstant.TYPE_RESOURCE.equals(c.getType())){
			clazz = Resource.class;
		}else if(CheckConstant.TYPE_RULE.equals(c.getType())){
			clazz = Rule.class;
		}else if(CheckConstant.TYPE_NOTICE.equals(c.getType())){
			clazz = Notice.class;
		}else if(CheckConstant.TYPE_ORG.equals(c.getType())){
			clazz = Org.class;
		}else if(CheckConstant.TYPE_PROJECT.equals(c.getType())){
			clazz = Project.class;
		}
		return clazz;
	}
}
