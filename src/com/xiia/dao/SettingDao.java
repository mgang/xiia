package com.xiia.dao;

import com.xiia.vo.Setting;


/**
 * @author	王书恒	2014-11-4
 * Setting的Dao 
 */
public class SettingDao extends BaseDao{
	/**
	 * 得到系统参数
	 * @return
	 */
	public Setting getSetting() {
		// TODO Auto-generated method stub
		String sql = "select * from t_setting where settingId=1";
		return (Setting) findObject(sql, null, Setting.class);
	}
	/**
	 * 更新系统参数
	 * @param setting
	 * @return
	 */
	public int updateSetting(Setting setting) {
		// TODO Auto-generated method stub
		String sql = "update t_setting set menmberScore=?,"
				+ "adminScore=?,"
				+ "headlineNewsNum=?,"
				+ "menuMaxCount=?,"
				+ "uploadResourceSize=?,"
				+ "acceptUploadFileType=?"
				+ " where settingId=?";
		Object[] p = {setting.getMenmberScore(),
				setting.getAdminScore(),
				setting.getHeadlineNewsNum(),
				setting.getMenuMaxCount(),
				setting.getUploadResourceSize(),
				setting.getAcceptUploadFileType(),
				setting.getSettingId()};
		return update(sql, p);
	}
	 
	
}
