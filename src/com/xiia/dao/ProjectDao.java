package com.xiia.dao;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.Project;

/**
 *@author	王书恒	2014-11-4
 * Project的Dao
 */

public class ProjectDao extends BaseDao {
	private static UserDao userDao;
	public ProjectDao(){
		userDao = new UserDao();
	}

	
	/**
	 * 增加项目
	 * @param project 
	 * @return boolean
	 */
	public boolean addProject(Project project) {
		String sql = "insert into t_project(projectName,note,addTime,userId,status)values(?,?,?,?,?)";
		Object[] params = {	project.getProjectName(),
				project.getNote(),
				project.getAddTime(),
				project.getUser().getUserId(),
				project.getStatus()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else 
			return false;
	}
	
	/**
	 * 删除项目
	 * @param project
	 * @return boolean
	 */
	public boolean delete(Project project) {
		String sql = "delete from t_project where projectId =?";
		Object[] params = {project.getProjectId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 通过projectId得到project对象
	 * @param projectId
	 * @return 返回project对象
	 */
	public Project getProjectById(int projectId) {
		// TODO Auto-generated method stub
		String sql = "select * from t_project where projectId=?";
		Object[] params = {projectId};
		Project p = (Project) findObject(sql, params, Project.class);
		
		return p;
				
	}
	/**
	 * 修改项目的名称,描述
	 * @param project
	 * @return boolean
	 */
	public boolean update(Project project) {
		String sql = "update t_project set projectName = ? ,note=? where projectId = ?";
		Object[] params = {project.getProjectName(),
				project.getNote(),
				project.getProjectId()};
		int c = update(sql,params);
		if(c>0)
			return true;
		else
			return false;
	}
	/**
	 * 查询得到项目的行数
	 * @return
	 */
	public int countProject() {
		// TODO Auto-generated method stub
		String sql = "select count(projectId) count from t_project";
		return getCountFromTable(sql, null);
	}
	/**
	 * 查询得到项目的分页数据
	 * @param page
	 * @param string
	 * @return
	 */
	public Pager findProjectPage(Pager page, String orderBy) {
		// TODO Auto-generated method stub
		String sql = "select * from t_project order by ? limit ?,?";
		Object[] params = {orderBy,
				(page.getCurrentPage()-1)*page.getPageSize(),
				page.getPageSize()};
		List<Project> pList = find(sql, params, Project.class);
		if(null != pList){
			for(Project p : pList){
				p.setUser(userDao.getUserById(p.getUserId()));
			}
		}
		page.setList(pList);
		return page;
	}

	
	
	

}
