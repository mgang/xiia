package com.xiia.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.util.ParamUtil;
import com.xiia.vo.User;

/**
 * 
 * @author meigang 2014-11-18 9:52
 * 基本过滤器
 */
public abstract class BaseFilter {
	
	/**
	 * 过滤未登陆用户
	 * @param req
	 */
	protected boolean filterNoLoginAdmin(HttpServletRequest req,HttpServletResponse res){
		boolean b = false;
		User user = (User) req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		if(null == user){
			try {
				//跳到后台的登陆界面
				req.getRequestDispatcher("/gf?action=toIndex").forward(req, res);
				b = true;
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return b;
	}
}
