package com.xiia.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.xiia.util.LogUtil;
/**
 * 
 * @author meigang 2014-11-18 9:51
 * 过滤器
 */
@WebFilter("*.jsp")
public class JspFilter extends BaseFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String url = request.getRequestURL().toString() +"?"+ request.getQueryString();
		LogUtil.logger.info("jspFilter:"+url);
		if(url.contains("bclient")){
			filterNoLoginAdmin(request, response);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		//初始化log4j环境
		String classPath = arg0.getServletContext().getRealPath("/WEB-INF/classes");
		LogUtil.logger = Logger.getLogger(JspFilter.class);
	}
	
}
