package com.xiia.tag;

import com.xiia.util.FunctionMatch;
import com.xiia.vo.Function;
import com.xiia.vo.Role;
import com.xiia.vo.User;

/**
 * 
 * @author meigang 2014-11-8 17:02
 * 自定义匹配权限的function标签
 */
public class CheckFunctionTag {
	
	/**
	 * 检查用户权限
	 * @param u 用户对象
	 * @param url 请求的url
	 * @return 如果该用户有该权限，就返回true.
	 */
	public static boolean checkFunctionByUser(User u,String url){
		boolean haveFun = false;
		if(null != u){
			for(Role role : u.getRoleList()){
				for(Function f : role.getFunList()){
					haveFun = FunctionMatch.matchFunctionByReqUrl(f, url);
					//匹配成功跳出
					if(haveFun)
						break;
				}
			}
		}
	
		return haveFun;
	}
}
