package com.xiia.admin.servlet;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.RuleService;
import com.xiia.servcie.imp.RuleSortService;
import com.xiia.service.inter.IRule;
import com.xiia.service.inter.IRuleSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.Rule;
import com.xiia.vo.RuleSort;
import com.xiia.vo.User;
@SuppressWarnings("serial")
@WebServlet("/rule.do")
/**
 * 
 * @author 胡天天 2014-11-8
 * 法规相关servlet
 *
 */
public class RuleServlet extends FrameworkServlet{
	private static IRule iRule;
	private static Rule rule;
	private static User user; 
	private static RuleSort ruleSort;
	private static IRuleSort iRuleSort;
	private static Pager page;
	public RuleServlet(){
		iRule = new RuleService();
		rule = new Rule();
		user = new User();
		ruleSort = new RuleSort();
		iRuleSort = new RuleSortService();
		page = new Pager();
	}
	/**
	 * 添加一个rule到数据库
	 * @param req
	 * @param res
	 * @return 
	 */
	public String createRule(HttpServletRequest req,HttpServletResponse res){
		String rulename = req.getParameter("ruleName");
		String ruleLink = req.getParameter("ruleLink");
		//将获得的名称和链接存入rule对象
		rule.setTitle(rulename);     						
		rule.setLink(ruleLink);								
		user=(User)req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		//将获得的user对象存入rule对象
		rule.setUser(user);	
		//获取页面单选框的值,为该法规所属分类的id
		String rulesortid = req.getParameter("ruleSortId");
		ruleSort.setSortId(Integer.parseInt(rulesortid));
		//通过只有id的ruleSort找到整个ruleSort对象
		ruleSort = iRuleSort.findRuleSortById(ruleSort); 
		//当增加一个rule时对应的rulesort数目也应该增加一个,应该用事务实现
		/*rulesort.setRuleNum(rulesort.getRuleNum()+1);  
		irulesort.updateRuleSortNum(rulesort);*/		
		rule.setRuleSort(ruleSort);
		iRule.addRules(rule);
		return "rule.do?action=findAllRule";
	}
	/**
	 * 更新数据库中的一条rule
	 * @param req
	 * @param res
	 * @return
	 */
	public String updateRule(HttpServletRequest req,HttpServletResponse res){
		String rulename = req.getParameter("ruleName");
		String ruleLink = req.getParameter("ruleLink");
		//获取页面单选框的值,为该法规所属分类的id
		String rulesortid = req.getParameter("ruleSortId");
		//获得页面的ruleId
		String ruleId = req.getParameter("ruleId");	
		//获取修改前该rule的sortId
		int oSortId = Integer.parseInt(req.getParameter("oSortId"));
		user=(User)req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		int id = Integer.parseInt(rulesortid);
		rule.setUser(user);
		ruleSort.setSortId(id);
		//通过只有id的rulesort找到整个rulesort对象
		ruleSort = iRuleSort.findRuleSortById(ruleSort);   
		rule.setRuleSort(ruleSort);
		rule.setRuleId(Integer.parseInt(ruleId));
		rule.setTitle(rulename);
		rule.setLink(ruleLink);
		iRule.updateRule(rule,oSortId);
		return "rule.do?action=findAllRule";
	}
	/**
	 * 得到数据库中所有的rule
	 * @param req
	 * @param res
	 * @return rulelist集合
	 */
	public String findAllRule(HttpServletRequest req,HttpServletResponse res){
		//分页
		int currentPage=1;
		if(req.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
		}
		//得到rule的中行数
		int totalCount = iRule.getAllCount();
		page.paging(currentPage,  ParamUtil.RULE_PAGE_SIZE, totalCount);
		page = iRule.findAllRule(page);
		req.setAttribute("page", page);
		return "bclient/rule/findAllRule.jsp";
	}
	/**
	 * 接受要修改的rule，通过id来获取rule对象对它进行修改
	 * @param req
	 * @param res
	 * @return
	 */
	public String editRule(HttpServletRequest req,HttpServletResponse res){
		//根据ruleId获取整个rule
		rule=iRule.findRuleById(Integer.parseInt(req.getParameter("ruleId")));
		req.setAttribute("rule", rule);
		//多态调用ruleSortService——ruleSortDao——basedao返回数据库所有的ruleSort
		List<RuleSort> rulesortlist = iRuleSort.findAllRuleSort(); 
		req.setAttribute("list", rulesortlist);  
		return "bclient/rule/updateRule.jsp";
	}
	/**
	 * 根据所选复选框的ruleid删除数据库中的rule
	 * @param req
	 * @param res
	 * @return
	 */
	public String deleteRule(HttpServletRequest req,HttpServletResponse res){
		String[] ruleid = req.getParameterValues("ruleId");
		for(int i=0;i<ruleid.length;i++){
			int id=Integer.parseInt(ruleid[i]);
			rule=iRule.findRuleById(id);
			iRule.deleteRule(rule);
		}
		return "rule.do?action=findAllRule";
	}
}