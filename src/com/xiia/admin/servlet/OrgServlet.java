package com.xiia.admin.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.OrgService;
import com.xiia.service.inter.IOrg;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.Org;
import com.xiia.vo.User;

/**
 * 分支机构相关servlet
 * @author 胡天天2014-11-10
 *
 */
@SuppressWarnings("serial")
@WebServlet("/org.do")
public class OrgServlet extends FrameworkServlet{
	private static IOrg iOrg;
	private static Org org;
	private static User user; 
	private static Pager page;
	public OrgServlet(){
		iOrg = new OrgService();
		org = new Org();
		user = new User();
		page = new Pager();
	}
	/**
	 * 新建一个分支机构org
	 * @param req
	 * @param res
	 * @return org的列表
	 */
	public String createOrg(HttpServletRequest req,HttpServletResponse res){
		String orgname = req.getParameter("orgName");
		String orglink = req.getParameter("orgLink");
		user=(User)req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		org.setOrgName(orgname);
		org.setLink(orglink);
		org.setUser(user);
		org.setStatus(1);
		iOrg.addOrg(org);
		return "org.do?action=findAllOrg";
	}
	/**
	 * 删除一个分支机构
	 * @param req
	 * @param res
	 * @return org的列表
	 */
	public String deleteOrg(HttpServletRequest req,HttpServletResponse res){
		//获取复选框中的内容 存放在字符串数组之中
		String[] orgid = req.getParameterValues("orgId"); 
		for(int i=0;i<orgid.length;i++){
			int id=Integer.parseInt(orgid[i]);
			org.setOrgId(id);
			iOrg.deleteOrg(org);
		}
		return "org.do?action=findAllOrg";
	}
	/**
	 * 更新一个分支机构
	 * @param req
	 * @param res
	 * @return org列表
	 */
	public String updateOrg(HttpServletRequest req,HttpServletResponse res){
		int orgid = Integer.parseInt(req.getParameter("orgId"));
		String orgname = req.getParameter("orgName");
		String orglink = req.getParameter("orgLink");
		user=(User)req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		org.setOrgId(orgid);
		org.setOrgName(orgname);
		org.setLink(orglink);
		org.setUser(user);
		iOrg.updateOrg(org);
		return "org.do?action=findAllOrg";
	}
	/**
	 * 查看数据库中所有的分支机构
	 * @param req
	 * @param res
	 * @return
	 */
	public String findAllOrg(HttpServletRequest req,HttpServletResponse res){
		//分页
		int currentPage=1;
		if(req.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
		}
		//得到Org的总行数
		int totalCount=iOrg.getAllCount();
		page.paging(currentPage, ParamUtil.ORG_PAGE_SIZE, totalCount);
		page = iOrg.findAllOrg(page);
		req.setAttribute("page",page);
		return  "bclient/org/findAllOrg.jsp";
	}
	/**
	 * 通过orgId获得整个org对象
	 * @param req
	 * @param res
	 * @return 编辑org页面
	 */
	public String editOrg(HttpServletRequest req,HttpServletResponse res){
		int orgid = Integer.parseInt(req.getParameter("orgId"));
		org =iOrg.findOrgById(orgid);
		req.setAttribute("org", org);
		return "bclient/org/updateOrg.jsp";
	}
}