package com.xiia.admin.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.SettingService;
import com.xiia.service.inter.ISetting;
import com.xiia.vo.Setting;

/**
 * 2014-11-4
 * @author 王书恒
 *系统设置后台Servlet
 */
@WebServlet("/setting.do")
public class SettingServlet extends FrameworkServlet{
	private static ISetting iSetting;
	private static final long serialVersionUID = 1L;
	public SettingServlet(){
		iSetting =  new SettingService();
	}
	/**
	 * 跳转到修改系统参数的界面
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String toUpdateSettingUI(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//得到当前的系统参数
		Setting setting = iSetting.getSetting();
		request.setAttribute("setting", setting);
		return "bclient/setting/updateSetting.jsp";
	}
	
	/**
	 * 更新系统参数
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String updateSetting(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Setting setting = doForm(request);
		if(iSetting.updateSetting(setting)){
			resString = "变更系统参数成功";
		}else{
			resString = "变更系统参数失败";
		}
		request.setAttribute("resString", resString);
		return toUpdateSettingUI(request, response);
	}
	/**
	 * 封装系统参数
	 * @param r
	 * @return
	 */
	private Setting doForm(HttpServletRequest r){
		Setting s = new Setting();
		if(null != r.getParameter("settingId"))
			s.setSettingId(Integer.parseInt(r.getParameter("settingId").trim()));
		if(null != r.getParameter("menmberScore"))
			s.setMenmberScore(Integer.parseInt(r.getParameter("menmberScore").trim()));
		if(null != r.getParameter("adminScore"))
			s.setAdminScore(Integer.parseInt(r.getParameter("adminScore").trim()));
		if(null != r.getParameter("menuMaxCount"))
			s.setMenuMaxCount(Integer.parseInt(r.getParameter("menuMaxCount").trim()));
		if(null != r.getParameter("headlineNewsNum"))
			s.setHeadlineNewsNum(Integer.parseInt(r.getParameter("headlineNewsNum").trim()));
		if(null != r.getParameter("uploadResourceSize"))
			s.setUploadResourceSize(Integer.parseInt(r.getParameter("uploadResourceSize").trim()));
		if(null != r.getParameter("acceptUploadFileType"))
			s.setAcceptUploadFileType(r.getParameter("acceptUploadFileType"));
		
		return s;
	}
}
