package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NoticeService;
import com.xiia.servcie.imp.NoticeSortService;
import com.xiia.service.inter.INotice;
import com.xiia.service.inter.INoticeSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.util.StringUtil;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;
import com.xiia.vo.User;
/**
 * 
 * @author zhouhao 2014-11-4 10:42
 * NoticeSort的后台servlet
 */
@WebServlet("/noticeSort.do")
public class NoticeSortServlet extends FrameworkServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private INoticeSort iNoticeSort;
	private INotice iNotice;
	private Pager page;
	
	public NoticeSortServlet(){
		iNoticeSort = new NoticeSortService();
		iNotice = new NoticeService();
		page = new Pager();
	}
	
	public String toNoticeSortManagerUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getNoticeSortNum();
		page.paging(currentPage, 10, totalCount);
		page = iNoticeSort.listNoticeSort(page);
		request.setAttribute("page", page);
		return "bclient/notice/module/noticeSortManager.jsp";
	}
	public String toAddNoticeSortUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		return "bclient/notice/edit/addNoticeSort.jsp";
	}
	public String toAddNoticeUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<NoticeSort> noticeSorts = iNoticeSort.listNoticeSort();
		if (noticeSorts != null){
			request.setAttribute("noticeSorts", noticeSorts);
		}
		return "bclient/notice/edit/addNotice.jsp";
	}
	/**
	 * 创建新的公告分类
	 * @param request
	 * @param response
	 * @return 
	 * @throws ServletException
	 * @throws IOException
	 */
	public String createNoticeSort(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		Date time = new Date();
		noticeSort.setCreateTime(time);
		noticeSort.setLastUpdateTime(time);
		noticeSort.setNoticeNum(0);  //新建公告分组内公告数为0
		User user = (User)request.getSession().getAttribute(ParamUtil.LOGIN_USER);
		noticeSort.setUserId(user.getUserId());
		if (iNoticeSort.addNoticeSort(noticeSort)){
			resString = "创建公告分类成功！";
		}else{
			resString = "创建公告分类失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeSortManagerUI(request, response);
	}
	/**
	 * 新建公告
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String createNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (iNotice.getNoticeSortNum() > 0){
			Notice notice = getNoticeFromRequest(request);
			Date time = new Date();
			notice.setAddTime(time);
			notice.setLastUpdateTime(time);
			notice.setStatus(1);  //撰写完成状态
			//公告分类id来自页面输入
			User user = (User)request.getSession().getAttribute(ParamUtil.LOGIN_USER);
			notice.setUserId(user.getUserId());
			if (iNotice.addNotice(notice)){
				resString = "创建公告成功！";
			}else{
				resString = "创建公告失败！";
			}
		}else{
			resString = "当前没有公告分类，请先创建公告分类！";
		}
		request.setAttribute("resString", resString);
		return toNoticeSortManagerUI(request, response);
	}
	/**
	 * 删除公告分类
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String deleteNoticeSort(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		noticeSort = iNoticeSort.getNoticeSortById(noticeSort.getSortId());
		if (iNoticeSort.deleteNoticeSort(noticeSort)){
			resString = "删除公告分类成功！";
		}else{
			resString = "删除公告分类失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeSortManagerUI(request, response);
	}
	public String showNoticeList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		noticeSort = iNoticeSort.getNoticeSortById(noticeSort.getSortId());
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getEditableNoticeNum(noticeSort);
		page.paging(currentPage, 10, totalCount);
		page = iNotice.getEditableNotice(noticeSort, page);
		request.setAttribute("page", page);
		request.setAttribute("noticeSort", noticeSort);
		return "bclient/notice/edit/noticeList.jsp";
	}
	/**
	 * 更改公告分类
	 * @param request
	 * @param response
	 * @return 
	 * @throws ServletException
	 * @throws IOException
	 */
	public String toUpdateNoticeSortUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		if (noticeSort.getSortId() != 0){
			noticeSort = iNoticeSort.getNoticeSortById(noticeSort.getSortId());
			request.setAttribute("noticeSort", noticeSort);
		}
		return "bclient/notice/edit/updateNoticeSort.jsp";
	}
	public String updateNoticeSort(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		noticeSort.setLastUpdateTime(new Date());
		String resString = null;
		if (iNoticeSort.updateNoticeSort(noticeSort)){
			resString = "修改公告分类成功！";
		}else{
			resString = "修改公告分类失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeSortManagerUI(request, response);
	}
	/**
	 * 从http请求中创建NoticeSort对象
	 * @param request
	 * @return 返回NoticeSort对象
	 * @throws ServletException
	 * @throws IOException
	 */
	private NoticeSort getNoticeSortFromRequest(HttpServletRequest request) throws ServletException, IOException {
		NoticeSort noticeSort = new NoticeSort();
		if (request.getParameter("sortId") != null){
			noticeSort.setSortId(Integer.parseInt(request.getParameter("sortId")));
		}if (request.getParameter("sortName") != null){
			noticeSort.setSortName(request.getParameter("sortName").trim());
		}if (request.getParameter("noticeNum") != null){
			noticeSort.setNoticeNum(Integer.parseInt(request.getParameter("noticeNum")));
		}if (request.getParameter("note") != null){
			noticeSort.setNote(request.getParameter("note").trim());
		}if (request.getParameter("createTime") != null){
			noticeSort.setCreateTime(StringUtil.toDate(request.getParameter("createTime")));
		}if (request.getParameter("lastUpdateTime") != null){
			noticeSort.setLastUpdateTime(StringUtil.toDate(request.getParameter("lastUpdateTime").trim()));
		}if (request.getParameter("userId") != null){
			noticeSort.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return noticeSort;
	}
	private Notice getNoticeFromRequest(HttpServletRequest request) throws ServletException, IOException {
		Notice notice = new Notice();
		if (request.getParameter("noticeId") != null){
			notice.setNoticeId(Integer.parseInt(request.getParameter("noticeId")));
		}if (request.getParameter("title") != null){
			notice.setTitle(request.getParameter("title").trim());
		}if (request.getParameter("content") != null){
			notice.setContent(request.getParameter("content").trim());
		}if (request.getParameter("addTime") != null){
			notice.setAddTime(StringUtil.toDate(request.getParameter("addTime")));
		}if (request.getParameter("lastUpdateTime") != null){
			notice.setLastUpdateTime(StringUtil.toDate(request.getParameter("lastUpdateTime").trim()));
		}if (request.getParameter("status") != null){
			notice.setStatus(Integer.parseInt(request.getParameter("status")));
		}if (request.getParameter("sortId") != null){
			notice.setSortId(Integer.parseInt(request.getParameter("sortId")));
		}if (request.getParameter("userId") != null){
			notice.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return notice;
	}
}
