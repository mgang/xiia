package com.xiia.admin.servlet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.MenuService;
import com.xiia.service.inter.IMenu;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.util.StringUtil;
import com.xiia.vo.Menu;
import com.xiia.vo.User;

/**
 * 2014 11/3 18:38
 * 
 * @author 李跃磊
 *
 */
@WebServlet("/menu.do")
public class MenuServlet extends FrameworkServlet {
	private static final long serialVersionUID = 1L;
	private IMenu iMenu = new MenuService();
	private Pager page = new Pager();

	/**
	 * 增加栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String addMenu(HttpServletRequest request,
			HttpServletResponse response) {
		Menu menu = getMenuRequest(request, response);
		Menu menu1 = iMenu.addMenu(menu);
		if (menu1 != null) {
			request.setAttribute("message", "添加栏目成功");
			return searchShowForntMenu(request, response);
		}
		request.setAttribute("message", "添加栏目失败");
		return searchShowForntMenu(request, response);
	}

	/**
	 * 查找用户自己的栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String searchMenuByUser(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		User currenUser = (User) session.getAttribute(ParamUtil.LOGIN_USER);
		List<Menu> menus = iMenu.selectMenu(currenUser);
		request.setAttribute("allMenu", menus);
		for (int i = 1; i <= 6; i++) {
			List<Menu> list = iMenu.selectMenu(i, currenUser);
			if (list != null) {
				request.setAttribute("menus" + i, list);
			} else {
				request.setAttribute("menus" + i, null);
			}
		}
		if (menus != null) {
			return "bclient/menu/showAllMenu.jsp";
		}
		return "error.jsp";
	}

	/**
	 * 根据状态查询栏目（注：适用前台查询已发布的栏目）
	 * 
	 * @param request
	 * @param response
	 * @return 栏目对象集合
	 */
	public String searchShowForntMenu(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, List<Menu>> map = iMenu.selectMenuByOrder();
		List<Menu> fatherMenu = new ArrayList<Menu>();
		if (map == null) {
			request.setAttribute("fatherMenu", null);
			return "bclient/menu/addMenu.jsp";
		}
		fatherMenu = map.get("fatherMenu");
		List<Menu> fatherMenu1 = new ArrayList<Menu>();
		for (Menu menu : fatherMenu) {
			menu.setChildMenu(map.get(menu.getMenuName()));
			fatherMenu1.add(menu);
		}
		request.setAttribute("fatherMenu", fatherMenu1);
		return "bclient/menu/addMenu.jsp";
	}

	/**
	 * 删除栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String deleteMenu(HttpServletRequest request,
			HttpServletResponse response) {
		List<Menu> menus = new ArrayList<Menu>();
		if (request.getParameterValues("checkMenu") != null) {
			String[] id = request.getParameterValues("checkMenu");
			for (int i = 0; i < id.length; i++) {
				Menu menu = new Menu();
				menu.setMenuId(StringUtil.stringToInt(id[i]));
				menus.add(menu);
			}
			if (iMenu.deleteMenu(menus)) {
				request.setAttribute("message", "删除成功");
				if (request.getParameter("s") != null) {
					if (request.getParameter("s").equals("2")) {
						return showCanUpdateMenu(request, response);
					}
					if (request.getParameter("s").equals("3")) {
						return "menu.do?action=showMenuBystatus&status=4";
					}
				}
				return "menu.do?action=showMenuBystatus&status=1";
			}
		} else {
			Menu menu = new Menu();
			if (request.getParameter("menuId") != null) {
				menu.setMenuId(StringUtil.stringToInt(request
						.getParameter("menuId")));
				menus.add(menu);
			}
			if (iMenu.deleteMenu(menus)) {
				request.setAttribute("message", "删除成功");
				if (request.getParameter("s") != null) {
					if (request.getParameter("s").equals("2")) {
						return showCanUpdateMenu(request, response);
					}
					if (request.getParameter("s").equals("3")) {
						return "menu.do?action=showMenuBystatus&status=4";
					}
				}
				return "menu.do?action=showMenuBystatus&status=1";
			}
		}
		request.setAttribute("message", "删除失败");
		return "menu.do?action=showMenuBystatus&status=1";
	}

	/**
	 * 查询所有状态栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String showAllMenu(HttpServletRequest request,
			HttpServletResponse response) {
		List<Menu> menus = iMenu.selectMenu();
		request.setAttribute("allMenu", menus);
		searchShowForntMenu(request, response);
		return "bclient/menu/updateMenu.jsp";
	}

	/**
	 * 修改栏目的名称或者链接
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public String updateNameAndLink(HttpServletRequest request,
			HttpServletResponse response) {
		Date date = new Date();
		List<Menu> menus = new ArrayList<Menu>();
		if (request.getParameter("menuId") != null) {
			int menuId = StringUtil.stringToInt(request.getParameter("menuId"));
			String menuName = request.getParameter("menuName");
			String link = request.getParameter("link");
			Menu menu = new Menu();
			menu.setMenuName(menuName);
			menu.setMenuId(menuId);
			menu.setLink(link);
			menu.setLastUpdateTime(date);
			menus.add(menu);
			if (iMenu.updateMenu(menus)) {
				request.setAttribute("message", "修改栏目名称和链接成功");
				if (request.getParameter("flag").equals("notpass")) {
					return "menu.do?action=showMenuBystatus&status=4";
				}
				request.setAttribute("message", "修改栏目名称和链接成功");
				return "menu.do?action=showCanUpdateMenu";
			}
		} else {
			String[] id = request.getParameterValues("checkMenu");
			String name = request.getParameter("params");
			String[] m = name.split(",");
			for (int i = 0; i < id.length; i++) {
				Menu menu = new Menu();
				menu.setMenuId(StringUtil.stringToInt(id[i]));
				menu.setMenuName(m[2 * i]);
				menu.setLink(m[2 * i + 1]);
				menu.setLastUpdateTime(date);
				menus.add(menu);
			}
			if (iMenu.updateMenu(menus)) {
				request.setAttribute("message", "修改栏目名称和链接成功");
				if (request.getParameter("flag").equals("notpass")) {
					return "menu.do?action=showMenuBystatus&status=4";
				}
				request.setAttribute("message", "修改栏目名称和链接成功");
				return "menu.do?action=showCanUpdateMenu";
			}
			request.setAttribute("message", "修改栏目名称和链接失败");
			return "menu.do?action=showCanUpdateMenu";
		}
		request.setAttribute("message", "修改栏目名称和链接失败");
		return "menu.do?action=showCanUpdateMenu";
	}

	/**
	 * 修改栏目显示顺序（发布在前台的栏目）
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String updateDisplayOrder(HttpServletRequest request,
			HttpServletResponse response) {
		Date date = new Date();
		List<Menu> menus = new ArrayList<Menu>();
		if (request.getParameter("menuId") != null) {
			int menuId = StringUtil.stringToInt(request.getParameter("menuId"));
			int order = StringUtil.stringToInt(request
					.getParameter("displayOrder"));
			Menu menu = new Menu();
			menu.setMenuId(menuId);
			menu.setDisplayOrder(order);
			menu.setLastUpdateTime(date);
			menus.add(menu);
			if (iMenu.updateMenuDisplayOrder(menus)) {
				request.setAttribute("message", "修改栏目显示顺序成功");
				return showCanUpdateDisplayOrderMenu(request, response);
			}
		} else {
			String[] order = request.getParameterValues("displayOrder");
			String[] id = request.getParameterValues("checkMenu");
			for (int i = 0; i < id.length; i++) {
				Menu menu = new Menu();
				menu.setMenuId(StringUtil.stringToInt(id[i]));
				menu.setDisplayOrder(StringUtil.stringToInt(order[i]));
				menu.setLastUpdateTime(date);
				menus.add(menu);
			}
			if (iMenu.updateMenuDisplayOrder(menus)) {
				request.setAttribute("message", "修改栏目显示顺序成功");
				return showCanUpdateDisplayOrderMenu(request, response);
			}
		}
		request.setAttribute("message", "修改栏目显示顺序失败");
		return showCanUpdateDisplayOrderMenu(request, response);
	}

	/**
	 * 发送审核请求
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String sendCheckRequest(HttpServletRequest request,
			HttpServletResponse response) {
		List<Menu> menus = new ArrayList<Menu>();
		if (request.getParameterValues("checkMenu") != null) {
			String[] id = request.getParameterValues("checkMenu");
			for (int i = 0; i < id.length; i++) {
				Menu menu = new Menu();
				menu.setMenuId(StringUtil.stringToInt(id[i]));
				menus.add(menu);
			}
			if (iMenu.applyCheckMenu(menus)) {
				request.setAttribute("message", "发送成功");
				return "menu.do?action=showMenuBystatus&status=1";
			}
		} else {
			Menu menu = new Menu();
			if (request.getParameter("menuId") != null) {
				menu.setMenuId(StringUtil.stringToInt(request
						.getParameter("menuId")));
				menus.add(menu);
			}
			if (iMenu.applyCheckMenu(menus)) {
				request.setAttribute("message", "发送成功");
				return "menu.do?action=showMenuBystatus&status=1";
			}

		}
		request.setAttribute("message", "发送失败");
		return "menu.do?action=showMenuBystatus&status=1";
	}

	/**
	 * 得到Request中的Menu信息
	 * 
	 * @param request
	 * @param response
	 * @return 栏目对象
	 */
	public Menu getMenuRequest(HttpServletRequest request,
			HttpServletResponse response) {
		Menu menu = new Menu();
		Date date = new Date();
		if (StringUtil.isNotEmpty(request.getParameter("menuName")))
			menu.setMenuName(request.getParameter("menuName"));
		if (StringUtil.isNotEmpty(request.getParameter("link")))
			menu.setLink(request.getParameter("link"));
		if (StringUtil.isNotEmpty(request.getParameter("menuRank")))
			menu.setMenuRank(StringUtil.stringToInt(request
					.getParameter("menuRank")));
		if (StringUtil.isNotEmpty(request.getParameter("preMenuId")))
			menu.setPreMenuId(StringUtil.stringToInt(request
					.getParameter("preMenuId")));
		menu.setStatus(1);
		menu.setCreateTime(date);
		menu.setLastUpdateTime(date);
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(ParamUtil.LOGIN_USER);
		menu.setUserId(user.getUserId());
		return menu;
	}

	/**
	 * 分页查询所有栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String showAllMenuByPager(HttpServletRequest request,
			HttpServletResponse response) {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage")
					.toString());
		}
		int totalCount = iMenu.countMenu();
		page.paging(currentPage, 8, totalCount);
		page = iMenu.findMenuPage(page);
		request.setAttribute("page", page);
		return "bclient/menu/showAllMenu.jsp";
	}

	/**
	 * 分页查询某个状态的栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String showMenuBystatus(HttpServletRequest request,
			HttpServletResponse response) {
		int currentPage = 1;
		int status = 0;
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage")
					.toString());
		}
		if (request.getParameter("status") != null) {
			status = StringUtil.stringToInt(request.getParameter("status")
					.toString());
		}
		int totalCount = iMenu.countMenuByStatus(status);
		page.paging(currentPage, 10, totalCount);
		page = iMenu.findMenuPage(page, status);
		request.setAttribute("page", page);
		switch (status) {
		case 1:
			return "bclient/menu/notCheck.jsp";
		case 2:
			return "bclient/menu/waitCheck.jsp";
		case 3:
			return "bclient/menu/passCheck.jsp";
		case 4:
			return "bclient/menu/notPassCheck.jsp";
		case 5:
			return "bclient/menu/showFront.jsp";
		case 6:
			return "bclient/menu/showUndo.jsp";
		default:
			return "error.jsp";
		}
	}

	/**
	 * 分页查询你可以修改的栏目
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public String showCanUpdateMenu(HttpServletRequest request,
			HttpServletResponse response) {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage")
					.toString());
		}
		int totalCount = iMenu.countCanUpdateMenu();
		page.paging(currentPage, 8, totalCount);
		page = iMenu.findCanUpdateMenuPage(page);
		request.setAttribute("page", page);
		return "bclient/menu/updateNameAndLink.jsp";
	}

	/**
	 * 显示前台栏目，即是查询可修改显示顺序的栏目
	 * 
	 * @param request
	 * @param response
	 * @return 跳转页面
	 */
	public String showCanUpdateDisplayOrderMenu(HttpServletRequest request,
			HttpServletResponse response) {
		searchShowForntMenu(request, response);
		return "bclient/menu/updateDisplayOrder.jsp";
	}

}
