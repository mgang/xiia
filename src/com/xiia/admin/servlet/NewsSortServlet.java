package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NewsSortService;
import com.xiia.service.inter.INewsSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.NewsSort;
import com.xiia.vo.User;

/**
 * 2014 11 5 9:00
 * @author 陈强
 * 新闻分类servlet类
 */
@WebServlet("/newsSort.do")
public class NewsSortServlet extends FrameworkServlet{
	private static final long serialVersionUID = 1L;
	private static INewsSort iNewsSort = new NewsSortService(); 
	private static NewsSort newsSort = new NewsSort();
	private static User newsUser =new User();
	private static Pager page = new Pager();
	public NewsSortServlet(){
		iNewsSort = new NewsSortService();
		newsSort = new NewsSort();
		newsUser =new User();
		page = new Pager();
	}
	/**
	 * 从jsp页面中获取数据
	 * @param r
	 * @return NewsSort
	 */
	private NewsSort doForm(HttpServletRequest r){
		NewsSort sort = new NewsSort();
		User u = null;
		u = (User) r.getSession().getAttribute("user");
		sort.setUser(u);
		if(null != r.getParameter("sortName"))
			sort.setSortName(r.getParameter("sortName"));
		if(null != r.getParameter("sortNote"))
			sort.setNote(r.getParameter("sortNote"));
		if(null!= r.getParameter("sortId"))
			sort.setSortId(Integer.parseInt(r.getParameter("sortId")));
		return sort;
	}
	/**
	 * 新建新闻分类
	 * @param req
	 * @param res
	 * @return String
	 */
	public String createNewsSort(HttpServletRequest req, HttpServletResponse res){
		newsSort = doForm(req);
		newsUser = (User) req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		newsSort.setUser(newsUser);
		iNewsSort.createNewSort(newsSort);
		return "newsSort.do?action=newsSortListTrue";
	}
	/**
	 * 拿到新闻分类的集合并封装到Pager
	 * @param req
	 * @param res
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String newsSortListTrue(HttpServletRequest req,
		HttpServletResponse res) throws ServletException, IOException {
		List<NewsSort> newsSortList = new ArrayList<NewsSort>();
		newsSortList = iNewsSort.findAllSort();
		//分页
		int currentPage=1;
		if(req.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
		}
		//得到News的总行数
		int totalCount=iNewsSort.getCount();
		page.paging(currentPage, ParamUtil.NEWS_SORT_PAGE_SIZE, totalCount);
		page =iNewsSort.findNewsSortPage(page);
		req.setAttribute("page",page);
		req.setAttribute("newsSortList", newsSortList);
		NewsSort sort = new NewsSort();
		if(null!=req.getParameter("sortId")){
			int sortId = Integer.parseInt(req.getParameter("sortId"));
			sort = iNewsSort.findNewsSortById(sortId);
			req.setAttribute("sort", sort);
		}
		return "bclient/news/newsSortList.jsp";
	}
	/**
	 * 拿到新闻分类并封装到Pager中只用于新建新闻中选择新闻分类
	 * @param req
	 * @param res
	 * @return String
	 * @throws ServletException
	 * @throws IOException
	 */
	public String newsSortListFalse(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
			List<NewsSort> newsSortList = new ArrayList<NewsSort>();
			newsSortList = iNewsSort.findAllSort();
			//分页
			int currentPage=1;
			if(req.getParameter("currentPage")!=null){
				currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
			}
			//得到News的总行数
			int totalCount=iNewsSort.getCount();
			page.paging(currentPage, ParamUtil.NEWS_SORT_PAGE_SIZE, totalCount);
			page =iNewsSort.findNewsSortPage(page);
			req.setAttribute("page",page);
			req.setAttribute("newsSortList", newsSortList);
			NewsSort sort = new NewsSort();
			if(null!=req.getParameter("sortId")){
				int sortId = Integer.parseInt(req.getParameter("sortId"));
				sort = iNewsSort.findNewsSortById(sortId);
				req.setAttribute("sort", sort);
			}
			return "bclient/news/addNews.jsp";
		}
	/**
	 * 删除新闻分类
	 * @param req
	 * @param res
	 * @return String
	 */
	public String deleteNewsSort(HttpServletRequest req,
			HttpServletResponse res){
		String arrString = req.getParameter("arr");
		String[] a = arrString.split(",");
		for(int i=1;i<a.length;i++){
			NewsSort newsSort = new NewsSort();
			int sortId = Integer.parseInt(a[i]);
			newsSort = iNewsSort.findNewsSortById(sortId);
			iNewsSort.deleteSort(newsSort);
		}
		return "newsSort.do?action=newsSortListTrue";
	}
	/**
	 * 查看新闻分类的信息
	 * @param req
	 * @param res
	 * @return String
	 */
	public String showNewsSort(HttpServletRequest req,
			HttpServletResponse res){
		int sortId = Integer.parseInt(req.getParameter("sortId"));
		NewsSort newsSort = iNewsSort.findNewsSortById(sortId);
		req.setAttribute("sort", newsSort);
		return "bclient/news/updateNewsSort.jsp";
	}
	/**
	 * 修改新闻分类
	 * @param req
	 * @param res
	 * @return String
	 */
	public String updateNewsSort(HttpServletRequest req,
			HttpServletResponse res){
		newsSort = doForm(req);
		newsUser = (User) req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		newsSort.setUser(newsUser);
		iNewsSort.updateSort(newsSort);
		return "newsSort.do?action=newsSortListTrue";
	}
}
