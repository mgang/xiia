package com.xiia.admin.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NewsService;
import com.xiia.servcie.imp.NewsSortService;
import com.xiia.service.inter.INews;
import com.xiia.service.inter.INewsSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.Check;
import com.xiia.vo.News;
import com.xiia.vo.NewsSort;
import com.xiia.vo.User;
/**
 * 2014 11 5 15:12
 * @author 陈强
 * 新闻servlet
 */
@WebServlet("/news.do")
public class NewsServlet extends FrameworkServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static INews iNews ;
	private static INewsSort iNewsSort ;
	private static Pager page ;
	public NewsServlet(){
		iNews = new NewsService();
		iNewsSort = new NewsSortService();
		page = new Pager();
	}
	/**
	 * 拿到从jsp中传过来的参数
	 * @param r
	 * @return News
	 */
	private News doForm(HttpServletRequest r){
		News news = new News();
		NewsSort sort = null;
		User u = null;
		u = (User) r.getSession().getAttribute("user");
		news.setUser(u);
		if(null != r.getParameter("newsTitle"))
			news.setTitle(r.getParameter("newsTitle"));
		if(null != r.getParameter("content"))
			news.setContent(r.getParameter("content"));
		if(null != r.getParameter("newsSortId"))
		{
			int sortId = Integer.parseInt(r.getParameter("newsSortId"));
			sort = iNewsSort.findNewsSortById(sortId);
			news.setNewsSort(sort);
		}
		return news;
	}
	/**
	 * 添加新闻
	 * @param req
	 * @param res
	 * @return String
	 */
	public String addNews(HttpServletRequest req, HttpServletResponse res){
		News news = new News();
		User u= null;
		u = (User) req.getSession().getAttribute(ParamUtil.LOGIN_USER);

		news = doForm(req);
		news.setUser(u);
		iNews.addNews(news);
		return "news.do?action=newsList&&newsSortId="+news.getNewsSort().getSortId() ;
	}
	/**
	 * 通过新闻分类来拿到新闻集合列表
	 * @param req
	 * @param res
	 * @return String
	 */
	public String newsList(HttpServletRequest req, HttpServletResponse res){
		int sortId = Integer.parseInt(req.getParameter("newsSortId"));
		NewsSort sort = new NewsSort();
		sort = iNewsSort.findNewsSortById(sortId);
		//分页
		int currentPage=1;
		if(req.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
		}
		//得到News的总行数
		int totalCount=iNews.getCount(sort);
		page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
		page =iNews.findNewsPage(page, sort);
		req.setAttribute("page",page);
		List<News> newsList = iNews.findNewsBySort(sort);
		req.setAttribute("newsList", newsList);
		req.setAttribute("sort",sort);
		return  "bclient/news/newsList.jsp";
	}
	/**
	 * 拿到所有新闻的集合列表
	 * @param req
	 * @param res
	 * @return String
	 */
	public String allNewsList(HttpServletRequest req, HttpServletResponse res){
		int currentPage=1;
		if(req.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
		}
		//得到News的总行数
		int totalCount=iNews.getAllNewsCount();
		page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
		page =iNews.findAllNewsPage(page);
		req.setAttribute("page",page);
		return  "bclient/news/allNewsList.jsp";
	}
	/**
	 * 删除新闻
	 * @param req
	 * @param res
	 * @return String
	 */
	public String deleteNews(HttpServletRequest req,
			HttpServletResponse res){
		String arrString = req.getParameter("arr");
		if(null!=req.getParameter("newsSortId")){
			int sortId = Integer.parseInt(req.getParameter("newsSortId"));
			String[] a = arrString.split(",");
			for(int i=1;i<a.length;i++){
				News  news = new News();
				int newsId = Integer.parseInt(a[i]);
				news = iNews.findNews(newsId);
				iNews.deleteNews(news);
			}
			return "news.do?action=newsList&newsSortId="+sortId;
		}
		else{
			String[] a = arrString.split(",");
			for(int i=1;i<a.length;i++){
				News  news = new News();
				int newsId = Integer.parseInt(a[i]);
				news = iNews.findNews(newsId);
				iNews.deleteNews(news);
			}
			return "news.do?action=allNewsList";
		}
			
		
	}
	/**
	 * 查看新闻的内容
	 * @param req
	 * @param res
	 * @return String
	 */
	public String showNews(HttpServletRequest req,
			HttpServletResponse res){
		int newsId = Integer.parseInt(req.getParameter("newsId"));
		if(null!=req.getParameter("newsSortId")){
			int sortId = Integer.parseInt(req.getParameter("newsSortId"));
			List<NewsSort> newsSortList = new ArrayList<NewsSort>();
			newsSortList = iNewsSort.findAllSort();
			News news  = iNews.findNews(newsId);
			NewsSort sort = iNewsSort.findNewsSortById(sortId);
			req.setAttribute("news", news);
			req.setAttribute("sort", sort);
			req.setAttribute("newsSortList", newsSortList);
			return "bclient/news/updateNews.jsp";
		}
		else{
			List<NewsSort> newsSortList = new ArrayList<NewsSort>();
			newsSortList = iNewsSort.findAllSort();
			News news  = iNews.findNews(newsId);
			int sortId = news.getSortId();
			NewsSort sort = iNewsSort.findNewsSortById(sortId);
			req.setAttribute("news", news);
			req.setAttribute("sort", sort);
			req.setAttribute("newsSortList", newsSortList);
			return "bclient/news/updateNews.jsp";
		}
	}
	/**
	 * 置顶新闻
	 * @param req
	 * @param res
	 * @return String
	 */
	public String topNews(HttpServletRequest req,
			HttpServletResponse res){
			News  news = new News();
			int newsId = Integer.parseInt(req.getParameter("newsId"));
			if(null!=req.getParameter("newsSortId")){
				int sortId = Integer.parseInt(req.getParameter("newsSortId"));
				news = iNews.findNews(newsId);
				iNews.updateNewsTopFlag(news);
				return "news.do?action=newsList&newsSortId="+sortId;
			}
			else{
				news = iNews.findNews(newsId);
				iNews.updateNewsTopFlag(news);
				return "news.do?action=allNewsList";
			}
			
	}
	/**
	 * 取消置顶
	 * @param req
	 * @param res
	 * @return String
	 */
	public String deleteTopNews(HttpServletRequest req,
			HttpServletResponse res){
		News  news = new News();
		int newsId = Integer.parseInt(req.getParameter("newsId"));
		if(null!=req.getParameter("newsSortId")){
			int sortId = Integer.parseInt(req.getParameter("newsSortId"));
			news = iNews.findNews(newsId);
			iNews.deleteNewsTopFlag(news);
			return "news.do?action=newsList&newsSortId="+sortId;
		}
		else{
			news = iNews.findNews(newsId);
			iNews.deleteNewsTopFlag(news);
			return "news.do?action=allNewsList";
		}
	}
	/**
	 * 修改新闻
	 * @param req
	 * @param res
	 * @return String
	 */
	public String updateNews(HttpServletRequest req,
			HttpServletResponse res){
		int newsId = Integer.parseInt(req.getParameter("newsId"));
		int sortId = Integer.parseInt(req.getParameter("newsSortId"));
		News news = iNews.findNews(newsId);
		NewsSort oldSort = iNewsSort.findNewsSortById(news.getSortId());
		NewsSort sort =iNewsSort.findNewsSortById(sortId);
		news.setNewsSort(sort);
		news.setContent(req.getParameter("content"));
		news.setTitle(req.getParameter("newsTitle"));
		iNews.updateNewsS(news,oldSort);
		return "news.do?action=newsList&&newsSortId="+news.getNewsSort().getSortId() ;
	}
	/**
	 * 修改新闻状态
	 * @param req
	 * @param res
	 * @return String
	 */
	public String updateNewsStatus(HttpServletRequest req,
			HttpServletResponse res){
		String arrString = req.getParameter("arr");
		String[] a = arrString.split(",");
		for(int i=1;i<a.length;i++){
			News  news = new News();
			int newsId = Integer.parseInt(a[i]);
			news = iNews.findNews(newsId);
			news.setStatus(2);
			iNews.releaseNews(news);
			if(iNews.findCheckByNewsId(news))
				iNews.addCheck(news);
			else
				iNews.updateCheck(news);
		}
		if(null!=req.getParameter("newsSortId")){
			int sortId = Integer.parseInt(req.getParameter("newsSortId"));
			return "news.do?action=newsList&newsSortId="+sortId;
		}
		else
			return "news.do?action=allNewsList";
	}
	/**
	 * 查看新闻未通过集合
	 * @param req
	 * @param res
	 * @return String
	 */
	public String noPass(HttpServletRequest req,
			HttpServletResponse res){
			//分页
			int currentPage=1;
			if(req.getParameter("currentPage")!=null){
				currentPage=Integer.parseInt(req.getParameter("currentPage").toString());
			}
			//得到News的总行数
			int totalCount=iNews.getCountNoPass();
			page.paging(currentPage, ParamUtil.NEWS_PAGE_SIZE, totalCount);
			page =iNews.findNewsNoPassPage(page);
			List<News> newsList = page.getList();
			for(News news:newsList){
				Check check = iNews.findCheckByNewsIdCheck(news);
				news.setMes(check.getMsg());
			}
			page.setList(newsList);
			req.setAttribute("page",page);
			return  "bclient/news/noPass.jsp";
	}
	/**
	 * 查看发送审核新闻
	 * @param req
	 * @param res
	 * @return String
	 */
	public String showCheckNews(HttpServletRequest req,
			HttpServletResponse res){
		int newsId = Integer.parseInt(req.getParameter("newsId"));
		News news  = iNews.findNews(newsId);
		req.setAttribute("news", news);
		return "bclient/news/updateCheckNews.jsp";
	}
	/**
	 * 修改审核未通过的新闻
	 * @param req
	 * @param res
	 * @return String
	 */
	public String updateCheckNews(HttpServletRequest req,
			HttpServletResponse res){
		int newsId = Integer.parseInt(req.getParameter("newsId"));
		News news = iNews.findNews(newsId);
		news.setContent(req.getParameter("content"));
		news.setTitle(req.getParameter("newsTitle"));
		NewsSort sort = iNewsSort.findNewsSortById(news.getSortId());
		news.setNewsSort(sort);
		iNews.updateNews(news);
		return "news.do?action=noPass" ;
	}
	/**
	 * 修改新闻审核的状态
	 * @param req
	 * @param res
	 * @return String
	 */
	public String updateCheckNewsStatus(HttpServletRequest req,
			HttpServletResponse res){
		String arrString = req.getParameter("arr");
		String[] a = arrString.split(",");
		for(int i=1;i<a.length;i++){
			News  news = new News();
			int newsId = Integer.parseInt(a[i]);
			news = iNews.findNews(newsId);
			news.setStatus(2);
			iNews.releaseNews(news);
			if(iNews.findCheckByNewsId(news))
				iNews.addCheck(news);
			else
				iNews.updateCheck(news);
		}
			return "news.do?action=noPass";
	}
	public String lookCheckNews(HttpServletRequest req,
			HttpServletResponse res){
		int newsId = Integer.parseInt(req.getParameter("newsId"));
		News news = iNews.findNews(newsId);
		req.setAttribute("news", news);
		return "bclient/news/lookCheckNews.jsp";
	}
}
