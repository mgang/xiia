package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.RevokeService;
import com.xiia.service.inter.IRevoke;
import com.xiia.util.CheckConstant;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.Revoke;
import com.xiia.vo.User;

/**
 * 
 * @author meigang 2014-11-10 17:51
 *
 */
@WebServlet("/revoke.do")
public class RevokeServlet extends FrameworkServlet{
	private static IRevoke iRevoke;
	private static Pager page;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public RevokeServlet(){
		iRevoke = new RevokeService();
		page = new Pager();
	}
	/**
	 * 查看撤销信息
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String listAll(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		int currentPage=1;
		if(request.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(request.getParameter("currentPage").toString());
		}
		//得到的总行数
		int totalCount=iRevoke.count();
		page.paging(currentPage, ParamUtil.REVOKE_ALL_PAGE_SIZE, totalCount);
		page =iRevoke.findAllRevokePage(page,"type");
		request.setAttribute("page",page);

		return "bclient/check/revokeList.jsp";
	}
	/**
	 * 撤销
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String revoke(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Revoke r = doForm(request);
		r.setUser((User) request.getSession().getAttribute(ParamUtil.LOGIN_USER));
		r.setFlag(CheckConstant.FALG_YES_REVOKE);
		r.setLastRevokeTime(new Date());
		if(iRevoke.revoke(r)){
			resString = "撤销成功";
		}else{
			resString = "撤销失败";
		}
		request.setAttribute("resString", resString);
		return "check.do?action=list&type="+r.getType();
	}
	/**
	 * 封装表单
	 * @param r
	 * @return
	 */
	private Revoke doForm(HttpServletRequest r){
		Revoke re = new Revoke();
		if(null != r.getParameter("revokedId"))
			re.setRevokedId(Integer.parseInt(r.getParameter("revokedId").trim()));
		if(null != r.getParameter("type"))
			re.setType(r.getParameter("type"));
		
		return re;
	}
}
