package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.ResourceSortService;
import com.xiia.service.inter.IResourceSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.ResourceSort;
import com.xiia.vo.User;

/**
 * 2014-11-4
 * @author 王书恒
 *资源分类后台Servlet
 */
@WebServlet("/resourceSort.do")
public class ResourceSortServlet extends FrameworkServlet{
	
	private static final long serialVersionUID = 1L;
	private static IResourceSort iResourceSort;
	private static Pager page;
	
	public ResourceSortServlet(){
		iResourceSort = new ResourceSortService();
		page = new Pager();
	}
	/**
	 * 创建新的资源分类
	 * @param request
	 * @param response
	 * @return 
	 * @throws ServletException
	 * @throws IOException
	 */
	public String addResourceSort(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		ResourceSort rs = doForm(request);
		rs.setAddTime(new Date());
		rs.setUser((User) request.getSession().getAttribute(ParamUtil.LOGIN_USER));
		if(iResourceSort.addResourceSort(rs)){
			resString = "新建分类成功";
		}else{
			resString = "新建分类失败";
		}
		request.setAttribute("resString", resString);
		return "bclient/resource/addResourceSort.jsp";
	
	}
	/**
	 * 查询所有资源分类
	 * @param request
	 * @param response
	 * @return
	 */
	public String list(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int currentPage=1;
		if(request.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(request.getParameter("currentPage").toString());
		}
		//得到ResourceSort的总行数
		int totalCount=iResourceSort.countResourceSort();
		page.paging(currentPage, ParamUtil.RESOURCE_SORT_PAGE_SIZE, totalCount);
		page =iResourceSort.findResourceSortPage(page,"sortId");
		request.setAttribute("page",page);
		
		return "bclient/resource/sortList.jsp";
	}
	
	
	
	/**
	 * 删除资源分类
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */	
	
	public String deleteResourceSort(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResourceSort rs = doForm(request);
		if(iResourceSort.deleteResourceSort(rs)){
			resString = "删除成功";
		}else{
			resString = "删除失败";
		}
		request.setAttribute("resString", resString);
		return list(request, response);
	}	
	
	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String toUpdateSortUI(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResourceSort rs = doForm(request);	
	
		request.setAttribute("resourceSort", rs);
		return "bclient/resource/updateResourceSort.jsp";
	}
	
	/**
	 * 修改资源分类
	 * @param request
	 * @param response
	 * @return 
	 * @throws ServletException
	 * @throws IOException
	 */
	public String updateResourceSort(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResourceSort rs = doForm(request);
		if(iResourceSort.updateResourceSort(rs)){
			resString = "修改成功";
		}else{
			resString = "修改失败";
		}
		request.setAttribute("resString", resString);
		return list(request, response);
	}
		
	
	/**
	 * 表单封装
	 * @param r
	 * @return 返回ResourceSort对象
	 * @throws ServletException
	 * @throws IOException
	 */
	private ResourceSort doForm(HttpServletRequest r) {
		ResourceSort rs = new ResourceSort();
		
		if(null != r.getParameter("sortId"))
			rs.setSortId(Integer.parseInt(r.getParameter("sortId").trim()));
		if (r.getParameter("sortName") != null)
			rs.setSortName(r.getParameter("sortName"));
		if (r.getParameter("resNum") != null)
			rs.setResNum(Integer.parseInt(r.getParameter("resNum").trim()));
		
		return rs;
	}

}
