package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.NoticeService;
import com.xiia.servcie.imp.NoticeSortService;
import com.xiia.service.inter.INotice;
import com.xiia.service.inter.INoticeSort;
import com.xiia.util.CheckConstant;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.util.StringUtil;
import com.xiia.vo.Check;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;
import com.xiia.vo.User;
/**
 * 
 * @author zhouhao 2014-11-4 10:43
 * Notice的后台servlet
 */
@WebServlet("/notice.do")
public class NoticeServlet extends FrameworkServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private INotice iNotice;
	private INoticeSort iNoticeSort;
	private Pager page;
	
	public NoticeServlet(){
		iNotice = new NoticeService();
		iNoticeSort = new NoticeSortService();
		page = new Pager();
	}
	/**
	 * 展示所有的公告或某个公告分类下的所有公告
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public String showNoticeList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeSort noticeSort = getNoticeSortFromRequest(request);
		List<Notice> notices = iNotice.listAllNotice(noticeSort);
		if (notices != null){
			request.setAttribute("notices", notices);
			return null;
		}
		return null;
	}*/
	
	/**
	 * 展示公告详细内容
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String showNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Notice notice = getNoticeFromRequest(request);
		notice = iNotice.showNotice(notice);
		if (notice != null){
			request.setAttribute("notice", notice);
			return "bclient/notice/viewNotice.jsp";
		}
		return null;
	}
	/**
	 * 删除公告
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String deleteNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Notice notice = getNoticeFromRequest(request);
		notice = iNotice.getNoticeById(notice.getNoticeId());
		String resString = null;
		if (iNotice.deleteNotice(notice)){
			resString = "删除公告成功！";
		}else{
			resString = "删除公告失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeListManagerUI(request, response);
	}
	/**
	 * 批量删除公告
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String deleteAllNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] ids = request.getParameterValues("noticeId");
		Notice notice = null;
		boolean isSuccess = true;  //批量删除结果标记
		for (int i = 0; i < ids.length; i++){
			notice = iNotice.getNoticeById(Integer.parseInt(ids[i]));
			if (!iNotice.deleteNotice(notice)){
				isSuccess = false;
				break;
			}
		}
		if (isSuccess){
			resString = "批量删除公告成功！";
		}else{
			resString = "批量删除公告失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeListManagerUI(request, response);
	}
	public String toUpdateNoticeUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Notice notice = getNoticeFromRequest(request);
		if (notice.getNoticeId() != 0){
			notice = iNotice.getNoticeById(notice.getNoticeId());
			request.setAttribute("notice", notice);
		}
		return "bclient/notice/edit/updateNotice.jsp";
	}
	/**
	 * 申请（发送）公告审核
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String sendToCheck(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Notice notice = getNoticeFromRequest(request);
		Check check = getCheckFromRequest(request);
		check.setCheckedId(notice.getNoticeId());
		check.setFlag(0);  //待审核状态
		check.setType(CheckConstant.TYPE_NOTICE);
		String resString = null;
		if (iNotice.sendCheck(notice, check)){
			resString = "发送审核成功！";
		}else{
			resString = "发送审核失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeListManagerUI(request, response);
	}
	/**
	 * 批量申请（发送）公告审核
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String sendAllNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] ids = request.getParameterValues("noticeId");
		Check check = null;
		Notice notice = null;
		boolean isSuccess = true;
		for (int i = 0; i < ids.length; i++){
			notice = new Notice();
			notice.setNoticeId(Integer.parseInt(ids[i]));
			check = new Check();
			check.setCheckedId(notice.getNoticeId());
			check.setFlag(0);
			check.setType("notice");
			if (!iNotice.sendCheck(notice, check)){
				isSuccess = false;
				break;
			}
		}
		if (isSuccess){
			resString = "批量发送审核成功！";
		}else{
			resString = "批量发送审核失败！";
		}
		request.setAttribute("resString", resString);
		return toNoticeListManagerUI(request, response);
	}
	/**
	 * 更改公告的标题和内容
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String updateNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Notice notice = getNoticeFromRequest(request);
		notice.setLastUpdateTime(new Date());
		String resString = null;
		if (iNotice.updateNotice(notice)){
			resString = "修改公告成功！";	
		}
		else{
			resString = "修改公告失败！";
		}
		request.setAttribute("resString", resString);
		return "bclient/notice/edit/updateNotice.jsp";
	}
	public String toNoticeListManagerUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getEditableNoticeNum();
		page.paging(currentPage, 10, totalCount);
		System.out.println(page.getTotalCount()+page.getCurrentPage()+page.getTotalPage());
		page = iNotice.getEditableNotice(page);
		request.setAttribute("page", page);
		//存储编辑完成公告
		/*Pager page2 = new Pager();
		int totalCount2 = iNotice.getFinishedNoticeNum();
		page2.paging(currentPage, 2, totalCount2);
		page2 = iNotice.getFinishedNotice(page2);
		request.setAttribute("finishPage", page2);*/
		//存储审核不通过公告
		/*Pager page3 = new Pager();
		int totalCount3 = iNotice.getUnPassNoticeNum();
		page3.paging(currentPage, 2, totalCount3);
		page3 = iNotice.getUnPassNotice(page3);
		request.setAttribute("unPassPage", page3);*/
		//存储已撤销公告
		/*Pager page4 = new Pager();
		int totalCount4 = iNotice.getRevokedNoticeNum();
		page4.paging(currentPage, 2, totalCount4);
		page4 = iNotice.getRevokedNotice(page4);
		request.setAttribute("revokePage", page4);*/
		return "bclient/notice/module/noticeListManager.jsp";
	}

	/**
	 * 展示所有的可编辑公告（撰写完成、审核不通过、已撤销）
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public String showEditableNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getEditableNoticeNum();
		page.paging(currentPage, 2, totalCount);
		page = iNotice.getEditableNotice(page);
		request.setAttribute("editablePage", page);
		return "bclient/notice/edit/allEditableNotice.jsp";
	}*/
	/**
	 * 展示所有的编辑完成状态的公告
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public String showFinishedNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getFinishedNoticeNum();
		page.paging(currentPage, 2, totalCount);
		page = iNotice.getFinishedNotice(page);
		request.setAttribute("finishPage", page);
		return "bclient/notice/edit/editFinishedNotice.jsp";
	}*/
	/**
	 * 展示所有的未通过审核状态的公告
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public String showUnPassedNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getUnPassNoticeNum();
		page.paging(currentPage, 2, totalCount);
		page = iNotice.getUnPassNotice(page);
		request.setAttribute("unPassPage", page);
		return "bclient/notice/edit/unPassedNotice.jsp";
	}*/
	/**
	 * 展示所有的已撤销状态的公告
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public String showRevokedNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		if (request.getParameter("currentPage") != null){
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int totalCount = iNotice.getRevokedNoticeNum();
		page.paging(currentPage, 2, totalCount);
		page = iNotice.getRevokedNotice(page);
		request.setAttribute("revokePage", page);
		return "bclient/notice/edit/revokedNotice.jsp";
	}*/
	/**
	 * 从http请求中创建NoticeSort对象
	 * @param request
	 * @return 返回NoticeSort对象
	 * @throws ServletException
	 * @throws IOException
	 */
	private NoticeSort getNoticeSortFromRequest(HttpServletRequest request) throws ServletException, IOException {
		NoticeSort noticeSort = new NoticeSort();
		if (request.getParameter("sortId") != null){
			noticeSort.setSortId(Integer.parseInt(request.getParameter("sortId")));
		}if (request.getParameter("sortName") != null){
			noticeSort.setSortName(request.getParameter("sortName").trim());
		}if (request.getParameter("noticeNum") != null){
			noticeSort.setNoticeNum(Integer.parseInt(request.getParameter("noticeNum")));
		}if (request.getParameter("note") != null){
			noticeSort.setNote(request.getParameter("note").trim());
		}if (request.getParameter("createTime") != null){
			noticeSort.setCreateTime(StringUtil.toDate(request.getParameter("createTime")));
		}if (request.getParameter("lastUpdateTime") != null){
			noticeSort.setLastUpdateTime(StringUtil.toDate(request.getParameter("lastUpdateTime").trim()));
		}if (request.getParameter("userId") != null){
			noticeSort.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return noticeSort;
	}
	/**
	 * 从http请求中创建Notice对象
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private Notice getNoticeFromRequest(HttpServletRequest request) throws ServletException, IOException {
		Notice notice = new Notice();
		if (request.getParameter("noticeId") != null){
			notice.setNoticeId(Integer.parseInt(request.getParameter("noticeId")));
		}if (request.getParameter("title") != null){
			notice.setTitle(request.getParameter("title").trim());
		}if (request.getParameter("content") != null){
			notice.setContent(request.getParameter("content").trim());
		}if (request.getParameter("addTime") != null){
			notice.setAddTime(StringUtil.toDate(request.getParameter("addTime")));
		}if (request.getParameter("lastUpdateTime") != null){
			notice.setLastUpdateTime(StringUtil.toDate(request.getParameter("lastUpdateTime").trim()));
		}if (request.getParameter("status") != null){
			notice.setStatus(Integer.parseInt(request.getParameter("status")));
		}if (request.getParameter("sortId") != null){
			notice.setSortId(Integer.parseInt(request.getParameter("sortId")));
		}if (request.getParameter("userId") != null){
			notice.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return notice;
	}
	private Check getCheckFromRequest(HttpServletRequest request) throws ServletException, IOException {
		Check check = new Check();
		if (request.getParameter("checkId") != null){
			check.setCheckId(Integer.parseInt(request.getParameter("checkId")));
		}if (request.getParameter("checkedId") != null){
			check.setCheckedId(Integer.parseInt(request.getParameter("checkedId")));
		}if (request.getParameter("type") != null){
			check.setType(request.getParameter("type").trim());
		}if (request.getParameter("msg") != null){
			check.setMsg(request.getParameter("msg").trim());
		}if (request.getParameter("lastCheckTime") != null){
			check.setLastCheckTime(StringUtil.toDate(request.getParameter("lastCheckTime").trim()));
		}if (request.getParameter("userId") != null){
			check.setUserId(Integer.parseInt(request.getParameter("userId")));
		}
		return check;
	}
}
