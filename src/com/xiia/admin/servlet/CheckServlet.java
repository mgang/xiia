package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.CheckService;
import com.xiia.service.inter.ICheck;
import com.xiia.util.CheckConstant;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.Check;
import com.xiia.vo.User;

/**
 * 
 * @author meigang 2014-11-10 17:47
 *
 */
@WebServlet("/check.do")
public class CheckServlet extends FrameworkServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ICheck iCheck;
	private static Pager page;
	public CheckServlet(){
		iCheck = new CheckService();
		page = new Pager();
	}
	/**
	 * 查询审核列表
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String list(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Check c = doForm(request);
		int currentPage=1;
		if(request.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(request.getParameter("currentPage").toString());
		}
		//得到的总行数
		int totalCount=iCheck.countByType(c.getType());
		page.paging(currentPage, ParamUtil.CHECK_PAGE_SIZE, totalCount);
		page =iCheck.findCheckPage(page,"flag",c);
		request.setAttribute("page",page);

		return "bclient/check/"+c.getType()+"List.jsp";
	}
	/**
	 * 查看审核信息
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String listAll(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		int currentPage=1;
		if(request.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(request.getParameter("currentPage").toString());
		}
		//得到的总行数
		int totalCount=iCheck.count();
		page.paging(currentPage, ParamUtil.CHECK_ALL_PAGE_SIZE, totalCount);
		page =iCheck.findAllCheckPage(page,"type");
		request.setAttribute("page",page);

		return "bclient/check/checkList.jsp";
	}
	/**
	 * 审核通过
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String pass(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Check c = doForm(request);
		
		c.setFlag(CheckConstant.FLAG_YES_CHECK);
		c.setLastCheckTime(new Date());
		c.setUser((User) request.getSession().getAttribute(ParamUtil.LOGIN_USER));
		if(iCheck.check(c,CheckConstant.STATUS_CHECK_PASS)){
			resString = "操作成功";
		}else{
			resString = "操作失败";
		}
		request.setAttribute("resString", resString);
		return "check.do?action=list&type="+c.getType();
	}
	/**
	 * 审核不通过
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String nopass(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Check c = doForm(request);
		c.setFlag(CheckConstant.FLAG_YES_CHECK);
		c.setLastCheckTime(new Date());
		c.setUser((User) request.getSession().getAttribute(ParamUtil.LOGIN_USER));
		if(iCheck.check(c,CheckConstant.STATUS_CHECK_NO_PASS)){
			resString = "操作成功";
		}else{
			resString = "操作失败";
		}
		request.setAttribute("resString", resString);
		return "check.do?action=list&type="+c.getType();
	}
	/**
	 * 封装表单
	 * @param r
	 * @return
	 */
	private Check doForm(HttpServletRequest r){
		Check c = new Check();
		if(null != r.getParameter("checkedId"))
			c.setCheckedId(Integer.parseInt(r.getParameter("checkedId").trim()));
		if(null != r.getParameter("type"))
			c.setType(r.getParameter("type"));
		if(null != r.getParameter("msg"))
			c.setMsg(r.getParameter("msg"));
		
		return c;
	}
}
