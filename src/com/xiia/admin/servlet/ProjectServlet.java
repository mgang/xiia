package com.xiia.admin.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.ProjectService;
import com.xiia.service.inter.IProject;
import com.xiia.util.CheckConstant;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.util.StringUtil;
import com.xiia.vo.Project;
import com.xiia.vo.User;
/**
 * 2014-11-4
 * @author 王书恒
 *项目后台Servlet
 */
@WebServlet("/project.do")
public class ProjectServlet	extends FrameworkServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static IProject iProject;
	private static Pager page;
	
	public ProjectServlet(){
		iProject = new ProjectService();
		page = new Pager();
	}

	
	/**
	 * 查询所有项目
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String list(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int currentPage=1;
		if(request.getParameter("currentPage")!=null){
			currentPage=Integer.parseInt(request.getParameter("currentPage").toString());
		}
		//得到Project的总行数
		int totalCount=iProject.countProject();
		page.paging(currentPage, ParamUtil.PROJECT_PAGE_SIZE, totalCount);
		page =iProject .findProjectPage(page,"projectId");
		request.setAttribute("page",page);
		
		return "bclient/project/projectList.jsp";
	}
	
	/**
	 * 添加项目
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String addProject(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Project project = doForm(request);
		User user = (User) request.getSession().getAttribute(ParamUtil.LOGIN_USER);
		project.setUser(user);
		project.setStatus(CheckConstant.STATUS_WRITE_DONE);
		project.setAddTime(new Date());
		if(iProject.addProject(project)){
			resString = "新增项目成功";
		}else{
			resString = "新增项目失败";
		}
		request.setAttribute("resString", resString);
		return "bclient/project/addProject.jsp";
	}
	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String toChange(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Project p = doForm(request);
		p = iProject.getProjectById(p.getProjectId());		
	
		request.setAttribute("project", p);
		return "bclient/project/updateProject.jsp";
	}
	/**
	 * 修改项目
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String updateProject(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		Project p = doForm(request);
		iProject.updateProject(p);
		if(iProject.updateProject(p)){
			resString = "修改成功";
		}else{
			resString = "修改失败";
		}
		request.setAttribute("resString", resString);
		return list(request, response);
	}
	
	/**
	 * 删除项目
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	
	public String deleteProject(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Project p = doForm(request);
		if(iProject.deleteProject(p)){
			resString = "删除成功";
		}else{
			resString = "删除失败";
		}
		request.setAttribute("resString", resString);
		return list(request, response);
	}
	
	/**
	 * 封装栏目
	 * @param request
	 * @param response
	 * @return
	 */
	public Project doForm(HttpServletRequest request){
	Project project = new Project();
	if(StringUtil.isNotEmpty(request.getParameter("projectId"))){
		project.setProjectId(StringUtil.stringToInt(request.getParameter("projectId").trim()));
	}if(StringUtil.isNotEmpty(request.getParameter("status"))){
		project.setStatus(StringUtil.stringToInt(request.getParameter("status")));
	}if(StringUtil.isNotEmpty(request.getParameter("projectName"))){
		project.setProjectName(request.getParameter("projectName"));
	}if(StringUtil.isNotEmpty(request.getParameter("note"))){
		project.setNote(request.getParameter("note"));
	}
	
	return project;
}

}
