package com.xiia.admin.servlet;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiia.common.servlet.FrameworkServlet;
import com.xiia.servcie.imp.RuleService;
import com.xiia.servcie.imp.RuleSortService;
import com.xiia.service.inter.IRule;
import com.xiia.service.inter.IRuleSort;
import com.xiia.util.Pager;
import com.xiia.util.ParamUtil;
import com.xiia.vo.RuleSort;
import com.xiia.vo.User;

/**
 * 
 * @author 胡天天	2014-11-6
 * 法规分类相关的servlet
 *
 */
@SuppressWarnings("serial")
@WebServlet("/ruleSort.do")
public class RuleSortServlet extends FrameworkServlet{
	private static IRuleSort iRuleSort;
	private static RuleSort ruleSort;
	private static User user;
	private static IRule iRule;
	private static Pager page;
	public RuleSortServlet(){
		iRuleSort = new RuleSortService();
		ruleSort = new RuleSort();
		user = new User();
		iRule = new RuleService();
		page = new Pager();
	}
	/**
	 * 处理action=addRuleSort，添加一条rulesort到数据库
	 * @param req HttpRequset对象
	 * @param res HttpResponse对象
	 */
	public String creatRuleSort(HttpServletRequest req,HttpServletResponse res){
		String sortname=req.getParameter("sortName");
		 //得到分类描述
		String note = req.getParameter("note");  
		user=(User)req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		ruleSort.setSortName(sortname);                     
		ruleSort.setNote(note);                  
		ruleSort.setUser(user);                             
		iRuleSort.createRuleSort(ruleSort);              
		return "ruleSort.do?action=findAllRuleSort";        
	}
	/**
	 * 处理action=findAllRuleSort,从数据库中读取所有的rulesort
	 * @param req
	 * @param res
	 * @return
	 */
	public String findAllRuleSort(HttpServletRequest req,HttpServletResponse res){
		/*  没有分页之前的写法
		   	多态调用rulesortservice——rulesortdao——basedao返回数据库所有的rulesort
			List<RuleSort> rulesortlist = iRuleSort.findAllRuleSort();        
			将rulesortlist存入request对象
			req.setAttribute("list", rulesortlist);  
		 	跳转到rulesort页面
		   
		  */
		//分页
		int currentPage = 1;
		if(req.getParameter("currentPage")!=null){
			currentPage = Integer.parseInt(req.getParameter("currentPage").toString());
		}
		//获得数据库中rulesort总行数
		int totalCount = iRuleSort.getSortCount();
		page.paging(currentPage, ParamUtil.RULE_SORT_PAGE_SIZE, totalCount);
		iRuleSort.findAllRuleSortP(page);
		req.setAttribute("page", page);
		return "bclient/rule/findAllRuleSort.jsp";              
	}
	/**
	 * 处理action=deleteRuleSort，删除数据库中的一条rulesort记录
	 * @param req
	 * @param res
	 * @return
	 */
	public String deleteRuleSort(HttpServletRequest req,HttpServletResponse res){
		String ruleSortId[]=req.getParameterValues("ruleSortId");
		for(int i=0;i<ruleSortId.length;i++){
			int id = Integer.parseInt(ruleSortId[i]);   
			ruleSort.setSortId(id);                       
			ruleSort=iRuleSort.findRuleSortById(ruleSort);
			iRule.deleteRuleBySortId(id);				
			iRuleSort.deleteRuleSort(ruleSort);  	   
		}
		return "ruleSort.do?action=findAllRuleSort";
	}
	/**
	 * 从数据库中得到当前所有的rulesort，提供给新建rule使用
	 * @param req
	 * @param res
	 * @return
	 */
	public String createRule(HttpServletRequest req,HttpServletResponse res){
		 //多态调用rulesortservice——rulesortdao——basedao返回数据库所有的rulesort
		List<RuleSort> rulesortlist = iRuleSort.findAllRuleSort();          
		req.setAttribute("List", rulesortlist);
		return "bclient/rule/createRule.jsp";
	}
	/**
	 * 通过rulesortid更改数据库中rulesort
	 * @param req
	 * @param res
	 * @return
	 */
	public String updateRuleSort(HttpServletRequest req,HttpServletResponse res){
		String sortid = req.getParameter("sortId");
		String sortname=req.getParameter("sortName");
		String note = req.getParameter("note");  
		user=(User)req.getSession().getAttribute(ParamUtil.LOGIN_USER);
		ruleSort.setUser(user);
		ruleSort.setSortId(Integer.parseInt(sortid));
		ruleSort.setSortName(sortname);
		ruleSort.setNote(note);
		iRuleSort.updateRuleSort(ruleSort);
		return "ruleSort.do?action=findAllRuleSort";
	}
	/**
	 * 通过页面信息编辑要修改的rulesort
	 * @param req
	 * @param rep
	 * @return
	 */
	public String editRuleSort(HttpServletRequest req,HttpServletResponse rep){
		String rulesortid = req.getParameter("ruleSortId");
		ruleSort.setSortId(Integer.parseInt(rulesortid));
		ruleSort=iRuleSort.findRuleSortById(ruleSort);
		req.setAttribute("ruleSort", ruleSort);
		return "bclient/rule/updateRuleSort.jsp";
	}
	
}
