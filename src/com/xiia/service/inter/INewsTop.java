package com.xiia.service.inter;

import com.xiia.vo.NewsTop;
/**
 * 2014 11 4 21:24
 * @author 陈强
 * 新闻置顶业务接口
 */
public interface INewsTop {
	public boolean newsTop(NewsTop newsTop);
	public boolean deleteNewsTop(NewsTop newsTop);
}
