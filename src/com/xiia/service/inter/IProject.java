package com.xiia.service.inter;

import java.util.Date;
import java.util.List;




import com.xiia.util.Pager;
import com.xiia.vo.Project;

/**
 * @author--王书恒--2014-11-4
 * Project接口
 */
public interface IProject {
	
	
	/**
	 * 增加项目
	 * @return boolean
	 */
	public boolean addProject(Project project);
	
	/**
	 * 删除项目
	 * @return boolean
	 */
	public boolean deleteProject(Project projects);
	
	
	/**
	 * 通过projectId得到project对象
	 * @param projectId
	 * @return 返回project对象
	 */
	public Project getProjectById(int projectId) ;
	/**
	 * 修改项目的名称
	 * @return boolean
	 */
	public boolean updateProject(Project project);
	/**
	 * 查询得到所有项目的行数
	 * @return
	 */
	public int countProject();
	/**
	 * 获得项目的分页数据
	 * @param page
	 * @param string
	 * @return
	 */
	public Pager findProjectPage(Pager page, String string);
	
	

}
