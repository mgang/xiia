package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.RuleSort;


/**
 * 
 * @author 胡天天  2014-11-4
 * RuleSort service接口
 *
 */
public interface IRuleSort {
	public boolean createRuleSort(RuleSort rulesort);
	public boolean deleteRuleSort(RuleSort rulesort);
	public boolean updateRuleSort(RuleSort rulesort);
	public Pager findAllRuleSortP(Pager page);
	public List<RuleSort> findAllRuleSort();
	public boolean updateRuleSortNum(RuleSort rulesort);
	public RuleSort findRuleSortById(RuleSort ruleSort);
	public boolean sortName(String name);
	public int getSortCount();
}
