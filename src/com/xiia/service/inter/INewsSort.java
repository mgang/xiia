package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.NewsSort;
/**
 * 2014 11 4 21:23
 * @author 陈强
 * 新闻分类业务接口
 */
public interface INewsSort {
	/**
	 * 添加新闻分类
	 * @param newsSort
	 * @return boolean
	 */
	public boolean createNewSort(NewsSort newsSort);
	/**
	 * 删除新闻分类
	 * @param newsSort
	 * @return boolean
	 */
	public boolean deleteSort(NewsSort newsSort);
	/**
	 * 修改新闻分类
	 * @param newsSort
	 * @return boolean 
	 */
	public boolean updateSort(NewsSort newsSort);
	/**
	 * 拿到所有分类
	 * @return List<NewsSort>
	 */
	public List<NewsSort> findAllSort();
	/**
	 * 修改新闻分类下的新闻数量
	 * @param newsSort
	 * @return boolean
	 */
	public boolean updateSortNewsNum(NewsSort newsSort);
	/**
	 * 是否存在同名新闻分类
	 * @param sortName
	 * @return boolean
	 */
	public boolean isExistSortName(String sortName);
	/**
	 * 通过新闻id来查找新闻分类
	 * @param sortId
	 * @return NewsSort
	 */
	public NewsSort findNewsSortById(int sortId);
	/**
	 * 拿到所有新闻并封装到Pager中
	 * @param page
	 * @return Pager
	 */
	public Pager findNewsSortPage(Pager page );
	/**
	 * 拿到新闻分类的数量
	 * @return int
	 */
	public int getCount() ;
}
