package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.ResourceSort;

/**
 * @author--王书恒--2014-11-4
 * ResourceSort接口
 */
public interface IResourceSort {
	
	/**
	 * 增加资源分类
	 * @return boolean
	 */
	public boolean addResourceSort(ResourceSort resourceSort);
	
	/**
	 * 删除资源分类
	 * @return boolean
	 */
	public boolean deleteResourceSort(ResourceSort resourceSorts);
	
	/**
	 * 修改资源分类的名称
	 * @return boolean
	 */
	public boolean updateResourceSort(ResourceSort resourceSort);
	/**
	 * 通过sortId得到resourceSort对象
	 * @param sortId
	 * @return 返回resourceSort对象
	 */
	public ResourceSort getResourceSortById(int sortId) ;
	/**
	 * 查询得到所有资源分类的行数
	 * @return
	 */
	public int countResourceSort();
	/**
	 * 得到资源分类的分页数据
	 * @param page
	 * @param string
	 * @return
	 */
	public Pager findResourceSortPage(Pager page, String string);
	/**
	 * 得到所有的资源分类
	 * @return
	 */
	public List<ResourceSort> getAllResourceSort();

}
