package com.xiia.service.inter;

import com.xiia.util.Pager;
import com.xiia.vo.Revoke;

/**
 * 
 * @author meigang 2014-11-10
 *
 */
public interface IRevoke {
	/**
	 * 撤销
	 * @param r
	 * @return 撤销成功返回true,反之返回false.
	 */
	boolean revoke(Revoke r);
	/**
	 * 得到撤销了的行数
	 * @return
	 */
	int count();
	/**
	 * 得到撤销页的数据
	 * @param page
	 * @param string
	 * @return
	 */
	Pager findAllRevokePage(Pager page, String string);

}
