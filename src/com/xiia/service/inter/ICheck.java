package com.xiia.service.inter;

import com.xiia.util.Pager;
import com.xiia.vo.Check;

/**
 * 
 * @author meigang 2014-11-10 17:34
 *
 */
public interface ICheck {
	/**
	 * 通过type查询得到行数
	 * @param type
	 * @return 返回查到的行数
	 */
	int countByType(String type);
	/**
	 * 查询得到审核分页数据page
	 * @param page
	 * @param orderBy 按审核排序
	 * @param c 查询的审核对象，用到type
	 * @return 返回带有List的page对象。
	 */
	Pager findCheckPage(Pager page, String orderBy,Check c);
	/**
	 * 审核通过
	 * @param c 要审核的对象
	 * @param status 改变实体中的status状态
	 * @return 成功返回true，返回返回false;
	 */
	boolean check(Check c,int status);
	/**
	 * 查询得到check表中的所有记录行数
	 */
	int count();
	/**
	 * 查询得到所有的check信息的分页数据
	 * @param page
	 * @param orderBy 按什么分页
	 * @return
	 */
	Pager findAllCheckPage(Pager page,String orderBy);

}
