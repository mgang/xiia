package com.xiia.service.inter;

import java.util.List;
import java.util.Map;

import com.xiia.util.Pager;
import com.xiia.vo.Menu;
import com.xiia.vo.User;

/**
 * 2014 11/3 18:29
 * 
 * @author 李跃磊
 *
 */
public interface IMenu {

	/**
	 * 查询系统栏目根据栏目的状态查询
	 * 
	 * @return 栏目对象集合
	 */
	public List<Menu> selectMenu(int status);

	/**
	 * 查询用户处于某状态的栏目
	 * 
	 * @param i
	 *            状态
	 * @param currenUser
	 *            用户
	 * @return 栏目对象集合
	 */
	public List<Menu> selectMenu(int i, User user);

	/**
	 * 查询所有栏目
	 * 
	 * @return 栏目集合
	 */
	public List<Menu> selectMenu();

	/**
	 * 按栏目的显示顺序查询栏目（注：栏目已经发布在前台显示）
	 * 
	 * @return 栏目键值对
	 */
	public Map<String, List<Menu>> selectMenuByOrder();

	/**
	 * 按状态和等级查询栏目
	 * 
	 * @param Rank
	 * @param status
	 * @return 栏目集合
	 */
	public List<Menu> selectMenuByRankAndStatus(int Rank, int status);

	/**
	 * 查询当前栏目的子栏目
	 * 
	 * @param menu
	 *            栏目对象
	 * @return 栏目对象集合
	 */
	public List<Menu> selectByPreMenuId(Menu menu);

	/**
	 * 根据用户查询用户自己的栏目（分类）
	 * 
	 * @param user
	 * @return
	 */
	public List<Menu> selectMenu(User user);

	/**
	 * 增加系统的栏目（注：不一定都显示在前台页面）
	 * 
	 * @return 栏目对象
	 */
	public Menu addMenu(Menu menu);

	/**
	 * 删除系统的栏目（未在前台显示的）,成功返回true否则false
	 * 
	 * @return true或者false
	 */
	public boolean deleteMenu(List<Menu> menus);

	/**
	 * 修改栏目，栏目的名称或者链接,成功返回true否则false
	 * 
	 * @return true或者false
	 */
	public boolean updateMenu(List<Menu> menus);

	/**
	 * 前台显示的栏目的显示顺序,成功返回true否则false
	 * 
	 * @param menus
	 * @return true或者false
	 */
	public boolean updateMenuDisplayOrder(List<Menu> menus);

	/**
	 * 撤销栏目 使已经在前台显示状态的栏目设置为不显示状态,成功返回true否则false
	 * 
	 * @param menus
	 *            栏目对象集合
	 * @return true或者false
	 */
	public boolean showOrUndoMenu(List<Menu> menus);

	/**
	 * 发送审核栏目请求，成功返回true否则false
	 * 
	 * @param menus
	 * @return true或者false
	 */
	public boolean applyCheckMenu(List<Menu> menus);

	/**
	 * 计算栏目表栏目的总记录条数
	 * 
	 * @return 总记录条数
	 */
	public int countMenu();

	/**
	 * 按页取栏目对象
	 * 
	 * @param page
	 *            页对象
	 * @return 页对象
	 */
	public Pager findMenuPage(Pager page);

	/**
	 * 按页查询status状态的栏目
	 * 
	 * @param page
	 * @param status
	 * @return 页对象
	 */
	public Pager findMenuPage(Pager page, int status);

	/**
	 * 按栏目状态计算该状态栏目总条数
	 * 
	 * @return 总条数
	 */
	public int countMenuByStatus(int status);

	/**
	 * 计算可修的栏目总条数
	 * 
	 * @return 总条数
	 */
	public int countCanUpdateMenu();
	
	/**
	 * 按页查询可修改栏目的栏目对象
	 * @param page
	 * @return 页对象
	 */
	public Pager findCanUpdateMenuPage(Pager page);

}
