package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.Check;
import com.xiia.vo.News;
import com.xiia.vo.NewsSort;
/**
 * 2014 11 4 21:22
 * @author 陈强
 * 新闻业务接口
 */
public interface INews {
	/**
	 * 添加新闻
	 * @param news
	 * @return boolean
	 */
	public boolean addNews(News news);
	/**
	 * 删除新闻
	 * @param news
	 * @return boolean
	 */
	public boolean deleteNews(News news);
	/**
	 * 修改新闻并包括移动至
	 * @param news
	 * @param oldSort
	 * @return boolean
	 */
	public boolean updateNewsS(News news,NewsSort oldSort);
	/**
	 * 修改新闻不包括移动至
	 * @param news
	 * @return boolean
	 */
	public boolean updateNews(News news);
	/**
	 * 通过新闻分类来查找新闻集合
	 * @param newsSort
	 * @return List<News>
	 */
	public List<News> findNewsBySort(NewsSort newsSort);
	/**
	 * 通过新闻id来查找新闻
	 * @param newsId
	 * @return boolean
	 */
	public News findNews(int newsId);
	/**
	 * 修改新闻的状态
	 * @param news
	 * @return boolean
	 */
	public boolean releaseNews(News news);
	/**
	 * 通过头条表中来查找新闻表中的新闻
	 * @return List<News>
	 */
	public List<News> getAllNewsByHeadline();
	/**
	 * 新闻置顶
	 * @param news
	 * @return boolean
	 */
	public boolean updateNewsTopFlag(News news);
	/**
	 * 取消新闻置顶
	 * @param news
	 * @return boolean
	 */
	public boolean deleteNewsTopFlag(News news);
	/**
	 * 修改新闻头条的标记
	 * @param news
	 * @return boolean
	 */
	public boolean updateNewsHeadlineFlag(News news);
	/**
	 * 删除新闻头条标记
	 * @param news
	 * @return boolean
	 */
	public boolean deleteNewsHeadlineFlag(News news);
	/**
	 * 通过新闻分类来查找新闻集合并封装到Pager中
	 * @param page
	 * @param newsSort
	 * @return Pager
	 */
	public Pager findNewsPage(Pager page,NewsSort newsSort );
	/**
	 * 得到新闻数量通过新闻分类
	 * @param newsSort
	 * @return int
	 */
	public int getCount(NewsSort newsSort) ;
	/**
	 * 添加到审查表
	 * @param news
	 * @return boolean
	 */
	public boolean addCheck(News news);
	/**
	 * 得到新闻未通过的数量
	 * @return int
	 */
	public int getCountNoPass();
	/**
	 * 得到新闻未通过的集合并封装到Pager中
	 * @param page
	 * @return Pager
	 */
	public Pager findNewsNoPassPage(Pager page );
	/**
	 * 修改审核字段
	 * @param news
	 * @return boolean
	 */
	public boolean updateCheck(News news);
	/**
	 * 通过id来查找审核表中的新闻
	 * @param news
	 * @return boolean
	 */
	public boolean findCheckByNewsId(News news);
	/**
	 * 通过新闻id来查找审核表中的新闻
	 * @param news
	 * @return check
	 */
	public Check findCheckByNewsIdCheck(News news);
	/**
	 * 通过新闻分类来查找已发布的的新闻集合并封装到Pager
	 * @param newsSort
	 * @param page
	 * @return Pager
	 */
	public Pager findReleaseNewsBySort(NewsSort newsSort,Pager page);
	/**
	 * 得到已发布新闻的数量
	 * @param newsSort
	 * @return int
	 */
	public int getConuntRelease(NewsSort newsSort);
	/**
	 * 得到所有新闻的集合并封装到Pager
	 * @param page
	 * @return Pager
	 */
	public Pager findAllNewsPage(Pager page);
	/**
	 * 得到所有新闻的数量
	 * @return int
	 */
	public int getAllNewsCount();
}
