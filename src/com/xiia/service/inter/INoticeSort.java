package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.NoticeSort;


/**
 * 
 * @author zhouhao 2014-11-4 10:26
 * NoticeSortService的接口
 */
public interface INoticeSort {
	/**
	 * 列出所有的公告分类
	 * @return 
	 */
	public List<NoticeSort> listNoticeSort();
	/**
	 * 列出所有的公告分类（分页）
	 * @return 
	 */
	public Pager listNoticeSort(Pager page);
	/**
	 * 将noticeSort对象插入到数据库中
	 * @param noticeSort
	 * @return 成功返回true,否则返回false
	 */
	public boolean addNoticeSort(NoticeSort noticeSort);
	/**
	 * 更改公告分类的名称或描述信息
	 * @param noticeSort
	 * @return
	 */
	public boolean updateNoticeSort(NoticeSort noticeSort);
	/**
	 * 在数据库中删除一条公告记录
	 * @param noticeSort
	 * @return
	 */
	public boolean deleteNoticeSort(NoticeSort noticeSort);
	/**
	 * 根据id从数据库中返回公告分类
	 * @param sortId
	 * @return
	 */
	public NoticeSort getNoticeSortById(int sortId);
	/**
	 * 获取前x个公告分类（按最后修改时间降序排列）
	 * @param x
	 * @return
	 */
	public List<NoticeSort> getTopXNoticeSort(int x);
	/**
	 * 获取noticeSort里的已发布公告总数量，若该对象为空，则返回所有已发布公告数量
	 * @param noticeSort
	 * @return
	 */
	public int getPublishedNoticeNum(NoticeSort noticeSort);
	public boolean isExistSortName(String sortName);
	

}
