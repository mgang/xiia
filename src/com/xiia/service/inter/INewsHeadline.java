package com.xiia.service.inter;

import java.util.List;

import com.xiia.vo.NewsHeadline;
/**
 * 2014 11 4 21:23
 * @author 陈强
 * 新闻头条业务接口
 */
public interface INewsHeadline {
	/**
	 * 添加新闻头条
	 * @param newsHeadline
	 * @return boolean
	 */
	public boolean addNewsHeadline(NewsHeadline  newsHeadline);
	/**
	 * 通过新闻Id来查找头条
	 * @param newsId
	 * @return NewsHeadline
	 */
	public NewsHeadline getHeadlineByNewsId(int newsId);
	/**
	 * 修改头条标题和头条描述
	 * @param headline
	 * @return boolean
	 */
	public boolean updateTitle(NewsHeadline headline);
	/**
	 * 修改头条照片路径
	 * @param headline
	 * @return boolean
	 */
	public boolean updateUrl(NewsHeadline headline);
}
