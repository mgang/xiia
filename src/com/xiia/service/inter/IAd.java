package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.Ad;


/**
 * 
 * @author 张文龙 2014-11-4 9:33
 *
 */
public interface IAd {
	/**
	 * 获得数据总数
	 */
	public int getAdNum();
	/**
	 * 录入广告
	 * @return
	 */
	public boolean entryAd(Ad ad);
	/**
	 * 多状态查询广告
	 * @return
	 */
	public Pager selectAd1(Pager page);
	/**
	 * 根据Id状态查询广告
	 * @param ad
	 * @return
	 */
	public List<Ad>selectAd(Ad ad);
	
	/**
	 * 公布广告
	 * @return
	 */
	public boolean releaseAd(Ad ad);
	/**
	 * 撤除广告
	 * @return
	 */
	public boolean undoAd(Ad ad);
	/**
	 * 修改广告链接
	 * @param ad 
	 * @return
	 */
	public boolean updateAdLink(Ad ad);
	/**
	 * 删除广告
	 * @return
	 */
	public boolean deleteAd(Ad ad);
	/**
	 * 申请审核
	 * @param ad
	 * @return
	 */
	public boolean checkAd(Ad ad);
	/**
	 * 向审核表中添加数据
	 */
	public boolean addAd(Ad ad);
}
