package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.Check;
import com.xiia.vo.Notice;
import com.xiia.vo.NoticeSort;

/**
 * 
 * @author zhouhao 2014-11-4 10:26
 * NoticeService的接口
 */
public interface INotice {
	/**
	 * 将noticeSort分类里的所有已发布公告列出来(分页)
	 * @param noticeSort
	 * @param page 
	 * @return List<Notice>
	 */
	public Pager listPublishedNotice(NoticeSort noticeSort, Pager page);
	/**
	 * 将noticeSort分类里的所有已发布公告列出来
	 * @param noticeSort
	 * @return
	 */
	public List<Notice> listPublishedNotice(NoticeSort noticeSort);
	/**
	 * 展示公告下的所有公告列表，若noticeSort的id不存在，则展示数据库中所有的公告
	 * @param noticeSort
	 * @return
	 */
	public List<Notice> listAllNotice(NoticeSort noticeSort);
	/**
	 * 展示公告的详细内容
	 * @param notice
	 * @return
	 */
	public Notice showNotice(Notice notice);
	/**
	 * 在数据库中插入一条公告记录
	 * @param notice
	 * @return
	 */
	public boolean addNotice(Notice notice);
	/**
	 * 获取数据库中公告分类的数量
	 * @return
	 */
	public int getNoticeSortNum();
	/**
	 * 删除一条公告
	 * @param notice
	 * @return
	 */
	public boolean deleteNotice(Notice notice);
	/**
	 * 更改公告的标题和内容
	 * @param notice
	 * @return
	 */
	public boolean updateNotice(Notice notice);
	/**
	 * 根据id从数据库返回公告
	 * @param noticeId
	 * @return
	 */
	public Notice getNoticeById(int noticeId);
	/**
	 * 获取公告分类下的第一条已发布公告（按最后修改时间降序排）
	 * @param noticeSort
	 * @return
	 */
	public Notice getTopNotice(NoticeSort noticeSort);
	/**
	 * 获取所有编辑完成的公告数量
	 * @return
	 */
	public int getFinishedNoticeNum();
	public int getUnPassNoticeNum();
	public int getRevokedNoticeNum();
	/**
	 * 获取所有可编辑的公告数量
	 * @return
	 */
	public int getEditableNoticeNum();
	/**
	 * 获取公告分类的所有可编辑的公告数量
	 * @param noticeSort
	 * @return
	 */
	public int getEditableNoticeNum(NoticeSort noticeSort);
	/**
	 * 获取公告分类的所有编辑完成的公告（分页）
	 * @param page
	 * @return
	 */
	public Pager getEditableNotice(NoticeSort noticeSort, Pager page);
	/**
	 * 获取所有编辑完成的公告（分页）
	 * @param page
	 * @return
	 */
	public Pager getFinishedNotice(Pager page);
	public Pager getUnPassNotice(Pager page);
	public Pager getRevokedNotice(Pager page);
	/**
	 * 获取所有可编辑的公告（分页）
	 * @return
	 */
	public Pager getEditableNotice(Pager page);
	/**
	 * 为公告申请（发送）审核
	 * @param notice
	 * @param check
	 * @return
	 */
	public boolean sendCheck(Notice notice, Check check);
	/**
	 * 获取所有的已发布公告
	 * @return
	 */
	public List<Notice> listAllPublishedNotice();
	public boolean isExistTitle(String title);
}
