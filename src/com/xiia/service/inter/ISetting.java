package com.xiia.service.inter;

import com.xiia.vo.Setting;


/**
 * @author--王书恒--2014-11-4
 * Setting接口
 */
public interface ISetting {
	/**
	 * 得到系统参数
	 * @return
	 */
	Setting getSetting();
	/**
	 * 变更系统参数
	 * @param setting
	 * @return
	 */
	boolean updateSetting(Setting setting);
	
}
