package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.Rule;

/**
 * 
 * @author 胡天天 2014-11-4
 * Rule service接口
 *
 */
public interface IRule {
	public boolean addRules(Rule rule);
	public boolean updateRule(Rule rule,int oSortId);
	public boolean deleteRule(Rule rule);
	public Pager findAllRule(Pager page);
	public Rule findRuleById(int ruleId);
	public boolean deleteRuleBySortId(int sortId);
	public boolean ruleName(String ruleName);
	public int getAllCount();
}
