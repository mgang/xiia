package com.xiia.service.inter;

import com.xiia.util.Pager;
import com.xiia.vo.Resource;

/**
 * @author--王书恒--2014-11-4
 * Resource接口
 */
public interface IResource {

	
	/**
	 * 增加资源
	 * @return boolean
	 */
	public boolean addResource(Resource resource);
	
	/**
	 * 删除资源
	 * @return boolean
	 */
	public boolean deleteResource(Resource resources);
	/**
	 * 通过resId得到resource对象
	 * @param resId
	 * @return 返回resource对象
	 */
	public Resource getResourceById(int resId);
	/**
	 * 修改资源的名称
	 * @return boolean
	 */
	public boolean updateResource(Resource resources);
	/**
	 * 查询得到所有资源分类的行数
	 * @return
	 */
	public int countResource();
	/**
	 * 得到资源的分页数据
	 * @param page
	 * @param string
	 * @return
	 */
	public Pager findResourcePage(Pager page, String string);
	/**
	 * 申请审核
	 * @param resource
	 * @return
	 */
	public boolean askCheck(Resource resource);


}
