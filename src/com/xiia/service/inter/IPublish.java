package com.xiia.service.inter;

import com.xiia.util.Pager;
import com.xiia.vo.Publish;

/**
 * 
 * @author meigang 2014-11-10 17:41
 *
 */
public interface IPublish {
	/**
	 * 发布
	 * @param p
	 * @return 发布成功返回true,反之返回false.
	 */
	boolean publish(Publish p);
	/**
	 * 得到已经发布的实体的行数
	 * @return
	 */
	int count();
	/**
	 * 得到已经发布的实体的数据page
	 * @param page
	 * @param string
	 * @return
	 */
	Pager findAllPublishPage(Pager page, String string);

}
