package com.xiia.service.inter;

import java.util.List;

import com.xiia.util.Pager;
import com.xiia.vo.Org;

/**
 * 
 * @author 胡天天    2014-11-4
 * Org service 接口
 *
 */
public interface IOrg {
	public boolean addOrg(Org org);
	public boolean updateOrg(Org org);
	public boolean deleteOrg(Org org);
	public Pager findAllOrg(Pager page);
	public Org findOrgById(int orgId);
	public boolean isExist(String orgName);
	public int getAllCount();
}
