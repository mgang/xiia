package com.xiia.util;
/**
 * 
 * @author 
 *
 */
public class CommonConstant {
	/**
	 * 文字广告
	 */
	public static final int AD_WORD_TYPE_AD = 1;
	
	/**
	 * 图片广告
	 */
	public static final int AD_PHOTO_TYPE_AD = 2;
	/**
	 * FALSH广告
	 */
	public static final int AD_FLASH_TYPE_AD = 3;
}
