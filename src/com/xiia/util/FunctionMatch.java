package com.xiia.util;

import com.xiia.vo.Function;

/**
 * 
 * @author meigang 2014-11-4 15:44
 * 权限匹配
 */
public class FunctionMatch {
	/**
	 * 通过ReqUrl来匹配Function权限
	 * @param fun 权限对象
	 * @param reqUrl 资源请求路径
	 * @return 如果配置成功，返回true;
	 * 			反之，则返回false;
	 */
	public static boolean matchFunctionByReqUrl(Function fun,String reqUrl){
		boolean b = true;
		int count = 1;
		//先匹配resource
		if(reqUrl.contains(fun.getResource())){
			//resource匹配成功
			count ++;
			//匹配后缀
			if(reqUrl.contains(fun.getStuffix())){
				//匹配后缀成功
				count ++;
				//匹配参数
				//System.out.println("reqUrl:"+reqUrl+"-fun:"+fun.getParams());
				for(String p : fun.getParams().split(",")){
					if(!reqUrl.contains(p)){
						//有参数不匹配
						b = false;
					}
				}
			}
		}
		if(count == 3 && b){
			return true;
		}else{
			return false;
		}
	}
}
