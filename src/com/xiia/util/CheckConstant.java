package com.xiia.util;
/**
 * 
 * @author meigang 2014-11-10 19:20
 * 关于审核模块用到的常量工具类
 */
public class CheckConstant {
	/**
	 * 要审核的实体类型
	 */
	public static final String TYPE_RULE = "rule";
	public static final String TYPE_AD = "ad";
	public static final String TYPE_NEWS = "news";
	public static final String TYPE_RESOURCE = "resource";
	public static final String TYPE_PROJECT = "project";
	public static final String TYPE_ORG = "org";
	public static final String TYPE_NOTICE = "notice";
	public static final String TYPE_MENU = "menu";
	
	/**
	 * 实体中的status字段对应的状态
	 */
	public static final int STATUS_WRITE_DONE = 1;//编辑完成
	public static final int STATUS_WAIT_CHECK = 2;//待审核
	public static final int STATUS_CHECK_PASS = 3;//审核通过
	public static final int STATUS_CHECK_NO_PASS = 4;//审核不通过
	public static final int STATUS_PUBLISH_DONE = 5;//已发布
	public static final int STATUS_REVOKE_DONE = 6;//已撤销
	
	/**
	 * 审核表中的flag的取值
	 */
	public static final int FLAG_NO_CHECK = 0;//未审核
	public static final int FLAG_YES_CHECK = 1;//已审核
	
	public static final int FLAG_NO_PUBLISH = 0;//未发布
	public static final int FLAG_YES_PUBLISH = 1;//已发布
	
	public static final int FLAG_NO_REVOKE = 0;//未撤销
	public static final int FALG_YES_REVOKE = 1;//已撤销
}
