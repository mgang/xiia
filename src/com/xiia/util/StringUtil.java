package com.xiia.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author zhouhao 2014-11-4 15:17 
 * 字符串工具类
 */
public class StringUtil {
	/**
	 * 将代表时间的字符串转换为Date类型(年、月、日)
	 * @param str 待转化的字符串
	 * @return Date对象
	 */
	public static Date toDate(String str){
		Date time = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			time = format.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return time;
	}
	
	/**
	 * 判断是否是空
	 * @param str
	 * @return true或者false
	 */
	public static boolean isEmpty(String str){
		if("".equals(str)|| str==null){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 判断是否不是空
	 * @param str
	 * @return true或者false
	 */
	public static boolean isNotEmpty(String str){
		if(!"".equals(str)&&str!=null){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 将字符串转换成整型数字
	 * @param str
	 * @return 整型数字
	 */
	public static int stringToInt(String str){
		return Integer.parseInt(str);
	}
	/**
	 * 通过文件名得到文件的扩展名
	 * @param fileName
	 * @return .jpg,.png
	 */
	public static String getFileExtention(String fileName){
		int index = fileName.lastIndexOf(".");
		return fileName.substring(index,fileName.length());
	}
	/**
	 * 取当前时间的年月日做为目录
	 * @return
	 */
	public static String getCurrentDateInDir(){
		String date = new Date().toLocaleString();
		int index = date.lastIndexOf(" ");
		return date.substring(0, index);
	}
}
