package com.xiia.util;

import com.xiia.servcie.imp.SettingService;

/**
 * 
 * @author meigang 2014-11-4 11:01
 * 参数工具类
 */
public class ParamUtil {
	/**
	 * 登陆的用户存在session中的key
	 */
	public static final String LOGIN_USER = "loginUser";
	/**
	 * 权限列表一页显示的大小
	 */
	public static int FUNCTION_PAGE_SIZE = 10;
	/**
	 * 角色列表一页显示的大小
	 */
	public static int ROLE_PAGE_SIZE = 10;
	/**
	 * 用户列表一页显示的大小
	 */
	public static int USER_PAGE_SIZE = 10;
	/**
	 * 添加普通管理员的默认给予积分
	 */
	public static int NOMAL_MANAGER_SCORE = 500;
	/**
	 * 会员注册给予的默认积分
	 */
	public static int MENMBER_SCORE = 20;
	/**
	 * 审核的一页显示的大小
	 */
	public static int CHECK_PAGE_SIZE = 10;
	/**
	 * 查看审核信息的所有显示的一页大小
	 */
	public static int CHECK_ALL_PAGE_SIZE = 4;
	/**
	 * 发布信息的一页大小
	 */
	public static int PUBLISH_ALL_PAGE_SIZE = 4;
	/**
	 * 撤销信息一页大小
	 */
	public static int REVOKE_ALL_PAGE_SIZE = 4;
	/**
	 * 新闻列表页面显示大小
	 */
	public static int NEWS_PAGE_SIZE = 6;
	/**
	 * 新闻分类页面显示大小
	 */
	public static int NEWS_SORT_PAGE_SIZE = 6;
	/**
	 * 资源分类一页显示的大小
	 */
	public static int RESOURCE_SORT_PAGE_SIZE = 10;
	/**
	 * 资源一页显示的大小
	 */
	public static final int RESOURCE_PAGE_SIZE = 10;
	/**
	 * 项目一页显示的大小
	 */
	public static final int PROJECT_PAGE_SIZE = 10;
	/**
	 * 上传资源的默认大小10M
	 */
	public static int UPLOAD_RESOURCE_FILE_SIZE = 10;
	/**
	 * 显示头条的数量
	 */
	public static int HEADLINE_NEWS_NUM = 5;
	/**
	 * 前台显示的栏目的最大数量
	 */
	public static int MENU_MAX_COUNT = 6;
	/**
	 * 上传资源支持的类型
	 */
	public static String ACCEPT_RESOURCE_FILE_TYPE = ".zip,.rar";
	static{
		//静态加载，第一次使用该工具类的时候
		SettingService.initParamUtilSetting();
	}
	/**
	 * 支持的上传文件类型，用逗号分隔
	 */
	public static String ACCEPT_UPLOAD_RESOURCE_FILE_TYPE = ".zip,.rar";
	/**
	 * 广告文件的最大size，单位：M
	 */
	public static int AD_FILE_SIZE = 10;
	/**
	 * 分支机构页面显示的条数
	 */
	//分支机构的分页条数
	public static int ORG_PAGE_SIZE=6;
	//政策法规的分页条数
	public static int RULE_PAGE_SIZE=6;
	//法规分类的分页条数
	public static int RULE_SORT_PAGE_SIZE=6;
}
