<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!--李跃磊 2014 10/22  -->
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>用户首页</title>
</head>
<body onload="startTime()">
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav_2.jsp"></jsp:include>
	<div id="user_content">
		<h3>当前位置: 首页》》会员</h3>
		<div id="user_left">
			<ul>
				<li class="user_head"><img src="${pageContext.request.contextPath}/fclient/image/user_head.png" alt="用户头像"></li>
				<li class="user_info">积&nbsp;&nbsp;&nbsp;&nbsp;分:&nbsp;&nbsp;&nbsp;&nbsp;100</li>
				<li class="user_info">会员类型:&nbsp;&nbsp;&nbsp;&nbsp;个人</li>
				<li class="user_info">会员名称:&nbsp;&nbsp;&nbsp;&nbsp;一个小子</li>
				<li class="user_info">邮&nbsp;&nbsp;&nbsp;&nbsp;箱:&nbsp;&nbsp;&nbsp;&nbsp;12345678@qq.com</li>
				<li class="user_info">Q&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12345678</li>
				<li class="user_info">电&nbsp;&nbsp;&nbsp;&nbsp;话:&nbsp;&nbsp;&nbsp;&nbsp;123456789</li>
			</ul>
		</div>
		<div id="user_right">
		</div>
	</div>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>