<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!--李跃磊 2014 10/22  -->
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>注册</title>
</head>
<body onload="startTime()">
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav_2.jsp"></jsp:include>
	<div id="user_register">
		<h3>当前位置：首页》》会员》注册</h3>
		<div id="register_content"  style="background-image: url('../image/register.bmp')">
			<center><strong>注册</strong></center><br/>
			<hr width="80%"><br/>
				<form action="" method="get">
					<ul>
						<li>用户名称:<input type="text" name="userName"/></li>	
						<li>密&nbsp;&nbsp;&nbsp;&nbsp;码:<input type="password" name="passWord1"/></li>	
						<li>
							用户类型:
							<select name="userType">
								<option value="个人用户">个人用户</option>
								<option value="企业用户">企业用户</option>
							</select>
						</li>
						<li>确认密码:<input type="password" name="passWord2"/></li>		
						<li>邮&nbsp;&nbsp;&nbsp;&nbsp;箱:<input type="email" name="userEmail"/></li>	
						<li>Q&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q:<input type="text" name="qqNumber"/></li>	
						<li>电&nbsp;&nbsp;&nbsp;&nbsp;话:<input type="text" name="userPhone"/></li>	
						<li>验&nbsp;证&nbsp;码:<input type="text" name="code" size="7"/></li>
					</ul>
					<center>
						<input type="submit" value="注册"/>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="取消"/>
					</center>
				</form>
		</div>
	</div>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>