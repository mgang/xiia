<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/notice/notice.css" type="text/css"></link>
<title>${notice.title }</title>
</head>
<body>
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav_2.jsp"></jsp:include>
	<div id="middle_zh">
		<div id="middle_left_zh">
			<img src="${pageContext.request.contextPath}/fclient/image/left_4.jpg"/>
		</div>
		<div id="middle_center_zh">
			<div id="menu_zh">
				<div style="background-color:#C35C0F;" id="title_zh"><span>公&nbsp;&nbsp;告</span></div>
				<ul id="items_zh">
				<c:forEach items="${noticeSorts }" var="noticeSort">
					<li class="item_zh">
						<a style="color:#C35C0F;" href="${pageContext.request.contextPath}/notice.htm?action=showNoticeList&sortId=${noticeSort.sortId}">
							<img style="border:1px solid #C35C0F;" src="${pageContext.request.contextPath}/fclient/image/item_ico_4.jpg"/>
							<span>${noticeSort.sortName }</span>
						</a>
					</li>
				</c:forEach>
				</ul>
			</div>
			<div id="contentbg_zh">
				<div id="content_zh">
					<div id="panel_zh"><span class="span">》当前位置：首页>>公告</span></div>
					<div id="text_zh">
						<div id="notice_head_zh">
							<span id="notice_title_zh">${notice.title }</span><br/>
							<span id="notice_content_zh">
								时间：${notice.lastUpdateTime }
								发布人：${notice.userId }
							</span>
						</div>
						<hr/>
						<div id="notice_content_zh">${notice.content }</div>
						<div id="notice_foot_zh">
							<c:if test="${not empty preNotice }"><!-- 左浮动 -->
								<div id="preone_zh">
									上一条：<a  href="${pageContext.request.contextPath}/notice.htm?action=showNotice&noticeId=${preNotice.noticeId}">${preNotice.title }</a>
								</div>
							</c:if>
							<c:if test="${not empty nextNotice }"><!-- 右浮动 -->
								<div id="nextone_zh">
									下一条：<a id="nextone_zh" href="${pageContext.request.contextPath}/notice.htm?action=showNotice&noticeId=${nextNotice.noticeId}">${nextNotice.title }</a>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="middle_right_zh"></div>
	</div>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>