<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/society/society.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/notice/noticeList.css" type="text/css"></link>
<title>${noticeSort.sortName }-襄阳市信息产业协会</title>
</head>
<body>
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav_2.jsp"></jsp:include>
	<div id="middle_zh">
		<div id="middle_left_zh">
			<img src="${pageContext.request.contextPath}/fclient/image/left_3.jpg"/>
		</div>
		<div id="middle_center_zh">
			<div id="menu_zh">
				<div style="background-color:#923233;" id="title_zh"><span>公&nbsp;&nbsp;告</span></div>
				<ul id="items_zh">
				<c:forEach items="${noticeSorts }" var="noticeSort">
					<li class="item_zh">
						<a style="color:#923233;" href="${pageContext.request.contextPath}/notice.htm?action=showNoticeList&sortId=${noticeSort.sortId}">
							<img style="border:1px solid #923233;" src="${pageContext.request.contextPath}/fclient/image/item_ico_3.jpg"/>
							<span>${noticeSort.sortName }</span>
						</a>
					</li>
				</c:forEach>
				</ul>
			</div>
			<div id="contentbg_zh">
				<div id="content_zh">
					<div id="panel_zh"><span class="span">》当前位置：首页>>公告>>${noticeSort.sortName }</span></div>
					<div id="text_zh">
						<c:if test="${not empty page.list }">
							<ul id="notice_ul_zh">
								<c:forEach items="${page.list }" var="notice">
									<li class="notice_li_zh">
										<span class="notice_symbol_zh"></span>
										<a href="${pageContext.request.contextPath}/notice.htm?action=showNotice&noticeId=${notice.noticeId}">${notice.title }</a>
										<span class="notice_time_zh">（<fmt:formatDate value="${notice.lastUpdateTime}" pattern="yyyy-M-d"/>）</span>
									</li>
								</c:forEach>
							</ul>
							<div align="center">
								<jsp:include page="../../public/pager.jsp">
									<jsp:param
										value="${pageContext.request.contextPath}/notice.htm?action=showNoticeList&sortId=${noticeSort.sortId }"
										name="path" />
								</jsp:include>
							</div>
						</c:if>
						<c:if test="${empty  page.list}">暂无数据！</c:if>
					</div>
				</div>
			</div>
		</div>
		<div id="middle_right_zh"></div>
	</div>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>