// JavaScript Document
// pptShow的jQuery插件 meigang 2014-10-21 15:25
var mgang ={
    width : 600, //默认的loopId，这里是自动获取，在调用时无需设置
    height : 400,
    bwtPx : 50,
    loopDelay : 3000,
    maxCount : 3,
    startIndex : 0,
    urlTextMarginTop:400 *0.8,
    urlTextMarginLeft:100,
    pptShowLoopHandler:'',
    stayStartIndex:0

}
//jQuery对象函数，只要是jQuery对象就可以调用
jQuery.fn.extend({
    pptShow : function(options){
        var defaults = {
            width : 600, //默认的loopId，这里是自动获取，在调用时无需设置
            height : 400,
            bwtPx : 50,
            loopDelay : 3000,
            maxCount : 3,
            startIndex : 0,
            urlTextMarginTop:400 *0.8,
            urlTextMarginLeft:100
        };
        var option = $.extend(defaults, options);
        mgang.width = option.width;
        mgang.height = option.height;
        mgang.bwtPx = option.bwtPx;
        mgang.loopDelay = option.loopDelay;
        mgang.maxCount = option.maxCount;
        mgang.startIndex = option.startIndex;
        mgang.urlTextMarginLeft = option.urlTextMarginLeft;
        mgang.urlTextMarginTop = option.urlTextMarginTop;
        console.info(mgang);
        //返回轮播插件对象，方便于chain调用
        return $(this);
    },
    initPPTShow : function(){
        $(".pptItem img").attr("img_align","right");
        $("img[imgId='1']").attr("img_align","left");
        $(".urlText").hide();
        $("img[imgId='1']").parent().find(".urlText").show();
        $(".pptItem").css({
           width:mgang.width+mgang.bwtPx*(mgang.maxCount-1),
           height:mgang.height
        });
        $(".urlText").each(function(index,dom){
            var urlTextIndex = index +1;
            console.info(urlTextIndex);
            $(this).css({
               marginTop:mgang.urlTextMarginTop,
               marginLeft:(urlTextIndex-1)*mgang.bwtPx + mgang.urlTextMarginLeft
            });
        });
        $(".pptItem img").each(function(index,dom){
            var _imgId = $(this).attr("imgId");
            var _left ;
            if(_imgId==1){
                _left = 0;
            }else{
                _left = mgang.width-mgang.bwtPx;
            }
            $(this).css({
                zIndex:10+_imgId,
                left:(_imgId-1)*mgang.bwtPx + _left
            })
        });
        $(".pptItem img").bind("mouseover",function(){
            console.info($(this).attr("src"));
            var _imgId = $(this).attr("imgId");
            //绑定事件
            $(".pptItem img").each(function(index,dom){
                var out_imgId = $(dom).attr("imgId");
                var img_align = $(dom).attr("img_align");
                if(_imgId<out_imgId  && img_align == "left"){
                    $(dom).attr("img_align","right");
                    $(dom).animate({
                        left : "+="+(mgang.width-mgang.bwtPx)
                    }, 500);
                    //console.info(_img_Id + "right->left");
                }
                if(_imgId>=out_imgId && out_imgId!=1 && img_align == "right"){
                    $(dom).attr("img_align","left");
                    $(dom).animate({
                        left : "-="+(mgang.width-mgang.bwtPx)
                    }, 500);
                    //console.info(_img_Id + "left->right");
                }
            });
            $(this).addClass("currentImg");
            mgang.startIndex=mgang.startIndex + parseInt(_imgId) - 1;
            $(".urlText").hide();
            $(".currentImg").parent().find(".urlText").show();
            clearInterval(mgang.pptShowLoopHandler);
            console.info("clear");
        }).bind("mouseout",function(){
            if($(this).hasClass("currentImg")){
                $(this).removeClass("currentImg");
            }
            mgang.pptShowLoopHandler = setInterval(startLoopShow,mgang.loopDelay);
            console.info("start");
        }) ;
        return $(this);
    },
    openLoopShow : function(){
        mgang.pptShowLoopHandler = setInterval(startLoopShow,mgang.loopDelay);
        console.info("start");
        return $(this);
    }
});
function startLoopShow(){
    mgang.startIndex++;
    console.info("mgang.startIndex:"+mgang.startIndex);
    if(mgang.startIndex==1){
        console.info("one");
        $("img[imgId="+ mgang.startIndex +"]").parent().find(".urlText").show();
    }
    if(mgang.startIndex>1){
        var target = $("img[imgId="+ mgang.startIndex  +"]");
        var img_align = target.attr("img_align");
        if(img_align=="right"){
            target.animate({
                left : "-="+(mgang.width-mgang.bwtPx)
            }, 500);
            target.attr("img_align","left");
            $(".urlText").hide();
            target.parent().find(".urlText").show();
        }
    }
    if(mgang.startIndex == mgang.maxCount+1){
        console.info("startIndex"+mgang.startIndex);
        for(var i=mgang.startIndex;i>1;i--){
            var temp =  $("img[imgId="+ i +"]");
            img_align = temp.attr("img_align");
            if(img_align=="left"){
                temp.animate({
                    left : "+="+(mgang.width-mgang.bwtPx)
                }, 500);
                temp.attr("img_align","right");
                $(".urlText").hide();
                temp.parent().find(".urlText").show();
            }
        }
        mgang.startIndex = 0;
        $(".urlText").hide();
        $("img[imgId='1']").parent().find(".urlText").show();
    }
}