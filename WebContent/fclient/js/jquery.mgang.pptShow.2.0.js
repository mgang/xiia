// JavaScript Document
// pptShow的jQuery插件 meigang 2014-10-21 15:25

var mgang ={
    width : 600, //默认的loopId，这里是自动获取，在调用时无需设置
    height : 400,
    bwtPx : 500,
    loopDelay : 3000,
    maxCount : 3,
    startIndex : 0,
    urlTextMarginTop:400 *0.8,
    urlTextMarginLeft:500,
    pptShowLoopHandler:false,
    isInitOpenStart:false,
    stayStartIndex:0

}
//jQuery对象函数，只要是jQuery对象就可以调用
jQuery.fn.extend({
    //loopShow是loopShow插件的最重要的函数，$("#myLoopShowId").loopShow({})这样就可以初始化轮播插件
    pptShow : function(options){
        var defaults = {
            width : 600, //默认的loopId，这里是自动获取，在调用时无需设置
            height : 400,
            bwtPx : 500,
            loopDelay : 3000,
            maxCount : 3,
            startIndex : 0,
            urlTextMarginTop:400 *0.8,
            urlTextMarginLeft:500
        };
        var option = $.extend(defaults, options);
        mgang.width = option.width;
        mgang.height = option.height;
        mgang.bwtPx = option.bwtPx;
        mgang.loopDelay = option.loopDelay;
        mgang.maxCount = option.maxCount;
        mgang.startIndex = option.startIndex;
        mgang.urlTextMarginLeft = option.urlTextMarginLeft;
        mgang.urlTextMarginTop = option.urlTextMarginTop;
        console.info(mgang);
        //返回轮播插件对象，方便于chain调用
        return $(this);
    },
    initPPTShow : function(){
        $(".pptItem img").attr("img_align","right");
        $("img[imgId='1']").attr("img_align","left");
        $(".urlText").hide();
        $("img[imgId='1']").parent().find(".urlText").show();
        $(".pptItem").css({
           width:mgang.width+mgang.bwtPx*(mgang.maxCount-1),
           height:mgang.height
        });
        $(".urlText").each(function(index,dom){
            var urlTextIndex = index +1;
            console.info(urlTextIndex);
            $(this).css({
               marginTop:mgang.urlTextMarginTop,
               marginLeft:(urlTextIndex-1)*mgang.bwtPx + mgang.urlTextMarginLeft
            });
        });
        $(".pptItem img").each(function(index,dom){
            var _imgId = $(this).attr("imgId");
            var _left ;
            if(_imgId==1){
                _left = 0;
            }else{
                _left = mgang.width-mgang.bwtPx;
            }
            $(this).css({
                zIndex:10+_imgId,
                left:(_imgId-1)*mgang.bwtPx + _left
            })
        });
        $(".pptItem img").bind("mouseover",picMouseOver).bind("mouseout",function(e){

            if($(this).hasClass("currentImg")){
                $(this).removeClass("currentImg");
            }
            if(mgang.isInitOpenStart){
                mgang.pptShowLoopHandler = setInterval(startLoopShow,mgang.loopDelay);
                console.info("start");
            }


        }) ;
        setInterval(checkOrder,"1");
        return $(this);
    },
    openLoopShow : function(){

        mgang.pptShowLoopHandler = setInterval(startLoopShow,mgang.loopDelay);
        console.info(" open start");
        mgang.isInitOpenStart = true;

        return $(this);
    }
});
function startLoopShow(){
    mgang.startIndex++;
    console.info("mgang.startIndex:"+mgang.startIndex);
    if(mgang.startIndex==1){
        console.info("one");
        $("img[imgId="+ mgang.startIndex +"]").parent().find(".urlText").show();
    }
    if(mgang.startIndex>1){
        var target = $("img[imgId="+ mgang.startIndex  +"]");
        var img_align = target.attr("img_align");
        if(img_align=="right"){
            target.animate({
                left : "-="+(mgang.width-mgang.bwtPx)
            }, 500);
            target.attr("img_align","left");
            $(".urlText").hide();
            target.parent().find(".urlText").show();
        }
    }
    if(mgang.startIndex > mgang.maxCount){
        console.info("startIndex"+mgang.startIndex);
        for(var i=mgang.startIndex;i>1;i--){
            var temp =  $("img[imgId="+ i +"]");
            img_align = temp.attr("img_align");
            if(img_align=="left"){
                temp.animate({
                    left : "+="+(mgang.width-mgang.bwtPx)
                }, 500);
                temp.attr("img_align","right");
                $(".urlText").hide();
                temp.parent().find(".urlText").show();
            }
        }
        mgang.startIndex = 0;
        $(".urlText").hide();
        $("img[imgId='1']").parent().find(".urlText").show();
    }
}
function picMouseOver(event){
    console.info($(this).attr("src"));
    var _imgId = $(this).attr("imgId");
    var left = parseInt($(this).css("left"));
    console.info("left:"+left);
    $(".pptItem img").each(function(index,dom){
        var out_imgId = parseInt($(dom).attr("imgId"));
        var img_align = $(dom).attr("img_align");
        console.info("_imgId:"+_imgId+" -- out_imgId:"+out_imgId+" -- img_align:"+img_align);
        var tempWidth;
        if(_imgId==1){
            tempWidth=0;
        }else if(_imgId>1){
            tempWidth = mgang.width-mgang.bwtPx;
        }
        if(_imgId<out_imgId  && img_align == "left"){
            //在2toRight前，3,4,5.。。先toRight
            for(var i=mgang.maxCount;i>_imgId;i--){
                var _thisLeft = parseInt($("img[imgId="+ i+"]").css("left"));
                console.info("img i:"+i+"  -- _thisLeft:"+_thisLeft);
                if(_thisLeft==((i-1)*mgang.bwtPx)){
                    console.info("left:"+ $("img[imgId="+i+"]").attr("imgId")+"--> toRight");
                    if($("img[imgId="+i+"]").attr("img_align") == "left"){
                        $("img[imgId="+i+"]").attr("img_align","right");
                        $("img[imgId="+i+"]").animate({
                            left : "+="+(mgang.width-mgang.bwtPx)
                        }, 500);
                    }
                }
            }
            //console.info(_img_Id + "right->left");
        }
        if(_imgId>=out_imgId && img_align == "right"){
            for(var i=mgang.maxCount;i>1;i--){
                var _thisLeft = parseInt($("img[imgId="+ i+"]").css("left"));
                if(_thisLeft==((out_imgId-1)*mgang.bwtPx+tempWidth)){
                    console.info("right:"+$("img[imgId="+i+"]").attr("imgId")+"--> toLeft");
                    if($("img[imgId="+i+"]").attr("img_align") == "right"){
                        $("img[imgId="+i+"]").attr("img_align","left");
                        $("img[imgId="+i+"]").animate({
                            left : "-="+(mgang.width-mgang.bwtPx)
                        }, 500);
                    }
                }
            }

            //console.info(_img_Id + "left->right");
        }
    });
    $(this).addClass("currentImg");
    mgang.startIndex=mgang.startIndex+parseInt(_imgId)-2;
    $(".urlText").hide();
    $(".currentImg").parent().find(".urlText").show();
    if(mgang.isInitOpenStart){
        clearInterval(mgang.pptShowLoopHandler);
        mgang.pptShowLoopHandler = null;
        console.info("clear");
    }
}

function checkOrder(){
    //3在left,123都在left,2在right，345都在right
    //得到最大的那個img的img_align
    var maxImgAlign = $("img[imgId="+mgang.maxCount+"]").attr("img_align");
    if(maxImgAlign == "left"){
        $(".pptItem img").each(function(index,dom){
            var imgAlign = $(dom).attr("img_align");
            var _imgId = parseInt($(dom).attr("imgId"));
            if(imgAlign == "right" && _imgId!=1){
                $(dom).attr("img_align","left");
                $(dom).animate({
                    left : "-="+(mgang.width-mgang.bwtPx)
                }, 0);
            }
        });
    }
    //2在右，345都要在right
    var twoImgAlign = $("img[imgId='2']").attr("img_align");
    if(twoImgAlign == "right"){
        $(".pptItem img").each(function(index,dom){
            var imgAlign = $(dom).attr("img_align");
            var _imgId = parseInt($(dom).attr("imgId"));
            if(imgAlign == "left" && _imgId>2){
                $(dom).attr("img_align","right");
                $(dom).animate({
                    left : "+="+(mgang.width-mgang.bwtPx)
                }, 0);
            }
        });
    }
}