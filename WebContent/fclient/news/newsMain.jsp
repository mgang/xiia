<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/society/society.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/news/newsMain.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/notice/noticeMain.css" type="text/css"></link>
<title>公告-襄阳市信息产业协会</title>
</head>
<body>
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav_2.jsp"></jsp:include>
	<div id="middle_zh">
		<div id="middle_left_zh"><img src="${pageContext.request.contextPath }/fclient/image/left_2.jpg"/></div>
		<div id="middle_center_zh">
			<div id="menu_zh">
				<div style="background-color:#517693;" id="title_zh"><span>新&nbsp;&nbsp;闻</span></div>
				<ul id="items_zh">
				<c:forEach items="${newsSortList }" var="sort">
					<li class="item_zh">
						<a style="color:#517693;" href="${pageContext.request.contextPath}/newsSort.htm?action=newsSortListFclient&clickSort=${sort.sortId}">
							<img style="border:1px solid #517693;" src="${pageContext.request.contextPath}/fclient/image/item_ico_2.jpg"/>
							<span>${sort.sortName }</span>
						</a>
					</li>
				</c:forEach>
				</ul>
			</div>
			<div id="contentbg_zh">
				<div id="content_zh">
					<div id="panel_zh"><span class="span">》当前位置：首页>>新闻>><c:if test="${firstSort != \'123\'}">${firstSort.sortName }</c:if></span></div>
					<div id="text_zh">
						<ul id="notice_ul_zh">
						<c:if test="${page != \'123\' }">
							<c:forEach items="${page.list }" var="news">
								<li class="notice_li_zh">
									<span class="notice_symbol_zh"></span>
									<a href="${pageContext.request.contextPath}/newsSort.htm?action=newsSortListContent&clickNews=${news.newsId}&clickSort=${firstSort.sortId}">${news.title}</a>
									<span class="notice_time_zh">（${news.lastUpdateTime }）</span>
									<%-- <fmt:formatDate pattern="yyyy-MM-dd" value="${notice.lastUpdateTime}"/> --%>
								</li>
							</c:forEach>
						</c:if>
						<c:if test="${page == \'123\'}">
						<h2>空!</h2>
						</c:if>
						</ul>
						<div align="center" id="news_page" >
							<jsp:include page="../../public/pager.jsp">
							<jsp:param
								value="${pageContext.request.contextPath}/news.do?action=newsList&newsSortId=${sort.sortId }"
								name="path" />
							</jsp:include>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="middle_right_zh"></div>
	</div>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>