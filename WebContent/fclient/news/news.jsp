<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/society/society.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/notice/notice.css" type="text/css"></link>
<title>${notice.title }</title>
</head>
<body>
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav_2.jsp"></jsp:include>
	<div id="middle_zh">
		<div id="middle_left_zh">
			<img src="${pageContext.request.contextPath}/fclient/image/left_4.jpg"/>
		</div>
		<div id="middle_center_zh">
			<div id="menu_zh">
				<div style="background-color:#C35C0F;" id="title_zh"><span>新&nbsp;&nbsp;闻</span></div>
				<ul id="items_zh">
				<c:forEach items="${newsSortList }" var="sort">
					<li class="item_zh">
						<a style="color:#C35C0F;" href="${pageContext.request.contextPath}/newsSort.htm?action=newsSortListFclient&clickSort=${sort.sortId}">
							<img style="border:1px solid #C35C0F;" src="${pageContext.request.contextPath}/fclient/image/item_ico_4.jpg"/>
							<span>${sort.sortName }</span>
						</a>
					</li>
				</c:forEach>
				</ul>
			</div>
			<div id="contentbg_zh">
				<div style="border:0px solid green;" id="content_zh">
					<div id="panel_zh"><span class="span">》当前位置：首页>>新闻>>	<c:if test="${firstSort != \'123\'}">${firstSort.sortName }
				</c:if></span></div>
					<div style="position:relative;border:0px solid yellow;" id="text_zh">
						<div id="notice_head_zh">
							<span id="notice_title_zh">${news.title }</span><br/>
							<span id="notice_content_zh">
								时间：${news.lastUpdateTime }
								撰写人：${news.user.userId }
							</span>
						</div>
						 <hr/>
						<div id="notice_content_zh">${news.content }</div>
					</div>
				</div>
			</div>
		</div>
		<div id="middle_right_zh"></div>
	</div>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>