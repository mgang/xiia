<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8> 
<title>首页</title>
</head>
<body onload="startTime()">
	<jsp:include page="/public/importIndex.jsp"></jsp:include>
	<jsp:include page="/public/head.jsp"></jsp:include>
	<jsp:include page="/public/logo.jsp"></jsp:include>
	<jsp:include page="/public/nav.jsp"></jsp:include>
	<div id="news_top">
		<jsp:include page="/public/pptShow.jsp"></jsp:include>
	</div>
	<jsp:include page="/public/menu.jsp"></jsp:include>
	<jsp:include page="/public/foot.jsp"></jsp:include>
</body>
</html>