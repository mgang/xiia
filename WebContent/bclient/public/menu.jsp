﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>
<!DOCTYPE html>
<meta charset="utf-8" />
<jsp:include page="../public/import.jsp"></jsp:include>
<style type="text/css">
</style>
<div id="menu">
	<jsp:include page="module/sysSettingFuns.jsp"></jsp:include>
	<jsp:include page="module/cmsManageFuns.jsp"></jsp:include>
	<jsp:include page="module/itemManageFuns.jsp"></jsp:include>
	<jsp:include page="module/checkPublishFuns.jsp"></jsp:include>
</div>