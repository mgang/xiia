﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="mg" uri="http://room.mgang.com/checkFunction" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/bclient/css/top.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/bclient/js/top.js" type="text/javascript"></script>
</head>
<table id="topBg" class="adminBGColor" width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width='20%' height="60"><img alt="" src="${pageContext.request.contextPath}/bclient/image/cloudServer.gif"></td>
    <td width="10%" height="60" valign="middle"><jsp:include page="../../public/timer.jsp"></jsp:include></td>
    <td width='80%' align="right" valign="bottom">
    	<table width="750" border="0" cellspacing="0" cellpadding="0">
      		<tr>
     			<td align="right" height="26" style="font-size:12px; padding-right:10px;line-height:26px;">
        			您好：<span>${loginUser.userName }</span>，欢迎使用襄阳信息产业协会云平台服务中心!
        			<div id="topLink">
	        			<a href="${pageContext.request.contextPath}" target="_blank">网站首页</a>
	        			<a href="${pageContext.request.contextPath}/bclient/main.jsp" target="main">后台首页</a>
	        			<c:if test="${mg:check(loginUser,'bclient/user/updatePwd.jsp') }">
	        				<a href="${pageContext.request.contextPath}/bclient/user/updatePwd.jsp" target="main">修改密码</a>
	        			</c:if>
	        			<a target="_top" href="${pageContext.request.contextPath}/gf?action=logout" onclick="return confirm('您真的要退出系统吗？');">安全退出</a>
        			</div>
      			</td>
      		</tr>
      		<tr>
        		<td align="right" height="" class="rmain">
				<dl id="tpa">
					<dd><div class='itemSel' id='item2'><a href="menu.jsp" onclick="changeSel(2)" target="menu">主菜单</a></div></dd>	
					<dd><div class='item' id='item3'><a href="sysSetting.jsp" onclick="changeSel(3)" target="menu">系统设置</a></div></dd>
					<dd><div class='item' id='item4'><a href="cmsManager.jsp" onclick="changeSel(4)" target="menu">内容管理</a></div></dd>
					<dd><div class='item' id='item5'><a href="itemManager.jsp" onclick="changeSel(5)" target="menu">栏目管理</a></div></dd>
					<dd><div class='item' id='item6'><a href="checkPublish.jsp" onclick="changeSel(6)" target="menu">审核发布</a></div></dd>
				</dl>
			</td>
      		</tr>
   	</table>
	</td>
  	</tr>
</table>