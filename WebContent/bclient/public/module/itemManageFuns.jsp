<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="menuItem"><span class="divTitle" st="on">栏目管理</span>
		
	<ul class="backMenu">
		<span class="ulTitle" st="on">广告</span>
		<li><a href="${pageContext.request.contextPath}/bclient/ad/entryAd.jsp" target="main">录入广告@</a></li>
		<li><a href="${pageContext.request.contextPath}/ad.do?action=showUpdate" target="main">查看广告列表@</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">公告@</span>
		<li><a href="${pageContext.request.contextPath }/noticeSort.do?action=toNoticeSortManagerUI" target="main">查看公告分类</a></li>
		<li><a href="${pageContext.request.contextPath }/notice.do?action=toNoticeListManagerUI" target="main">查看公告列表</a></li>
		<li><a href="${pageContext.request.contextPath }/noticeSort.do?action=toAddNoticeSortUI" target="main">新建公告分类</a></li>
		<li><a href="${pageContext.request.contextPath }/noticeSort.do?action=toAddNoticeUI" target="main">添加公告</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">政策法规</span>
		<li><a href="${pageContext.request.contextPath}/ruleSort.do?action=createRule" target="main">新建法规@</a></li>
		<li><a href="${pageContext.request.contextPath}/rule.do?action=findAllRule" target="main">查看法规列表@</a></li>
		<li><a href="${pageContext.request.contextPath}/bclient/rule/createRuleSort.jsp" target="main">新建法规分类@</a></li>
		<li><a href="${pageContext.request.contextPath}/ruleSort.do?action=findAllRuleSort" target="main">查看分类列表@</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">分支机构</span>
		<li><a href="${pageContext.request.contextPath}/bclient/org/createOrg.jsp" target="main">新建分支机构@</a></li>
		<li><a href="${pageContext.request.contextPath}/org.do?action=findAllOrg" target="main">查看机构列表@</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">资源</span>
		<li><a href="${pageContext.request.contextPath}/bclient/resource/addResourceSort.jsp" target="main">添加资源分类@</a></li>
		<li><a href="${pageContext.request.contextPath}/resourceSort.do?action=list" target="main">查看分类列表@</a></li>
		<li><a href="${pageContext.request.contextPath}/resource.do?action=toAddResourceUI" target="main">添加资源@</a></li>
		<li><a href="${pageContext.request.contextPath}/resource.do?action=list" target="main">查看资源列表@</a></li>
		
		
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">新闻</span>
		<li><a href="${pageContext.request.contextPath}/newsSort.do?action=newsSortListTrue" target="main">查看新闻分类</a></li>
		<li><a href="${pageContext.request.contextPath}/news.do?action=allNewsList" target="main">查看所有新闻</a></li>
		<li><a href="${pageContext.request.contextPath}/bclient/news/newsSort.jsp" target="main">添加新闻分类</a></li>
		<li><a href="${pageContext.request.contextPath}/newsSort.do?action=newsSortListFalse" target="main">添加新闻</a></li>
		<li><a href="${pageContext.request.contextPath}/news.do?action=noPass" target="main">审核未通过</a></li>
		
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">项目</span>
		<li><a href="${pageContext.request.contextPath}/bclient/project/addProject.jsp" target="main">新增项目@</a></li>
		<li><a href="${pageContext.request.contextPath}/project.do?action=list" target="main">查看项目列表@</a></li>
	
	</ul>
</div>