<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="menuItem"><span class="divTitle" st="on">审核发布管理</span>
	<ul class="backMenu">
		<span class="ulTitle" st="on">审核发布管理@</span>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=rule" target="main">审核政策法规</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=org" target="main">审核分支机构</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=ad" target="main">广告[审核 发布 撤销]</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=news" target="main">新闻[审核 发布 撤销]</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=notice" target="main">公告[审核 发布 撤销]</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=project" target="main">项目[审核 发布 撤销]</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=resource" target="main">资源[审核 发布 撤销]</a></li>
		<li><a href="${pageContext.request.contextPath}/check.do?action=list&type=menu" target="main">栏目[审核 发布 撤销]</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">相关信息</span>
		<li><a href="${pageContext.request.contextPath}/check.do?action=listAll" target="main">查看审核信息@</a></li>
		<li><a href="${pageContext.request.contextPath}/publish.do?action=listAll" target="main">查看发布信息@</a></li>
		<li><a href="${pageContext.request.contextPath}/revoke.do?action=listAll" target="main">查看撤销信息@</a></li>
		
	</ul>
</div>