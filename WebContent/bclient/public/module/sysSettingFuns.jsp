<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mg" uri="http://room.mgang.com/checkFunction" %>
<div class="menuItem"><span class="divTitle" st="on">系统设置</span>
	<ul class="backMenu">
		<span class="ulTitle" st="on">用户设置</span>
		<c:if test="${mg:check(loginUser,'bclient/user/updatePwd.jsp') }">
			<li><a href="${pageContext.request.contextPath}/bclient/user/updatePwd.jsp" target="main">修改密码@</a></li>
		</c:if>
		<c:if test="${mg:check(loginUser,'admin.do?action=listUser&type=user') }">
			<li><a href="${pageContext.request.contextPath}/admin.do?action=listUser&type=user" target="main">查看会员列表@</a></li>
		</c:if>
		<c:if test="${mg:check(loginUser,'admin.do?action=listUser&type=admin') }">
			<li><a href="${pageContext.request.contextPath}/admin.do?action=listUser&type=admin" target="main">查看管理员列表@</a></li>
		</c:if>
		<c:if test="${mg:check(loginUser,'bclient/user/addAdmin.jsp') }">
			<li><a href="${pageContext.request.contextPath}/bclient/user/addAdmin.jsp" target="main">新增管理员@</a></li>
		</c:if>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">用户权限设置</span>
		<c:if test="${mg:check(loginUser,'bclient/user/addRole.jsp') }">
			<li><a href="${pageContext.request.contextPath}/bclient/user/addRole.jsp" target="main">添加角色@</a></li>
		</c:if>
		<c:if test="${mg:check(loginUser,'role.do?action=listRole') }">
			<li><a href="${pageContext.request.contextPath}/role.do?action=listRole" target="main">查看角色列表@</a></li>
		</c:if>
		<c:if test="${mg:check(loginUser,'bclient/user/addFun.jsp') }">
			<li><a href="${pageContext.request.contextPath}/bclient/user/addFun.jsp" target="main">添加权限@</a></li>
		</c:if>
		<c:if test="${mg:check(loginUser,'function.do?action=listFunction') }">
			<li><a href="${pageContext.request.contextPath}/function.do?action=listFunction" target="main">查看权限列表@</a></li>
		</c:if>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">栏目设置</span>
		<li><a href="${pageContext.request.contextPath}/menu.do?action=showAllMenuByPager" target="main">查看栏目@</a></li>
		<li><a href="${pageContext.request.contextPath}/menu.do?action=showCanUpdateMenu" target="main">修改栏目@</a></li>
		<li><a href="${pageContext.request.contextPath}/menu.do?action=searchShowForntMenu" target="main">新增栏目@</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">系统参数设置</span>
		<li><a href="${pageContext.request.contextPath}/setting.do?action=toUpdateSettingUI" target="main">变更系统参数@</a></li>
	</ul>
</div>