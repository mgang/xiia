<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="menuItem"><span class="divTitle" st="on">站内内容管理</span>
	
	<ul class="backMenu">
		<span class="ulTitle" st="on">文章</span>
		<li><a href="#">文字广告</a></li>
		<li><a href="#">普通新闻</a></li>
		<li><a href="#">公告</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">图片</span>
		<li><a href="#">图片广告</a></li>
		<li><a href="#">头条新闻</a></li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">视频</span>
		<li>暂无</li>
	</ul>
	<ul class="backMenu">
		<span class="ulTitle" st="on">其它资源</span>
		<li><a href="#">栏目</a></li>
		<li><a href="#">分支机构</a></li>
		<li><a href="#">政策法规</a></li>
		<li><a href="#">项目</a></li>
		<li><a href="#">资源</a></li>
	</ul>
</div>