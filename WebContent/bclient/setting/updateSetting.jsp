<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<link
	href="${pageContext.request.contextPath}/bclient/css/setting/setting.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/bclient/css/user/admin.css"
	type="text/css" rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css"
	type="text/css" rel="stylesheet" />
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">系统参数设置&gt;变更系统参数</div>
	<div class="funContent">
		<form style="width:80%;padding: 10px;"
			action="${pageContext.request.contextPath}/setting.do?action=updateSetting"
			id="updateSettingForm" method="post">
			<table
				class="table table-condensed table-bordered table-striped table-hover table12"
				cellspacing="0" cellpadding="0">
				<tr>
					<th style="width: 300px;">系统参数</th>
					<th>系统参数值</th>
				</tr>
				<tr>
					<td>会员注册默认积分:</td>
					<td align="left"><input type="number" name="menmberScore" value="${setting.menmberScore }" required></td>
				</tr>
				<tr>
					<td>新增管理员默认积分:</td>
					<td align="left"><input type="number" name="adminScore" value="${setting.adminScore }" required></td>
				</tr>
				<tr>
					<td>前台显示头条的数量:</td>
					<td align="left"><input type="number" name="headlineNewsNum" value="${setting.headlineNewsNum }" required></td>
				</tr>
				<tr>
					<td>前台显示一级栏目的最大数量:</td>
					<td align="left"><input type="number" name="menuMaxCount" value="${setting.menuMaxCount }" required></td>
				</tr>
				<tr>
					<td>支持上传资源的文件类型:<br/><span style="color:red;">以逗号分隔</span></td>
					<td align="left"><input type="text" name="acceptUploadFileType" value="${setting.acceptUploadFileType }" required></td>
				</tr>
				<tr>
					<td>支持上传资源的文件大小:<br/><span style="color:red;">单位：M</span></td>
					<td align="left"><input type="number" name="uploadResourceSize" value="${setting.uploadResourceSize }" required></td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<input type="hidden" id="msg" value="${resString }" />
						<input type="hidden" name="settingId" value="${setting.settingId }"/>
						<input type="submit" value=" 更新系统参数 " />
						提示：系统参数更新后，将会立即生效。
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$("#updateSettingForm").validate({
			errorClass: "inputError",
			rules:{
				menmberScore:{
					required:true,
					digits:true,
					max:50
				},
				adminScore:{
					required:true,
					digits:true,
					max:1000
				},
				headlineNewsNum:{
					required:true,
					digits:true,
					max:8
				},
				menuMaxCount:{
					required:true,
					digits:true,
					max:6
				},
				acceptUploadFileType:{
					required:true,
					minlength:4,
					maxlength:64
				},
				uploadResourceSize:{
					required:true,
					digits:true,
					max:10
				}
			},
			messages:{
				menmberScore:{
					required:"会员注册默认积分不能为空",
					digits:"必须是整数",
					max:"最大不超过50"
				},
				adminScore:{
					required:"添加管理员默认积分不能为空",
					digits:"必须是整数",
					max:"最大不超过1000"
				},
				headlineNewsNum:{
					required:"前台头条显示参数不能为空",
					digits:"必须是整数",
					max:"最大不超过8"
				},
				menuMaxCount:{
					required:"前台一级栏目显示最大数量不能为空",
					digits:"必须是整数",
					max:"最大不超过6"
				},
				acceptUploadFileType:{
					required:"支持的上传资源的文件类型不能为空",
					minlength:"不能少于4个字符",
					maxlength:"不能多于64个字符"
				},
				uploadResourceSize:{
					required:"上传资源的文件大小不能为空",
					digits:"必须是整数",
					max:"最大是10M"
				}
			}
		});
		//弹出反馈信息
		alertMsg("msg");
	});
</script>    