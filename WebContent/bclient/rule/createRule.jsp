<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;法规分类&gt;新建法规
	</div>
		<form action="${pageContext.request.contextPath}/rule.do?action=createRule" method="post" id="rule" >  
			<b>新建法规:</b><br>
			法规名称:<input type="text" name="ruleName" required="required" id="name" onblur="checkName()"><br>
			法规链接:<input type="text" name="ruleLink" required="required"><br>
			所属分类  
			<select name="ruleSortId" required="required">
					 	<c:forEach items="${List}" var="ruleSort">
							<option  value="${ruleSort.sortId}" >${ruleSort.sortName}</option>
						</c:forEach>
					</select> 
			<input type="submit" value="确定" class="rule_submit">
		</form>				
	</div>
<script type="text/javascript" charset="utf-8">
var i=0;
$(function(){
	$("#rule").validate({
		errorClass:"inputError",
		rules:{
			ruleName:{
				required:true,
				maxlength:16
			},
			ruleLink:{
				required:true,
				maxlength:16
			}		
		},
		messages:{
			ruleName:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			},
			ruleLink:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			}
		}
	})
});
function checkName(){
	var name = $("#name").val();
	if($("#name").val()!=''){
		$.ajax({
			url:getRootPath()+"/ajaxServlet",
			data:{"action":"ruleName","name":name},
			type:'post',
			dataType:"json",
			success:function(res){
				if(res.msg=="T"){
					alert("该法规已存在！");
					$("#name").val("");
				}
				
			}
		})
	}
}

</script>

