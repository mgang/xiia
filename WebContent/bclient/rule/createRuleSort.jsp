<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;法规分类&gt;新建法规分类
	</div>
		<b>新建法规分类</b>
		<form action="${pageContext.request.contextPath}/ruleSort.do?action=creatRuleSort" method="post" 
		      id="rulesort">  
			新建分类:<input type="text" name="sortName" id="name" onblur="checkName()"><br>
			分类描述:<input type="text" name="note"><br>
			<input type="submit" value="确定" class="sort_submit">
		</form>				
</div>

<script type="text/javascript">
$(function(){
	$("#rulesort").validate({
		errorClass:"inputError",
		rules:{
			sortName:{
				required:true,
				maxlength:16
			},
			note:{
				required:true,
				maxlength:16
			}		
		},
		messages:{
			sortName:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			},
			note:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符",
			}
		}
	})
});
function checkName(){
	var name = $("#name").val();
	if(name!=""){
		$.ajax({
			url:getRootPath()+"/ajaxServlet",
			data:{"action":"sortName","name":name},
			type:'post',
			dataType:"json",
			success:function(res){
				if(res.msg=="T"){
					alert("该分类已存在！");
					$("#name").val("");
				}
			}
		})
	}
}
</script>