<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;法规分类&gt;修改法规
	</div>
		<form action="${pageContext.request.contextPath}/rule.do?action=updateRule&ruleId=${rule.ruleId}" method="post" id="rule"
			  onsubmit="return goSubmit();">  
			<b>修改法规:</b><br>
			<input type="hidden" name="oSortId" value="${rule.sortId}">
			法规名称：<input type="text" name="ruleName" required="required" value="${rule.title}" id="name" onblur="checkName()"><br>
			法规连接：<input type="text" name="ruleLink" required="required" value="${rule.link}"><br>
			所属法规分类：<select name="ruleSortId" required="required">
			<c:forEach items="${list}" var="ruleSort">
 				<option value="${ruleSort.sortId}">${ruleSort.sortName}</option>
			</c:forEach>
			</select>
			<input type="submit" value="确定" class="sort_submit">
		</form>				
	</div>

<script type="text/javascript" charset="utf-8">
$(function(){
	$("#rule").validate({
		errorClass:"inputError",
		rules:{
			ruleName:{
				required:true,
				maxlength:16
			},
			ruleLink:{
				required:true,
				maxlength:16
			}		
		},
		messages:{
			ruleName:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			},
			ruleLink:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			}
		}
	})
});
function checkName(){
	var name = $("#name").val();
	if($("#name").val()!=''){
		$.ajax({
			url:getRootPath()+"/ajaxServlet",
			data:{"action":"ruleName","name":name},
			type:'post',
			dataType:"json",
			success:function(res){
				if(res.msg=="T"){
					alert("该法规已存在！");
					$("#name").val("");
				}
				
			}
		})
	}
}
</script>