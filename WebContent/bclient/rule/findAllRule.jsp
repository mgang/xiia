<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;法规分类&gt;查看分类列表
	</div>
	<div class="funContent">
		<form action="${pageContext.request.contextPath}/rule.do?action=deleteRule" method="post" onsubmit="return deleter()">
		<table class="table table-condensed table-bordered table-striped table-hover table12" cellspacing="0" cellpadding="0">
		    	<tr>
				<td>编号</td>
				<td>法规标题</td>
				<td>法规链接</td>
				<td>法规添加时间</td>
				<td>法规状态</td>
				<td>所属分类</td>
				<td>添加人</td>
				<td colspan=2>&nbsp;&nbsp;操作</td>
			</tr>
			<%
				int a=0;
			%>
			<c:if test="${empty page.list}">
				<td colspan="10">暂无数据！</td>
			</c:if>
			<c:forEach items="${page.list}" var="rule">
			<tr>
				<%
					a=a+1;
				%>
				<td><%=a%></td>
				<td>${rule.title}</td>
				<td><a href="http://${rule.link}" target="blank">${rule.link}</a></td>
				<td>${rule.addTime}</td>
				<c:if test="${rule.status eq 1}">
					<td>编辑完成</td>
				</c:if>
				<c:if test="${rule.status eq 2}">
					<td>待审核</td>
				</c:if>
				<c:if test="${rule.status eq 3}">
					<td>审核通过</td>
				</c:if>
				<c:if test="${rule.status eq 4}">
					<td>审核不通过</td>
				</c:if>
				<c:if test="${rule.status eq 5}">
					<td>已发布</td>
				</c:if>
				<c:if test="${rule.status eq 6}">
					<td>已撤销</td>
				</c:if>
				<td>${rule.ruleSort.sortName}</td>
				<td>${rule.user.userName}</td>
				<td><a href="${pageContext.request.contextPath}/rule.do?action=editRule&ruleId=${rule.ruleId}">修改</a></td>
				<td><input type="checkbox" value="${rule.ruleId }" name="ruleId" class="checkbox"></td>	
			</tr>
			</c:forEach>
		</table>	
			<center><input type="submit" value="删除" ></center>
		</form>	
		<div align="center">
			<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/rule.do?action=findAllRule"
				name="path"/>
			</jsp:include>
		</div>	
	</div>
</div>
<script type="text/javascript">
	var i=0;
	function deleter(){
		$(".checkbox").each(function(index,dom){
			if(this.checked){
				i=1;
			}
		})
		if(i==0){
			alert("您还没有选择!");
			return false;
		}
		else{
			return true;
		}
	}
</script>
