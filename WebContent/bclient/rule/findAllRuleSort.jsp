<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;法规分类&gt;查看分类列表
	</div>
	<div class="funContent">
		<form action="${pageContext.request.contextPath}/ruleSort.do?action=deleteRuleSort" method="post" onsubmit="return deleter()">
		<table class="table table-condensed table-bordered table-striped table-hover table12" cellspacing="0" cellpadding="0">
		    	<tr>
				<td>编号</td>
				<td>分类名称</td>
				<td>分类描述</td>
				<td>法规数量</td>
				<td>创建时间</td>
				<td>修改时间</td>
				<td>添加人</td>
				<td>修改</td>
				<td colspan=2>操作</td>
			</tr>
			<%
				int a=0;
			%>
			<c:if test="${empty page.list}">
				<td colspan="10">暂无数据！</td>
			</c:if>
			<c:forEach items="${page.list}" var="ruleSort">
			<tr>
				<%
					a=a+1;
				%>
				<td><%=a%></td>
				<td>${ruleSort.sortName}</td>
				<td>${ruleSort.note}</td>
				<td>${ruleSort.ruleNum}</td>
				<td>${ruleSort.createTime}</td>
				<td>${ruleSort.lastUpdateTime}</td>
				<td>${ruleSort.user.userName}</td>	
				<td><a href="${pageContext.request.contextPath}/ruleSort.do?action=editRuleSort&ruleSortId=${ruleSort.sortId}">修改</a></td>
				<td><input type="checkbox" name="ruleSortId" value="${ruleSort.sortId }" class="checkbox"></td>
			</tr>
			</c:forEach>
		</table>	
		<center><input type="submit" value="删除"></center>
		</form>		
	</div>
	<div align="center">
			<jsp:include page="../../public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/ruleSort.do?action=findAllRuleSort"
					name="path" />
			</jsp:include>
		</div>
</div>
<script type="text/javascript">
	var i=0;
	function deleter(){
		$(".checkbox").each(function(index,dom){
			if(this.checked){
				i=1;
			}
		})
		if(i==0){
			alert("您还没有选择!");
			return false;
		}
		else{
			return true;
		}
	}
</script>