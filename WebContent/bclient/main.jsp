﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/bclient/css/main.css" type="text/css" rel="stylesheet"/>    
<div class="bclient_banner">
	<img alt="" src="${pageContext.request.contextPath}/bclient/image/bclient_main.png">
</div>
<div class="useHelp">
	<div class="mainTitle">
		　系统使用小助手
	</div>
	<pre style="color:gray;">
1.当您登陆进来后，您拥有的操作（功能）完全取决于您拥有的权限。
2.当管理员的权限发生变更时，不会立即生效，需要重新登陆。
	</pre>
</div>
<div class="friendLink">
	<div class="mainTitle">
		　友情链接
	</div>
	<div class="link">
		<a href="${pageContext.request.contextPath}" target="_blank">襄阳市信息产业协会</a>
		<a href="http://www.xf.gov.cn/" target="_blank">襄阳市政府</a>
	</div>
</div>