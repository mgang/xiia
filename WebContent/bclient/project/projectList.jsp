<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link
	href="${pageContext.request.contextPath}/bclient/css/project/project.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css"
	type="text/css" rel="stylesheet" />
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;资源&gt;查看项目列表</div>
	<div class="funContent">
		<table
			class="table table-condensed table-bordered table-striped table-hover table12"
			cellspacing="0" cellpadding="0">
			<tr>
			<th>项目编号</th>				
			<th>项目名称</th>
			<th>项目描述</th>
			<th>添加时间</th>
			<th>创建人</th>	
			<th>状态</th>
			<th>操作</th>
			</tr>
			<c:if test="${empty page.list }">
				<tr>
					<td colspan="7">暂无资源</td>
				</tr>
			</c:if>
			<c:forEach items="${page.list }" var="p">
			<tr>
				<td>${p.projectId}</td>				
				<td>${p.projectName}</td>
				<td>${p.note}</td>
				<td>${p.addTime}</td>
				<td>${p.user.userName}</td>
				<td>
					<c:if test="${p.status eq 1 }">编辑完成</c:if>
					<c:if test="${p.status eq 4 }">审核不通过</c:if>
				</td>
				<td>
				<a href="${pageContext.request.contextPath}/project.do?action=askCheck&projectId=${p.projectId}" onclick="return confirm('您真的要对该项目申请审核吗？')">申请审核</a>
				<a href="${pageContext.request.contextPath}/project.do?action=toChange&projectId=${p.projectId}" onclick="return confirm('您真的要修改该项目名称吗？')">修改</a>
				<a href="${pageContext.request.contextPath}/project.do?action=deleteProject&projectId=${p.projectId}" onclick="return confirm('您真的要删除该项目吗？');">删除</a>	
				</td>
			</tr>
		</c:forEach>		
		</table>
		 <input type="hidden" value="${resString }" id="msg"/>
	</div>
	<div align="center">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/project.do?action=list"
				name="path" />
		</jsp:include>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function(){
		//弹出反馈信息
		alertMsg("msg");
	});
</script>    