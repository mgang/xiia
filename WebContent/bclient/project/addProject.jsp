<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<link
	href="${pageContext.request.contextPath}/bclient/css/project/project.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/bclient/css/user/admin.css"
	type="text/css" rel="stylesheet" />
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;项目管理&gt;新增项目</div>
	<div class="funContent">
		<form
			action="${pageContext.request.contextPath}/project.do?action=addProject"
			id="addProjectForm" method="post">
			<table>
				<tr>
					<td>项目名称:</td>
					<td><input type="text" name="projectName" required></td>
					<td id="msg_have"></td>
				</tr>

				<tr>
					<td>项目描述:</td>
					<td><textarea rows="5" cols="32" name="note" class="note"
							required></textarea></td>
				</tr>
				<tr>
					<td><input type="hidden" id="msg" value="${resString }" /></td>
					<td><input type="submit" value=" 新增项目 " />
					</td>
					<td>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$("#addProjectForm").validate({
			errorClass: "inputError",
			rules:{
				projectName:{
					required:true,
					minlength:4,
					maxlength:16
				},
				note:{
					required:true,
					minlength:6,
					maxlength:200
				}
			},
			messages:{
				projectName:{
					required:"项目名不能为空",
					minlength:"最少4个字符",
					maxlength:"最多16个字符"
				},
				note:{
					required:"项目描述不能为空",
					minlength:"最少6个字符",
					maxlength:"最多200个字符"
				}
			}
		});
		//弹出反馈信息
		alertMsg("msg");
		
	});
</script> 