<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<link
	href="${pageContext.request.contextPath}/bclient/css/user/admin.css"
	type="text/css" rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/bclient/css/ad/entryAd.css"
	type="text/css" rel="stylesheet" />


<div class="main">
	<div class="funTitle">广告&gt;录入广告</div>
	<div class="content">
		<form id="entry_ad"
			action="${pageContext.request.contextPath}/ad.do?action=entry"
			method="post" enctype="multipart/form-data">
			&nbsp;名&nbsp;&nbsp;称&nbsp; <input type="text" name="adName"><br />
			&nbsp;类&nbsp;&nbsp;型&nbsp; <select id="adType" name="adType" size="1"><br />
				<option value="1" selected>文字广告</option>
				<option value="2">图片广告</option>
				<option value="3">Flash广告</option>
			</select> <br /> 
			<div id="area">
			
			</div>
			<input type="hidden" id="msg" value="${resString }" /> <input
				type="submit" value=" 确认添加广告 ">
		</form>
	</div>
</div>
<div class="wordArea" st="out" style="display: none;">
	文字内容
	<textarea rows="4" cols="30" name="content"></textarea>
	<br />
</div>
<div class="fileArea" st="out" style="display: none;">
	广告文件<input type="file" name="adFile" />支持的广告文件大小10M<br /> 目的链接<input
		type="text" name="adPath"><br />
</div>
<script type="text/javascript" charset="utf-8">
	$(function() {
		//初始化界面
		$(".wordArea").clone().show().appendTo($("#area"));
		$("#adType").bind("change",function(index,dom){
			var _val = $(this).val();
			$("#area").children().remove();
			if(_val == 1){
				$(".wordArea").clone().show().appendTo($("#area"));
			}else if(_val == 2 || _val == 3){
				$(".fileArea").clone().show().appendTo("#area");
			}
		});
		$("#entry_ad").validate({
			errorClass : "inputError",
			success : "inputSuccess",
			rules : {
				adName : {
					required : true,
					minlength : 2,
					maxlength : 128
				},
				adPath : {
					required : true,
					minlength : 6,
					maxlength : 128,
					url : true
				},
                content :{
                	required : true,
				    minlength : 20,
				    maxlength : 256,
                }
			},
			messages : {
				adName : {
					required : "您的名称不能为空",
					minlength : "您的名称不能少于2个字符",
					maxlength : "您的名称不能多于64个字符"
				},
				adPath : {
					required : "您的链接不能为空",
					minlength : "您的链接不能少于6个字符",
					maxlength : "您的链接不能多余64个字符",
					url : "您的格式输入错误"
				},
			}
		});
		//弹出反馈信息
		alertMsg("msg");

	});
</script>