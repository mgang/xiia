<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
     <jsp:include page="../public/importValidate.jsp"></jsp:include>
     <link href="${pageContext.request.contextPath}/bclient/css/ad/entryAd.css" type="text/css" rel="stylesheet"/>
     <link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<body>
  <div class="main">
    <div class="funTitle">
		　广告&gt;广告列表&gt;修改页面
    </div>
    <div class="content">
     <form id="update_ad" action="${pageContext.request.contextPath }/ad.do?action=updateLink" method="post">
      <c:forEach items="${adv }" var="advert">          
                 编号:${advert.adId }<br>
                 名称:${advert.adName }<br>
      <input type="hidden" name="adId" value="${advert.adId }">
                  链接:<input type="text" name="adPath" value="${advert.adPath }"><br>
      </c:forEach>
      <input type="submit" value="修改">  
     </form>
    </div>
  </div>
</body>
</html>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$("#update_ad").validate({
			errorClass: "inputError",
			success: "inputSuccess",
			rules:{
				adPath:{
					required:true,
					minlength:6,
					maxlength:64,
					url:true
				},
				
				
			},
			messages:{
				adPath:{
					required:"您的链接不能为空",
					minlength:"您的链接不能少于6个字符",
					maxlength:"您的链接不能多余64个字符",
					url:"您的格式输入错误"
				},
			}
		});
		//弹出反馈信息
		alertMsg("updateMsg");
		
	});
</script>
 
