<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link
	href="${pageContext.request.contextPath}/bclient/css/ad/entryAd.css"
	type="text/css" rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/bclient/css/ad/ad_table.css"
	type="text/css" rel="stylesheet" />
<script type="text/javascript">
    window.onload=function(){
    	if('${ret}'!=''){
    		alert('${ret}');
    	}
    }
</script>
<body>
	<div class="main">
		<div class="funTitle">广告&gt;广告列表</div>
		<div class="content">
			<table
				class="table table-condensed table-bordered table-striped table-hover table12"
				cellspacing="0" cellpadding="0">
				<tr>
					<th>&nbsp;编&nbsp;&nbsp;号&nbsp;</th>
					<th>&nbsp;名&nbsp;&nbsp;称&nbsp;</th>
					<th>&nbsp;类&nbsp;&nbsp;型&nbsp;</th>
					<th>&nbsp;内&nbsp;&nbsp;容&nbsp;</th>
					<th>&nbsp;链&nbsp;&nbsp;接&nbsp;</th>
					<th>&nbsp;状&nbsp;&nbsp;态&nbsp;</th>
					<th>创建时间</th>
					<th>最新更新链接时间</th>
					<th>用户ID</th>
					<th>操作</th>

				</tr>
				<c:forEach items="${page.list }" var="advert">
					<tr align="middle" style="font-size:12px;">
						<td>${advert.adId }</td>
						<td>${advert.adName }</td>
						<td>${advert.adType }</td>
						<td><c:if test="${empty advert.content }">--</c:if>${advert.content}</td>
						<td><c:if test="${empty advert.adPath }">--</c:if>${advert.adPath }</td>
						<td><c:if test="${advert.status==1 }">编辑完成</c:if> <c:if
								test="${advert.status==4 }">审核不通过</c:if> <c:if
								test="${advert.status==6 }">撤除</c:if></td>
						<td>${advert.createTime}</td>
						<td><c:if test="${empty lastUpdateTime }">--</c:if>${advert.lastUpdateTime}</td>
						<td>${advert.userId }</td>
						<td><a
							href="${pageContext.request.contextPath }/ad.do?action=update&adId=${advert.adId}">修改</a>
							<a
							href="${pageContext.request.contextPath }/ad.do?action=check&adId=${advert.adId}">发送审核</a>
							<a
							href="${pageContext.request.contextPath }/ad.do?action=delete&adId=${advert.adId}">删除</a>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div align="center" style="position: absolute; bottom: 20px; left: 460px;">
			<jsp:include page="/public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/ad.do?action=showUpdate"
					name="path" />
			</jsp:include>
		</div>
	</div>
</body>
</html>