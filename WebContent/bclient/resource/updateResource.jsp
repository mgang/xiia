<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link
	href="${pageContext.request.contextPath}/bclient/css/resource/resource.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;资源&gt;修改资源</div>
	<div class="funContent">
		<form id="updateResForm" style="width: 80%;"  
			action="${pageContext.request.contextPath}/resource.do?action=updateResource"
			method="post" class="funForm">
			<table>
			
				<tr>
					<td align="right">所需积分:</td>
					<td>
						<select name="resScore"  required="required">
							<option id="res0" value="0">0</option>
							<option id="res1" value="1">1</option>
							<option id="res2" value="2">2</option>
							<option id="res5" value="5">5</option>
							<option id="res10" value="10">10</option>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td align="right">所属资源分类:</td>
					<td>
						<select name="sortId"  required="required">
							<c:forEach items="${rsList}" var="rs">
								<option id="sort_${rs.sortId }" value="${rs.sortId }">${rs.sortName}</option>
							</c:forEach>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td><br/></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><input type="hidden" name="resourceId" value="${resource.resourceId }"/>
						<input type="hidden" id="resScore" value="${resource.resScore }"/>
						<input type="hidden" id="sortId" value="${resource.sortId }"/>
						<input type="hidden" id="msg" value="${resString}" /></td>
					<td><input type="submit" value=" 确认修改 " />   
					<a href="${pageContext.request.contextPath}/resource.do?action=list">返回资源列表</a></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var _resSorce = $("#resScore").val();
		var _sortId = $("#sortId").val();
		$("#res"+_resSorce).attr("selected",true);
		$("#sort_"+_sortId).attr("selected",true);
		//反馈信息
		alertMsg("msg");
	});
</script>
