<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<link
	href="${pageContext.request.contextPath}/bclient/css/resource/resource.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;资源&gt;修改资源分类</div>
	<div class="funContent">
		<form
			action="${pageContext.request.contextPath}/resourceSort.do?action=updateResourceSort"
			id="updateSortForm" method="post">
			<table>
				<tr>
					<td>分类名称:</td>
					<td><input type="text" name="sortName" value="${resourceSort.sortName }"></td>
					<td></td>
					<td id="msg_have"></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><input type="hidden" name="sortId" value="${resourceSort.sortId }"/>
					<input type="hidden" id="msg" value="${resString }" /></td>
					<td><input type="submit" value=" 确认修改 " />　　
					<a href="${pageContext.request.contextPath}/resourceSort.do?action=list">返回资源分类列表</a></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function() {
		$("#updateSortForm").validate({
			errorClass: "inputError",
			successClass: "inputSuccess",
			rules:{
				sortName:{
					required:true,
					minlength:2,
					maxlength:6
				}
			},
			messages:{
				sortName:{
					required:"资源分类名不能为空",
					minlength:"最少2个字符",
					maxlength:"最多6个字符"
				}
			}
		});
		//弹出反馈信息
		alertMsg("msg");
	});
</script>
