<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link
	href="${pageContext.request.contextPath}/bclient/css/resource/resource.css"
	rel="stylesheet" type="text/css">
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;资源&gt;添加资源</div>
	<div class="funContent">
		<form id="addResForm" style="width: 80%;"  enctype="multipart/form-data"
			action="${pageContext.request.contextPath}/resource.do?action=addResource"
			method="post" class="funForm">
			<table>
				<tr>
					<td align="right">资源文件:</td>
					<td><input type="file" name="upFile" size="12" accept=".zip" required="required"/></td>
					<td style="font-size: 10px;color:gray;">暂时只支持压缩文件zip类型，大小在10M内。</td>
				</tr>
				<tr>
					<td align="right">所需积分:</td>
					<td>
						<select name="resScore" required="required">
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="5">5</option>
							<option value="10">10</option>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td align="right">所属资源分类:</td>
					<td>
						<select name="sortId" required="required">
							<c:forEach items="${rsList}" var="rs">
								<option value="${rs.sortId }">${rs.sortName }</option>
							</c:forEach>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td><br/></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><input type="hidden" id="msg" value="${resString}" /></td>
					<td><input type="submit" value=" 上传资源 " /></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		
		//反馈信息
		alertMsg("msg");
	});
</script>
