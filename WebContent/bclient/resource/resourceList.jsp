﻿<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link
	href="${pageContext.request.contextPath}/bclient/css/resource/resource.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css"
	type="text/css" rel="stylesheet" />
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;资源&gt;查看资源列表</div>
	<div class="funContent">
		<table
			class="table table-condensed table-bordered table-striped table-hover table12"
			cellspacing="0" cellpadding="0">
			<tr>
			<th style="width:30px;">编号</th>
			<th style="width:60px;">分类名称</th>
			<th>上传者</th>	
			<th style="width:60px;">资源名称</th>
			<th style="width:60px;">资源大小</th>
			<th style="width:240px;">对应服务器路径</th>
			<th style="width:60px;">下载次数</th>
			<th>上传时间</th>
			<th>所需积分</th>	
			<th>状态</th>	
			<th style="width:150px;">操作</th>					
		</tr>
		<c:if test="${empty page.list }">
			<tr>
				<td colspan="11">暂无资源</td>
			</tr>
		</c:if>
		<c:forEach items="${page.list }" var="r">		
			<tr>
				<td>${r.resourceId}</td>
				<td>${r.resourceSort.sortName}</td>
				<td>${r.user.userName}</td>
				<td>${r.fileName}</td>
				<td><fmt:formatNumber value="${r.fileSize div 1024}" maxFractionDigits="2"></fmt:formatNumber>kb</td>
				<td>${r.filePath}</td>
				<td>${r.downloadCount}</td>				
				<td><fmt:formatDate value="${r.addTime }" type="both"/> </td>
				<td>${r.resScore}</td>
				<td>
					<c:if test="${r.status eq 1 }">编辑完成</c:if>
					<c:if test="${r.status eq 4 }">审核不通过</c:if>
				</td>
				<td>
				<a href="${pageContext.request.contextPath}/resource.do?action=askCheck&resourceId=${r.resourceId}" onclick="return confirm('您真的要对该资源申请审核吗？')">申请审核</a>
				<a href="${pageContext.request.contextPath}/resource.do?action=toUpdateResourceUI&resourceId=${r.resourceId}"onclick="return confirm('您真的要修改该资源名称吗？')">修改</a>
				<a href="${pageContext.request.contextPath}/resource.do?action=deleteResource&resourceId=${r.resourceId}&sortId=${r.sortId}" onclick="return confirm('您真的要删除该资源吗？');">删除</a>	
				</td>
			</tr>
		</c:forEach>
		</table>
		 <input type="hidden" value="${resString }" id="msg"/>
	</div>
	<div align="center">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/resource.do?action=list"
				name="path" />
		</jsp:include>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function(){
		//弹出反馈信息
		alertMsg("msg");
	});
</script>    
