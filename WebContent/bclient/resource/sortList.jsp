<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link
	href="${pageContext.request.contextPath}/bclient/css/resource/resource.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css"
	type="text/css" rel="stylesheet" />
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">栏目管理&gt;资源&gt;查看资源分类列表</div>
	<div class="funContent">
		<table
			class="table table-condensed table-bordered table-striped table-hover table12"
			cellspacing="0" cellpadding="0">
			<tr>
				<th>分类编号</th>
				<th>资源分类名</th>
				<th>资源数目</th>
				<th>创建时间</th>
				<th>创建人</th>
				<th>操作</th>
				</tr>
				<c:if test="${empty page.list }">
					<tr>
						<td colspan="6">暂无资源</td>
					</tr>
				</c:if>
			<c:forEach items="${page.list }" var="rs">
				<tr>
					<td>${rs.sortId }</td>
					<td>${rs.sortName }</td>
					<td>${rs.resNum }</td>
					<td><fmt:formatDate value="${rs.addTime }" type="both"/> </td>
					<td>${rs.user.userName}</td>
					<td>
					<a href="${pageContext.request.contextPath}/resourceSort.do?action=toUpdateSortUI&sortId=${rs.sortId}&sortName=${rs.sortName}"onclick="return confirm('您真的要修改该资源分类名称吗？')">修改</a>
					<a href="${pageContext.request.contextPath}/resourceSort.do?action=deleteResourceSort&sortId=${rs.sortId}" onclick="return confirm('您真的要删除该资源分类吗？');">删除</a>	
					</td>
				</tr>
			</c:forEach>
		</table>
		 <input type="hidden" value="${resString }" id="msg"/>
	</div>
	<div align="center">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/resourceSort.do?action=list"
				name="path" />
		</jsp:include>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function(){
		//弹出反馈信息
		alertMsg("msg");
	});
</script>    
