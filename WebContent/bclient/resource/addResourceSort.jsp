<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<link
	href="${pageContext.request.contextPath}/bclient/css/resource/resource.css"
	rel="stylesheet" type="text/css">	
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;资源&gt;添加资源分类
	</div>
	<div class="funContent">
		<form id="addSortForm" style="width:80%;"
		    action="${pageContext.request.contextPath}/resourceSort.do?action=addResourceSort" method="post" class="funForm">
		    <table>
		    	<tr>
					<td>分类名称:</td>
					<td><input type="text"  name="sortName" class="name" ></td>
					<td id="msg_have"></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><input type="hidden" id="msg" value="${resString }"/></td>
					<td><input type="submit" value=" 确定 "/></td>
					<td></td>
				</tr>
		</table>
	</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#addSortForm").validate({
			errorClass: "inputError",
			successClass: "inputSuccess",
			rules:{
				sortName:{
					required:true,
					minlength:2,
					maxlength:6
				}
				
			},
			messages:{
				sortName:{
					required:"资源分类名不能为空",
					minlength:"最少2个字符",
					maxlength:"最多6个字符"
				}
				
			}
		});
		//反馈信息
		alertMsg("msg");
	});
</script>
