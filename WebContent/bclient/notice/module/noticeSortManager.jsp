<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/bclient/css/notice/noticeSort.css"/>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/bclient/css/user/bt_table.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<div id="notice-sort">
	<div id="edit-title"><span>公告>>公告分类列表</span></div>
	<button class="add-sort-btn" onclick="toAddNoticeSort()">新建分类</button>
	<c:if test="${not empty page.list }">
		<button class="add-sort-btn" onclick="toAddNotice()">添加公告</button>
	</c:if>
	<table class="table table-condensed table-bordered table-striped table-hover" cellspacing="0" cellpadding="0" border="1">
		<tr>
			<td>编号</td>
			<td>名称</td>
			<td>公告数量</td>
			<td>描述</td>
			<td>创建时间</td>
			<td>修改时间</td>
			<td>创建者</td>
			<td>操作</td>
		</tr>
		<c:if test="${not empty page.list }">
			<c:forEach items="${page.list }" var="noticeSort">
				<tr style="font-size:14px;">
					<td class="notice-id">${noticeSort.sortId}</td>
					<td class="notice-sort-name">
						<c:if test="${noticeSort.noticeNum != 0 }">
							<a href="${pageContext.request.contextPath }/noticeSort.do?action=showNoticeList&sortId=${noticeSort.sortId}">${noticeSort.sortName }</a>
						</c:if>
						<c:if test="${noticeSort.noticeNum == 0 }">
							<span>${noticeSort.sortName }</span>
						</c:if>
					</td>
					<td class="notice-num">${noticeSort.noticeNum }</td>
					<td class="notice-sort-note"><span>${noticeSort.note }</span></td>
					<td>${noticeSort.createTime }</td>
					<td>${noticeSort.lastUpdateTime }</td>
					<td>${noticeSort.userId }</td>
					<td class="notice-operation">
						<a href="${pageContext.request.contextPath }/noticeSort.do?action=toUpdateNoticeSortUI&sortId=${noticeSort.sortId}">编辑</a>
						<a class="del" onclick="return delConfirm()" href="${pageContext.request.contextPath }/noticeSort.do?action=deleteNoticeSort&sortId=${noticeSort.sortId}">删除</a>
					</td>
				</tr>
			</c:forEach>
		</c:if>	
	</table>
	<c:if test="${not empty page.list }">
		<div align="center" style="margin:10px auto;">
			<jsp:include page="../../../public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/noticeSort.do?action=toNoticeSortManagerUI"
					name="path" />
			</jsp:include>
		</div>
	</c:if>
	<c:if test="${empty page.list }"><warn>暂无公告分类！</warn></c:if>
</div>
<script type="text/javascript">
	$(document).ready(function(){
			var res = "${resString}";
			if ( res != ""){
				alert(res);
			}
		});
	function toAddNoticeSort(){
		var path = "${pageContext.request.contextPath}";
		window.location.href = path + "/noticeSort.do?action=toAddNoticeSortUI";
	}
	function toAddNotice(){
		window.location.href = "${pageContext.request.contextPath}/noticeSort.do?action=toAddNoticeUI";
	}
	function delConfirm(){
		var msg = "该操作会删除此分类下的所有公告，您确定要删除吗？";
		if (true == confirm(msg)){
			return true;
		}else{
			return false;
		}
	}
</script>