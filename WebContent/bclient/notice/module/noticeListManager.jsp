<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/bclient/css/notice/noticeList.css"/>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/bclient/css/user/bt_table.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<div id="notice-list">
	<div id="edit-title"><span>公告>>公告列表</span></div>
	<div id="edit-unit">		
		<form id="fm" action="${pageContext.request.contextPath }/notice.do?action=" method="post">
			<table class="table table-condensed table-bordered table-striped table-hover" cellspacing="0" cellpadding="0" border="1">
				<tr>
					<td><input type="checkbox" id="check-control"/></td>
					<td>编号</td>
					<td>标题</td>
					<td class="time-width">加入时间</td>
					<td class="time-width">修改时间</td>
					<td>状态</td>
					<td>分类</td>
					<td>创建者</td>
					<td>操作</td>
				</tr>
				<c:if test="${not empty page.list }">
					<c:forEach items="${page.list }" var="notice">
						<tr style="font-size:14px;">
							<td><input type="checkbox" class="check" name="noticeId" value="${notice.noticeId }"/></td>
							<td>${notice.noticeId }</td>
							<td class="notice-title"><span>${notice.title }</span></td>
							<td><time>${notice.addTime }</time></td>
							<td><time>${notice.lastUpdateTime }</time></td>
							<td class="notice-status">
								<c:if test="${notice.status == 1 }"><finish>编辑完成</finish></c:if>
								<c:if test="${notice.status == 4 }"><unPass>不通过</unPass></c:if>
								<c:if test="${notice.status == 6 }"><revoke>已撤销</revoke></c:if>
							</td>
							<td><a href="${pageContext.request.contextPath }/noticeSort.do?action=showNoticeList&sortId=${notice.sortId }">${notice.sortId }</a></td>
							<td>${notice.userId }</td>
							<td class="notice-operation">
								<a class="edit" href="${pageContext.request.contextPath }/notice.do?action=toUpdateNoticeUI&noticeId=${notice.noticeId}">修改</a>
								<a onclick="return Sendconfirm()" href="${pageContext.request.contextPath }/notice.do?action=sendToCheck&noticeId=${notice.noticeId}">发送审核</a>
								<a class="del" onclick="return Delconfirm()" href="${pageContext.request.contextPath }/notice.do?action=deleteNotice&noticeId=${notice.noticeId}">删除</a>
							</td>
						</tr>
					</c:forEach>
				</c:if>				
			</table>
			<c:if test="${not empty page.list }">
				<div id="edit-notice-oper">
					<div id="check-div" ><input type="checkbox" onclick="reverseCheck()"/><span>反选</span></div>
					<button onclick="return sendAll()">发送审核</button>
					<button onclick="return deleteAll()">删&nbsp;&nbsp;除</button>
				</div>
			</c:if>
		</form>
		<c:if test="${not empty page.list }">
			<div align="center">
				<jsp:include page="../../../public/pager.jsp">
					<jsp:param
						value="${pageContext.request.contextPath}/notice.do?action=toNoticeListManagerUI"
						name="path" />
				</jsp:include>
			</div>
		</c:if>
		<c:if test="${empty page.list }">暂无可编辑的公告！</c:if>
	</div>
</div>
<script type="text/javascript">
function checkAll(){
	$(".check").each(function(){
		this.checked = true;
	});
}
function unCheckAll(){
	$(".check").each(function(){
		this.checked = false;
	});
}
function reverseCheck(){//反选
	if (document.getElementById("check-control").checked == true){
		document.getElementById("check-control").checked = false;
	}else{
		document.getElementById("check-control").checked = true;
	}
	$(".check").each(function(){
		if (this.checked == false){
			this.checked = true;
		}else{
			this.checked = false;
		}
	});
}
var checkFlag = 0;
$(document).ready(function(){
	var res = "${resString}";
	if ( res != ""){
		alert(res);
	}
	$("#check-control").click(function(){
		if (checkFlag == 0){
			 checkAll();
			 checkFlag = 1;
		}else{
			 unCheckAll();
			 checkFlag = 0;
		}
	});
});

function sendAll(){
	var action = $("#fm").attr("action");
	$("#fm").attr("action", action + "sendAllNotice");
	if (isSelect()){
		if (true == SendAllconfirm()){
			$("#fm").submit();
		}else{
			$("#fm").attr("action", action); //清空追加的action字符串
			return false;
		}
		
	}else{
		alert("至少勾选一项！");
		$("#fm").attr("action", action); //清空追加的action字符串
		return false;
	}
}
function deleteAll(){
	var action = $("#fm").attr("action");
	$("#fm").attr("action", action + "deleteAllNotice");
	if (isSelect()){
		if (DelAllconfirm()){
			$("#fm").submit();
		}else{
			$("#fm").attr("action", action); //清空追加的action字符串
			return false;
		}
		 //$("#fm").submit();  
	}else{
		alert("至少勾选一项！");
		$("#fm").attr("action", action); //清空追加的action字符串
		return false;
	}
}
function isSelect(){
	var flag = false; //判断是否至少有一个复选框被选中
	$(".check").each(function(){
		if (this.checked == true){
			flag = true;
		}
	});
	return flag;
}
function Delconfirm(){
	var msg = "确定要删除该公告吗?";
	if (true == confirm(msg)){
		return true;
	}else{
		return false;
	}
}
function DelAllconfirm(){
	var msg = "确定要删除所有被选中的公告吗?";
	if (true == confirm(msg)){
		return true;
	}else{
		return false;
	}
}
function Sendconfirm(){
	var msg = "确定要将该公告发送审核吗?";
	if (true == confirm(msg)){
		return true;
	}else{
		return false;
	}
}
function SendAllconfirm(){
	var msg = "确定要将该所有被选中的公告发送审核吗?";
	if (true == confirm(msg)){
		return true;
	}else{
		return false;
	}
}
</script>