<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<!DOCTYPE html>
<style>
#noticeSort{
	border:1px solid gray;
	border-radius:3px;
	height:450px;	
}
#edit-title{
	background:url(bclient/image/height30bg.png) repeat-x;
	display:block;
	width:100%;
	height:30px;
	border-top-right-radius:3px;
	border-top-left-radius:3px;
}
#edit-title span{
	margin-left:10px;
	height:30px;
	line-height:30px;
}
#add-edit-notice{
	color:#2995D4;
	margin-top:20px;
	border:0px solid blue;
}
#add-edit-notice form{
	position:relative;
	padding:3px;
	width:456px;
	height:175px;
	border:2px solid gray;
	border-radius:5px;
}
#title-notice{
	text-align:left;
}
#title-notice input{
	width:120px;
}
#content-notice{
	margin-top:10px;
	margin-bottom:20px;
}
#content-notice textarea{
	width:450px;
	height:80px;
}
#sub-btn{
	position:absolute;
	display:block;
	color:black;
	background-color:#2995D4;
	border:1px solid #2995D4;
	border-radius:3px;
	bottom:3px;
	right:3px;
}
.inputError{
	color:red;
	font-size:12px;
}
</style>
<div id="noticeSort">
	<jsp:include page="../../public/importValidate.jsp"></jsp:include>
	<div id="edit-title"><span>公告>>新建公告分类</span></div>
	<div id="add-edit-notice">
		<form id="fm" action="${pageContext.request.contextPath }/noticeSort.do?action=createNoticeSort" method="post">
			<div id="title-notice">分类名称：<input type="text" id="sortName" name="sortName"/></div>
			<div id="content-notice">描述信息：<textarea name="note"></textarea></div> 
			<input id="sub-btn" type="submit" onclick="return checkSortName()" value="保存"/>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var res = "${resString}";
		if ( res != ""){
			alert(res);
		}
		$("#fm").validate({
			errorClass: "inputError",
			success: "inputSuccess",
			rules:{
				sortName:{
					required:true,
					minlength:2,
					maxlength:12
					/* remote:{
						url:function(){
							var url = getRootPath() + "/ajaxServlet";
							alert(url);
							return url;
						},
						type:"post",
						dataType:"text",
						data:{
							action:"isExistSortName",
							sortName:function(){
								return $("#sortName").val();
							}
						}
					} */
				},
				note:{
					required:true,
					minlength:5,
					maxlength:45
				}
				
			},
			messages:{
				sortName:{
					required:"分类名称不能为空",
					minlength:"分类名称长度不能少于2个字符",
					maxlength:"分类名称长度不能多于12个字符"
					/* remote:jQuery.format("不能使用这个名称") */
				},
				note:{
					required:"描述信息不能为空",
					minlength:"描述信息长度不能少于5个字符",
					maxlength:"描述信息长度不能多余45个字符"
				}
			}
		});
	});
	function checkSortName(){
		$.ajax({
			url:getRootPath() + "/ajaxServlet",
			data:{"action":"isExistSortName", "sortName":$("#sortName").val()},
			type:"post",
			dataType:"json",
			success:function(res){
				var msg = res.msg;
				if (msg == "T"){
					alert("该分类名称已存在！");
					return false;
				}
			}
		});
	}
</script>