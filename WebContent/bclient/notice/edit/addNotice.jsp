<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.fckeditor.net" prefix="FCK"%> 
<jsp:include page="../../public/importValidate.jsp"></jsp:include>
<!DOCTYPE html>
<script type="text/javascript" language="javascript" src="${pageContext.request.contextPath }/fckeditor/fckeditor.js"></script>
<style>
#addNotice{
	border:1px solid gray;
	border-radius:3px;	
}
#edit-title{
	background:url(bclient/image/height30bg.png) repeat-x;
	display:block;
	width:100%;
	height:30px;
	border-top-right-radius:3px;
	border-top-left-radius:3px;
}
#edit-title span{
	margin-left:10px;
	height:30px;
	line-height:30px;
}
#add-edit-notice{
	color:#2995D4;
	margin-top:20px;
	margin-left:10px;
	border:0px solid pink;
}
#add-edit-notice form{
	postition:relative;
	padding:3px;
	width:756px;
	height:420px;
	border:2px solid gray;
	border-radius:5px;
}
#title-notice{
	text-align:left;
}
#title-notice input{
	width:280px;
}
#sort-name{
	margin-top:10px;
}
#sort-name select{
	width:120px;
	border-radius:1px;
}
#sort-name select option{
	margin-left:0px;
}
#content-notice{
	margin-top:10px;
}
#content-notice textarea{
	width:755px;
	height:280px;
}
#sub-btn{
	clear:both;
	position:absolute;
	display:block;
	color:black;
	background-color:#2995D4;
	border:1px solid #2995D4;
	border-radius:3px;
	bottom:56px;
	right:386px;
}
.inputError{
	color:red;
	font-size:12px;
}
</style>
<div id="addNotice">
	<jsp:include page="../../public/importValidate.jsp"></jsp:include>
	<div id="edit-title"><span>公告>>添加公告</span></div>
	<div id="add-edit-notice">
		<form id="fm" action="${pageContext.request.contextPath }/noticeSort.do?action=createNotice" method="post">
			<div id="title-notice">公告标题：<input type="text" id="title" name="title"/></div>
			<div id="sort-name">
				所属分类：
				<select name="sortId">
					<option selected="selected" value="">&nbsp;所属分类</option>
					<c:forEach items="${noticeSorts }" var="noticeSort">
						<option value="${noticeSort.sortId }">&nbsp;&nbsp;${noticeSort.sortName }</option>
					</c:forEach>
				</select>
				</div>
			<div id="content-notice">公告类容：<textarea id="content" name="content"></textarea></div> 
			<input id="sub-btn" type="submit" onclick="return checkTitle()" value="保存"/>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		/* var fckeditor=new FCKeditor('content');
    	fckeditor.BasePath= getRootPath() + "/fckeditor/";//相对路径，相对于当前页面
    	fckeditor.ToolbarSet="smile";
    	fckeditor.Width="100%";
    	fckeditor.Height="120";
    	fckeditor.ReplaceTextarea();
    	document.all.content.value=""; */
    	
		var res = "${resString}";
		if ( res != ""){
			alert(res);
		}
		$("#fm").validate({
			errorClass: "inputError",
			success: "inputSuccess",
			rules:{
				title:{
					required:true,
					minlength:8,
					maxlength:32
				},
				sortId:{
					required:true
				},
				content:{
					required:true,
					minlength:30,
					maxlength:60000,
				}
				
			},
			messages:{
				title:{
					required:"公告标题不能为空",
					minlength:"标题长度不能少于8个字符",
					maxlength:"标题长度不能多于32个字符"
				},
				sortId:{
					required:"请选择一个公告分类"
				},
				content:{
					required:"公告内容不能为空",
					minlength:"公告内容长度不能少于30个字符",
					maxlength:"公告内容长度不能多于60000个字符",
				}
			}
		});
	});
	function checkTitle(){
		$.ajax({
			url:getRootPath() + "/ajaxServlet",
			data:{"action":"isExistTitle", "title":$("#title").val()},
			type:"post",
			dataType:"json",
			success:function(response){
				var msg = response.msg;
				if (msg == "T"){
					alert("该标题已存在！");
					return false;
				}
			}
		});
		/* var content = document.getElementsByTagName("textarea")[0].value;
		content = "<pre>" + content + "</pre>"; */
	}
</script>