<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mg" uri="http://room.mgang.com/checkFunction" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="import.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　审核管理&gt;审核分支机构
	</div>
	<div class="funContent">
		    <table class="table table-condensed table-bordered table-striped table-hover table12" cellspacing="0" cellpadding="0">
		    	<tr>
		    		<th style="">机构编号</th>
		    		<th style="">机构名称</th>
		    		<th style="">机构链接</th>
		    		<th>加入时间</th>
		    		<th>最后修改时间</th>
		    		<th>状态</th>
		    		<th>是否审核</th>
		    		<th colspan="2" style="width: 200px;">操作</th>
		    	</tr>
		    	<c:if test="${empty page.list }">
		    		<tr>
		    			<td colspan="9">无审核任务</td>
		    		</tr>
		    	</c:if>
		    	<c:forEach items="${page.list }" var="c">
		    		<tr>
			    		<td>${c.obj.orgId}</td>
			    		<td>${c.obj.orgName }</td>
			    		<td>${c.obj.link }</td>
			    		<td><fmt:formatDate value="${c.obj.addTime }" type="both"/></td>
			    		<td><fmt:formatDate value="${c.obj.lastUpdateTime }" type="both"/> </td>
			    		<td style="color:green;">
			    			<c:if test="${c.obj.status eq 2}">待审核</c:if>
			    			<c:if test="${c.obj.status eq 3}">审核通过</c:if>
			    			<c:if test="${c.obj.status eq 4}">审核不通过</c:if>
			    			<c:if test="${c.obj.status eq 5}">已发布</c:if>
			    			<c:if test="${c.obj.status eq 6}">已撤销</c:if>
			    		</td>
			    		<td style="color: red;">
			    			<c:if test="${c.flag eq 1 }">已审核</c:if>
			    			<c:if test="${c.flag eq 0 }">未审核</c:if>
			    		</td>
			    		<td>
		    				<a class="btnPass" status="${c.obj.status }"
		    				 	href="${pageContext.request.contextPath}/check.do?action=pass&checkedId=${c.checkedId}&type=org" onclick="return confirm('您真的确定审核通过该分支机构吗？')">审核通过</a>
		    			　	<a class="btnNopass" status="${c.obj.status }"
		    					checkedId="${c.checkedId }" href="javascript:void(0);" >审核不通过</a>
		    				<a class="btnPublish" status="${c.obj.status }"
		    					 href="${pageContext.request.contextPath}/publish.do?action=publish&publishedId=${c.checkedId}&type=org" onclick="return confirm('您真的确定发布该分支机构吗？')">发布</a>
		    				<a class="btnRevoke" status="${c.obj.status }"
		    					 href="${pageContext.request.contextPath}/revoke.do?action=revoke&revokedId=${c.checkedId}&type=org" onclick="return confirm('您真的确定撤销该分支机构吗？')">撤销</a>
			    		</td>
			    	</tr>
		    	</c:forEach>
		    	
		    </table>
		    <input type="hidden" value="${resString }" id="msg"/>
			
	</div>
	<div align="center">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/check.do?action=list&type=org"
				name="path" />
		</jsp:include>
	</div>
</div>
<div style="display: none;" id="nopassDialog">
	<form style="padding:10px;" method="post"
		 action="${pageContext.request.contextPath}/check.do?action=nopass&type=org">
			审核不通过原因：<br/>
			<input type="hidden" name="checkedId" id="checkedId"/>
			<textarea style="width:380px;height: 100px;" name="msg" required="required"></textarea><br/>
			<input type="submit" value="提交">	
	</form>
</div>
