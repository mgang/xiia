<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mg" uri="http://room.mgang.com/checkFunction" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="import.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　相关信息&gt;查看发布信息
	</div>
	<div class="funContent">
		    <table class="table table-condensed table-bordered table-striped table-hover table12" cellspacing="0" cellpadding="0">
		    	<tr>
		    		<th style="">编号</th>
		    		<th style="">类型</th>
		    		<th style="width:500px;">发布的实体</th>
		    		<th>最后发布时间</th>
		    		<th>状态</th>
		    		<th>是否发布</th>
		    		<th>发布者</th>
		    	</tr>
		    	<c:if test="${empty page.list }">
		    		<tr>
		    			<td colspan="8">无发布信息</td>
		    		</tr>
		    	</c:if>
		    	<c:forEach items="${page.list }" var="p">
		    		<tr>
			    		<td>${p.publishId}</td>
			    		<td>${p.type }</td>
			    		<td align="left">
			    			<c:if test="${p.type eq 'ad' }">
			    				广告标题:${p.obj.adName };<br/>
			    				广告类型:${p.obj.adType };<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.createTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'menu' }">
			    				栏目名:${p.obj.menuName };<br/>
			    				栏目级别:${p.obj.menuRank };<br/>
			    				栏目链接:${p.obj.link }<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.createTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'news' }">
			    				新闻标题:${p.obj.title };<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.addTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'notice' }">
			    				公告标题:${p.obj.title };<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.addTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'org' }">
			    				分支机构名称:${p.obj.orgName };<br/>
			    				链接:${p.obj.link };<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.addTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'project' }">
			    				项目名称:${p.obj.projectName };<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.addTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'resource' }">
			    				资源名称:${p.obj.fileName };<br/>
			    				资源大小:${p.obj.fileSize };<br/>
			    				所需积分:${p.obj.resScore };<br/>
			    				上传时间:<fmt:formatDate value="${p.obj.addTime }" type="both"/><br/>
			    			</c:if>
			    			<c:if test="${p.type eq 'rule' }">
			    				政策法规名称:${p.obj.title };<br/>
			    				链接:${p.obj.link };<br/>
			    				创建时间:<fmt:formatDate value="${p.obj.addTime }" type="both"/><br/>
			    			</c:if>
			    		</td>
			    		<td><c:if test="${empty p.lastPublishTime }">--</c:if>
			    			<fmt:formatDate value="${p.lastPublishTime }" type="both"/>
			    		</td>
			    		<td style="color:green;">
			    			<c:if test="${p.obj.status eq 2}">待审核</c:if>
			    			<c:if test="${p.obj.status eq 3}">审核通过</c:if>
			    			<c:if test="${p.obj.status eq 4}">审核不通过</c:if>
			    			<c:if test="${p.obj.status eq 5}">已发布</c:if>
			    			<c:if test="${p.obj.status eq 6}">已撤销</c:if>
			    		</td>
			    		<td style="color: red;">
			    			<c:if test="${p.flag eq 1 }">已发布</c:if>
			    			<c:if test="${p.flag eq 0 }">未发布</c:if>
			    		</td>
			    		<td>
			    		<c:if test="${empty p.user }">--</c:if>
			    		${p.user.userName }</td>
			    	</tr>
		    	</c:forEach>
		    	
		    </table>
		    <input type="hidden" value="${resString }" id="msg"/>
			
	</div>
	<div align="center">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/publish.do?action=listAll"
				name="path" />
		</jsp:include>
	</div>
</div>
