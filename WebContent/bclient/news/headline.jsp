<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<meta charset="utf-8">
<link
	href="${pageContext.request.contextPath}/bclient/css/news/newsSortList.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bclient/js/news/newsSortList.js"></script>
<title>新闻分类列表</title>

<div id="listBody">
	<div class="funTitle"><b>当前位置>><a href="#">栏目管理</a>>><a href="#">新闻</a>>>新闻分类表
	<span id="create_sort"><a href="${pageContext.request.contextPath}/bclient/news/newsSort.jsp">新建分类</a></span></b>
	</div>
	<form onsubmit="getAllNewsSortId()"
	 action="${pageContext.request.contextPath}/newsSort.do?action=deleteNewsSort" method="post" id="newsSortForm">
		<input type="hidden" id="arr" value="" name="arr"/>
		<table id="sortListTable"  cellpadding="0" cellspacing="0" class="table table-condensed table-bordered table-striped table-hover table12">
			<tr>
			
				<th>新闻分类编号</th>
				<th>新闻分类名</th>
				<th>新闻数量</th>
				<th>创建时间</th>
				<th>最后添加时间</th>
				<th>创建人</th>
				<th>新闻分类描述</th>
				<th>操作</th>
				<th>删除</th>
			</tr>
			<%
				int a = 0;
			%>	
			<c:forEach items="${newsSortList }" var="newsSort">
				<tr>
					<%
						a = a + 1;
					%>
					
					<td><%=a%></td>
					<td><a href="${pageContext.request.contextPath}/news.do?action=newsList&newsSortId=${newsSort.sortId }">${newsSort.sortName }</a></td>
					<td>${newsSort.newsNum }</td>
					<td>${newsSort.createTime }</td>
					<td>${newsSort.updateTime }</td>
					<td>${newsSort.user.userName}</td>
					<td>${newsSort.note}</td>
					<td><a href="${pageContext.request.contextPath}/newsSort.do?action=showNewsSort&sortId=${newsSort.sortId}">修改</a></td>
					<td><input type="checkbox" class="allSortId" value="${newsSort.sortId }">
					
				</tr>
			</c:forEach>
			
		</table>
		<div class="deleteSort">
			<input type="button" onclick="show_confi()" value="删除" />
		</div>
	</form>
</div>
