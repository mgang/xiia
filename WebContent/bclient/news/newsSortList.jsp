<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<meta charset="utf-8">
<link
	href="${pageContext.request.contextPath}/bclient/css/news/newsSortList.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/bclient/js/news/newsSortList.js"></script>
<title>新闻分类列表</title>

<div id="listBody">
	<div class="funTitle"><b>当前位置>>栏目管理>>新闻>>新闻分类表
	<span id="create_sort"><a href="${pageContext.request.contextPath}/bclient/news/newsSort.jsp">新建分类</a></span></b>
	</div>
	<form
	 action="${pageContext.request.contextPath}/newsSort.do?action=deleteNewsSort" method="post" id="newsSortForm">
		<input type="hidden" id="arr" value="" name="arr"/>
		<table id="sortListTable"  cellpadding="0" cellspacing="0" border="1" class="table table-condensed table-bordered table-striped table-hover table12">
			<tr>
			
				<th>新闻分类编号</th>
				<th>新闻分类名</th>
				<th>新闻数量</th>
				<th>创建时间</th>
				<th>最后添加时间</th>
				<th>创建人</th>
				<th>新闻分类描述</th>
				<th colspan="2">操作</th>
				<th>删除</th>
			</tr>
			<%
				int a = 0;
			%>	
			<c:if test="${empty page.list }">
				<td colspan="10"> 暂时无数据</td>
			</c:if>
			<c:forEach items="${page.list }" var="newsSort">
				<tr>
					<%
						a = a + 1;
					%>
					
					<td><%=a%></td>
					<td>${newsSort.sortName }</td>
					<td>${newsSort.newsNum }</td>
					<td><fmt:formatDate value="${newsSort.createTime  }" type="both"/></td>
					<td><fmt:formatDate value="${newsSort.updateTime   }" type="both"/></td>
					<td>${newsSort.user.userName}</td>
					<td>${newsSort.note}</td>
					<td><a href="${pageContext.request.contextPath}/newsSort.do?action=showNewsSort&sortId=${newsSort.sortId}">修改</a></td>
					<td><a href="${pageContext.request.contextPath}/news.do?action=newsList&newsSortId=${newsSort.sortId }">查看</a></td>
					<td><input type="checkbox" class="allSortId" value="${newsSort.sortId }"/></td>
					
				</tr>
			</c:forEach>
		</table>
		<div id="checkboxFun">
			<input type="button" onclick="allSelect()" value="全选">
			<input type="button" onclick="reverseSelect()" value="反选">
			<input type="button" onclick="cancelSelect()" value="取消">
		</div>
		<div class="deleteSort">
			<input type="button" onclick="show_confi()" value="删除" />
		</div>
	</form>
	<div align="center">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/newsSort.do?action=newsSortListTrue"
				name="path" />
		</jsp:include>
		</div>
</div>
