<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bclient/css/news/news.css"/>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bclient/css/news/updateNews.css"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/bclient/js/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/bclient/js/news/news.js" type="text/javascript"></script>
<jsp:include page="../../public/fckeditor.jsp"></jsp:include>
<div id="add_news_div" class="fun">
	<div class="funTitle">当前位置>>审核发布>>新闻>>查看新闻
		<b><span id="create_sort"><a href="${pageContext.request.contextPath}/check.do?action=list&type=news">返回</a></span></b>
	</div>
	<div id="add_body">
			<center>
				<h3>
					<b>查看新闻</b>
				</h3>
			</center>
			<div class="form-group">
				<span>新闻标题:</span><br>
				<input class="form-control" placeholder="标题"
					name="newsTitle" value="${news.title }" id="newsTitle" required/>
			</div>
			<input type="hidden" id="title_check"/><br>
			<div class="form-group">
				<textarea class="form-control" name="content" rows="10"></textarea>
				<input type="hidden" id="hidden_content" value="${ news.content}">
			</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var fckeditor=new FCKeditor('content');
    	fckeditor.BasePath= getRootPath() + "/fckeditor/";//相对路径，相对于当前页面
    	fckeditor.ToolbarSet="mgTool";
    	fckeditor.Width="895";
    	fckeditor.Height="400";
    	fckeditor.ReplaceTextarea();
    	document.all.content.value=$("#hidden_content").val();
    	
    	var _sortId = $("#sortId").val();
    	$("#sort_"+_sortId).attr("selected",true);
	})
</script>
