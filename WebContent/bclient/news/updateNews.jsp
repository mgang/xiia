<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bclient/css/news/news.css"/>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bclient/css/news/updateNews.css"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/bclient/js/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/bclient/js/news/news.js" type="text/javascript"></script>
<jsp:include page="../../public/fckeditor.jsp"></jsp:include>
<div id="add_news_div" class="fun">
	<div class="funTitle">当前位置>>栏目管理>>新闻>><a href="${pageContext.request.contextPath}/newsSort.do?action=newsSortListTrue">新闻分类</a>>>查看编辑新闻
		<b><span id="create_sort"><a href="${pageContext.request.contextPath}/news.do?action=newsList&newsSortId=${sort.sortId }">返回</a></span></b>
	</div>
	<div id="add_body">
		<form  id="write_news_form" onsubmit=" return updateF()"
			action="${pageContext.request.contextPath}/news.do?action=updateNews&newsId=${news.newsId}"
			method="post">
			<center>
				<h3>
					<b>查看编辑</b>
				</h3>
			</center>
			<div class="form-group">
				<span>新闻标题:</span><br>
				<input class="form-control" placeholder="标题"
					name="newsTitle" value="${news.title }" id="newsTitle" required/>
			</div>
			<input type="hidden" id="title_check"/><br>
			<div class="form-group">
				<textarea class="form-control" name="content" rows="10"></textarea>
				<input type="hidden" id="hidden_content" value="${ news.content}">
			</div>
			<div class="form-group" id="news_select">
				<input type="hidden" id="sortId" value="${sort.sortId }"/>
				<span>所属新闻分类:</span> 
					<select name="newsSortId" id="select1" required>
						<c:forEach items="${newsSortList }" var="newsSort">
							<option id="sort_${newsSort.sortId }"  value="${newsSort.sortId }">${newsSort.sortName }</option>
						</c:forEach>
					</select>
				<input type="submit" class="btn btn-primary btn-sm" value="保存" id="news_submit"/> 
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var fckeditor=new FCKeditor('content');
    	fckeditor.BasePath= getRootPath() + "/fckeditor/";//相对路径，相对于当前页面
    	fckeditor.ToolbarSet="mgTool";
    	fckeditor.Width="895";
    	fckeditor.Height="400";
    	fckeditor.ReplaceTextarea();
    	document.all.content.value=$("#hidden_content").val();
    	
    	var _sortId = $("#sortId").val();
    	$("#sort_"+_sortId).attr("selected",true);
	})
</script>
