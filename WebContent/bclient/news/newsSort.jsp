<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/bclient/js/jquery.validate.min.js"></script>
<link href="${pageContext.request.contextPath}/bclient/css/news/newsSort.css" rel="stylesheet" type="text/css">

<script src="${pageContext.request.contextPath}/bclient/js/news/newsSort.js" type="text/javascript"></script>
<title>新闻分类</title>
</head>
<body>
	<div class="fun">
		<div class="funTitle">当前位置：>>栏目管理>>新闻>><a href="${pageContext.request.contextPath}/newsSort.do?action=newsSortListTrue">新闻分类</a>>>新建新闻分类</div>
		<div class="blank"></div>
		<div>
			<div class="main_panel">新建新闻分类</div>
			<div id="sort_main">
				<form  action="${pageContext.request.contextPath}/newsSort.do?action=createNewsSort" id="create_news_sort" method="post" onsubmit="return goSubmit();">
					<span style="color:black;">新闻分类名：</span><br>
					<input type="text" name="sortName" class="sort_title" onkeyup="javascript:checkSortName(this);" id="sortTitle" required><br>
					 <span id="title_have"></span><br>
					<span id="label_sort" style="color:black;">新闻分类描述：</span><textarea rows="5" cols="32" name="sortNote" class="sort_note" id="sortNote" required></textarea><br>
					<div><input type="submit" value="确定" class="sort_submit"></div>
				</form>
			</div>
			<div class="main_panel"></div>
		</div>
		<div class="blank"></div>
	</div>
</body>
</html>