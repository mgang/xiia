<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta charset="utf-8">
<link
	href="${pageContext.request.contextPath}/bclient/css/news/newsSortList.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>

<link
	href="${pageContext.request.contextPath}/bclient/css/news/newsList.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/thirdParty/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/bclient/js/news/allNewsList.js" type="text/javascript">
</script>
<div id="listBody">
	<div class="funTitle"><b>当前位置>>栏目管理>>新闻>>所有新闻
		<span id="create_news"><a href="${pageContext.request.contextPath}/newsSort.do?action=newsSortListFalse&sortId=${sort.sortId}" target="main">添加新闻</a></span></b>
	</div>
	<form 
		action="#" method="post" id="newsList" >
		<input type="hidden" id="arr" value="" name="arr"/>
		<table id="sortListTable" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-bordered table-striped table-hover table12">
			<tr>
				<th>新闻编号</th>
				<th>新闻标题</th>
				<th>所属新闻分类</th>
				<th>编辑时间</th>
				<th>最后修改时间</th>
				<th>审核状态</th>
				<th>编辑人</th>
				<th colspan="3">操作</th>
				<th>设置</th>
				<th>选择</th>
				
			</tr>
			<%
				int a = 0;
			%>	
			<c:if test="${empty page.list }">
				<td colspan="11"> 暂时无数据</td>
			</c:if>
			<c:forEach items="${page.list }" var="news">
				<tr>
					<%
						a = a + 1;
					%>
					<td>
						<%=a%>
						<c:if test="${news.headlineFlag eq 1 }">
							（头条）
						</c:if>
					</td>
					<td>${news.title }</td>
					<td>${news.newsSort.sortName }</td>
					<td><fmt:formatDate value="${news.addTime }" type="both"/></td>
					<td><fmt:formatDate value="${news.lastUpdateTime }" type="both"/></td>
					<c:if test="${news.status eq 1 }" >
						<td>完成</td>
					</c:if>
					<c:if test="${news.status eq 2 }" >
						<td>待审核</td>
					</c:if>
					<c:if test="${news.status eq 3 }" >
						<td>审核通过</td>
					</c:if>
					<c:if test="${news.status eq 4 }" >
						<td>审核不通过</td>
					</c:if>
					<c:if test="${news.status eq 5 }" >
						<td>已发布</td>
					</c:if>
					<c:if test="${news.status eq 6 }" >
						<td>已撤销</td>
					</c:if>
					<td>${news.user.userName}</td>
					<td><a href="${pageContext.request.contextPath}/news.do?action=showNews&newsId=${news.newsId}">查看编辑</a></td>
					<c:if test="${news.headlineFlag eq 0 }">	
						<td><a href="javascript:void(0);" class="btnHeadline" newsId="${news.newsId }">头条</a>
						<input type="hidden" class="hidden_action" value="${sort.sortId }">
						</td>
						<td><a href="javascript:void(0);" class="btnHead" newsId="${news.newsId }">照片</a></td>
					</c:if>
					<c:if test="${news.headlineFlag eq 1 }">
							<td>已设置</td>
							<td>已上传</td>
						</c:if>
					<c:if test="${news.topFlag eq 0 }">
						<td><a href = "${pageContext.request.contextPath}/news.do?action=topNews&newsId=${news.newsId}">置顶</a></td>
					</c:if>
					<c:if test="${news.topFlag eq 1}">
						<td><a href = "${pageContext.request.contextPath}/news.do?action=deleteTopNews&newsId=${news.newsId}">取消置顶</a></td>
					</c:if>
					<td><input type="checkbox" class="allNewsId" value="${news.newsId }">
				</tr>
			</c:forEach>
		</table>
		<div id="checkboxFun">
			<input type="button" onclick="allSelect()" value="全选">
			<input type="button" onclick="reverseSelect()" value="反选">
			<input type="button" onclick="cancelSelect()" value="取消">
		</div>
		<div class="deleteNews">
			<input type="button" value="删除" onclick="deleteNe()" />
			<input type="hidden" value="${pageContext.request.contextPath}/news.do?action=deleteNews" id="deleteNews">
			<input type="button" value="发送审核" onclick="sen()" />
			<input type="hidden" value="${pageContext.request.contextPath}/news.do?action=updateNewsStatus" id="sendCheck">
		</div>
	</form>
	<div align="center" id="news_page">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/news.do?action=newsList&newsSortId=${sort.sortId }"
				name="path" />
		</jsp:include>
	</div>
</div>
<div id="set_headline" style="display: none;">
	<form onsubmit="return goSubmit();" action = "" id="form_headline" method="post">
		头条标题：<br>
		<input type="text" id="headline_title" name="headline_title" onkeyup="javascript:checkHeadName(this);" required><br>
		<span id="titleLong"></span><br>
		头条描述：<br>
		<textarea rows="2" cols="15" id="headline_note" name="headline_note"  onkeyup="javascript:checkHeadNote(this);" required></textarea><br>
		<span id="noteLong"></span><br>
		<div id="submit_form">
			<input type="submit" value="提交">
			<input type="reset" value="重置">
		</div>
	</form>
	
</div>
<div id="set_head" style="display: none;">
	<form onsubmit="return headlineUrl()"
	action = "" method="post" id="form_head" enctype="multipart/form-data" >
		<input type="hidden" id="hidden_photo" value="${pageContext.request.contextPath}/">
		头条照片：<input type="file" id="head_url" name="head_url"><br>
		<input type="submit"  value="上传照片">
	</form>
</div>

