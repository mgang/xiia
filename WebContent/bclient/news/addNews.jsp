<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bclient/css/news/news.css"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/bclient/js/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/bclient/js/news/news.js" type="text/javascript"></script>
<jsp:include page="../../public/fckeditor.jsp"></jsp:include>
</head>
<body>
<div id="add_news_div" class="fun">
	<div  class="funTitle">当前位置>>栏目管理>>新闻>><a href="${pageContext.request.contextPath}/newsSort.do?action=newsSortListTrue">新闻分类</a>>>编辑新闻</div>
	<div id="add_body">
		<form  id="write_news_form" onsubmit=" return checkForm();"
			action="${pageContext.request.contextPath}/news.do?action=addNews"
			method="post">
			<center>
				<c:if test="${empty newsSortList}">
					<h2 style="color:red"><b>无新闻分类，请新建分类！</b></h2>
					<input type="hidden" class="emptySort" value="0">
				</c:if>
				<c:if test="${not empty newsSortList}">
					<h3>
						<b>编辑新闻</b>
						<input type="hidden" class="emptySort" value="1">
					</h3>
				</c:if>
			</center>
			<div class="form-group">
				<span>新闻标题:</span><br>
				<input type="text" class="form-control" placeholder="标题" name="newsTitle" id="newsTitle" required>
			</div>
			<input type="hidden" id="title_check"/><br>
			<div class="form-group">
				<textarea class="form-control" name="content" style="width:200px;height: 400px;"
					></textarea>
			</div>
			<div class="form-group" id="news_select">
				<span>所属新闻分类:</span> 
					<select name="newsSortId" id="select1" required>
						<c:forEach items="${newsSortList }" var="newsSort">
							<option  value="${newsSort.sortId }">${newsSort.sortName }</option>
						</c:forEach>
					</select>
				<input type="submit" class="btn btn-primary btn-sm" value="保存" id="news_submit"/> 
			</div>
		</form>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
	$(function(){
		var fckeditor=new FCKeditor('content');
    	fckeditor.BasePath= getRootPath() + "/fckeditor/";//相对路径，相对于当前页面
    	fckeditor.ToolbarSet="mgTool";
    	fckeditor.Width="895";
    	fckeditor.Height="400";
    	fckeditor.ReplaceTextarea();
    	document.all.content.value="";
	})
</script>

