<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta charset="utf-8">
<link
	href="${pageContext.request.contextPath}/bclient/css/news/newsSortList.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/bclient/css/news/newsList.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/thirdParty/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/bclient/js/news/newsList.js" type="text/javascript">
</script>
<div id="listBody">
	<div class="funTitle"><b>当前位置>>栏目管理>>新闻>><a href ="${pageContext.request.contextPath}/newsSort.do?action=newsSortListTrue">新闻分类列表</a>>>未通过
		
	</div>
	<form 
		action="#" method="post" id="newsList" >
		<input type="hidden" id="arr" value="" name="arr"/>
		<table id="sortListTable" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-bordered table-striped table-hover table12">
			<tr>
				<th>新闻编号</th>
				<th>新闻标题</th>
				<th>编辑时间</th>
				<th>最后修改时间</th>
				<th>审核状态</th>
				<th>反馈信息</th>
				<th>编辑人</th>
				<th>操作</th>
				<th>选择</th>
			</tr>
			<%
				int a = 0;
			%>
			<c:if test="${empty page.list }">
				<td colspan="9"> 暂时无数据</td>
			</c:if>
			<c:forEach items="${page.list }" var="news">
				<tr>
					<%
						a = a + 1;
					%>
					<td>
						<%=a%>
						<c:if test="${news.headlineFlag eq 1 }">
							（头条）
						</c:if>
					</td>
					<td>${news.title }</td>
					<td><fmt:formatDate value="${news.addTime }" type="both"/></td>
					<td><fmt:formatDate value="${news.lastUpdateTime }" type="both"/></td>
					<c:if test="${news.status eq 1 }" >
						<td>完成</td>
					</c:if>
					<c:if test="${news.status eq 2 }" >
						<td>待审核</td>
					</c:if>
					<c:if test="${news.status eq 3 }" >
						<td>审核通过</td>
					</c:if>
					<c:if test="${news.status eq 4 }" >
						<td>审核不通过</td>
					</c:if>
					<c:if test="${news.status eq 5 }" >
						<td>已发布</td>
					</c:if>
					<c:if test="${news.status eq 6 }" >
						<td>已撤销</td>
					</c:if>
					<td>${news.mes }</td>
					<td>${news.user.userName}</td>
					<td><a href="${pageContext.request.contextPath}/news.do?action=showCheckNews&newsId=${news.newsId}">编辑</a></td>
					<td><input type="checkbox" class="allNewsId" value="${news.newsId }">
				</tr>
			</c:forEach>
		</table>
		<div id="checkboxFun">
			<input type="button" onclick="allSelect()" value="全选">
			<input type="button" onclick="reverseSelect()" value="反选">
			<input type="button" onclick="cancelSelect()" value="取消">
		</div>
		<div class="deleteNews">
			<input type="button" value="发送审核" onclick="sen()" />
			<input type="hidden" value="${pageContext.request.contextPath}/news.do?action=updateCheckNewsStatus&newsSortId=${sort.sortId}" id="sendCheck">
		</div>
	</form>
	<div align="center" id="news_page">
		<jsp:include page="../../public/pager.jsp">
			<jsp:param
				value="${pageContext.request.contextPath}/news.do?action=newsList&newsSortId=${sort.sortId }"
				name="path" />
		</jsp:include>
	</div>
</div>


