
//弹出提示(input的id的value值)
function alertMsg(id){
	var _msg = document.getElementById(id).value;
	if(_msg.length > 0){
		alert(_msg);
	}
}
$(function(){
	//弹出反馈信息
	alertMsg("msg");
	$(".btnNopass").click(function(){
		if(confirm("您真的要执行审核不通过操作吗？")){
			$("#checkedId").val($(this).attr("checkedId"));
			$.layer({
			    type: 1,
			    shade: [0],
			    area: ['400', '200'],
			    title: '审核信息反馈',
			    border: [10, 0.3, '#000'],
			    page: {dom : '#nopassDialog'}
			});
		}
	});
	
	//禁用和显示a操作链接
	$(".btnPass").each(function(index,dom){
		var _status = $(this).attr("status");
		if(_status != 2){
			$(this).replaceWith("<span style='color:gray'>审核通过</span>");
		}
	});
	$(".btnNopass").each(function(index,dom){
		var _status = $(this).attr("status");
		if(_status != 2){
			$(this).replaceWith("<span style='color:gray'>审核不通过</span>");
		}
	});
	$(".btnPublish").each(function(index,dom){
		var _status = $(this).attr("status");
		if(_status != 3 && _status != 6){
			$(this).replaceWith("<span style='color:gray'>发布</span>");
		}
	});
	$(".btnRevoke").each(function(index,dom){
		var _status = $(this).attr("status");
		if(_status != 5){
			$(this).replaceWith("<span style='color:gray'>撤销</span>");
		}
	});
	
});