//检查栏目是否为空  
function check(x) {
	var name = document.getElementById(x);
	if (name.value == "" || name.value == " ") {
		return true;
	}
	return false;
}

// 检查栏目长度是否达到要求
function checkMenu(x) {
	var name = document.getElementById(x);
	if (name.value == "" || name.value == " ") {
		alert("栏目名称为空!");
		return true;
	}
	if (name.value.length < 2 || name.value.length > 16) {
		alert(name.value.length + "栏目长度只能是2~8个汉字");
		return true;
	}
	return false;
}
// 检查链接格式是否正确(根据id验证元素)
function checkLink(y) {
	var link = document.getElementById(y);
	if (check(y)) {
		alert("链接为空");
		return true;
	}
	if (link.value == '#') {
		return false;
	}
	var strRegex = "^((https|http|ftp|rtsp|mms)://)?[a-z0-9A-Z]{3}\.[a-z0-9A-Z][a-z0-9A-Z]{0,61}?[a-z0-9A-Z]\.com|net|cn|cc (:s[0-9]{1-4})?/$";
	var re = new RegExp(strRegex);
	if (re.test(link.value)) {
		return false;
	} else {
		alert("链接格式错误");
		return true;
	}
}
// 返回值和上面相反
function check_Link(y) {
	var link = document.getElementById(y);
	if (check(y)) {
		alert("链接为空");
		return false;
	}
	if (link.value == '#') {
		return true;
	}
	var strRegex = "^((https|http|ftp|rtsp|mms)://)?[a-z0-9A-Z]{3}\.[a-z0-9A-Z][a-z0-9A-Z]{0,61}?[a-z0-9A-Z]\.com|net|cn|cc (:s[0-9]{1-4})?/$";
	var re = new RegExp(strRegex);
	if (re.test(link.value)) {
		return ture;
	} else {
		alert("链接格式错误");
		return false;
	}
}

// 设置等于name的复选框，为node状态选中或未选中
function checkAll(node, name) {
	var objs = document.getElementsByName(name);
	for (var i = 0; i < objs.length; i++) {
		objs[i].checked = node;
	}
}
function change(x) {
	var p = document.getElementsByName("showx");
	for (var i = 0; i < p.length; i++) {
		p[i].style.display = "none";
	}
	document.getElementById(x).style.display = "block";
}
// 检查是否选择name等于x的栏目
function check_select(x) {
	var z = document.getElementsByName(x);
	var n = document.getElementsByName("displayOrder");
	var h = 1;
	var flag = 0;
	for (var i = 0; i < z.length; i++) {
		if (z[i].checked) {
			flag++;
			var reg = new RegExp("^[0-9]*$");
			if (reg.test(n[i].value)) {
				h++;
			}
		}
	}
	if (flag == 0) {
		alert("你未选择，请选择要操作的栏目");
		return false;
	}
	if (flag == (h - 1)) {
		return true;
	} else {
		alert("请检查！,显示顺序为大于0的正整数");
		return false;
	}
}
function check_selectMenu(x) {
	var z = document.getElementsByName(x);
	var flag = 0;
	for (var i = 0; i < z.length; i++) {
		if (z[i].checked) {
			flag++;

		}
	}
	if (flag == 0) {
		alert("你未选择，请选择要操作的栏目");
		return false;
	}
	return true;
}
// 修改链接和名称之前，对输入值进行检查是否为空或满足链接格式
function check_update(x, y) {
	if (!checkUpdateForm()) {
		return false;
	} else {
		document.getElementById("change_one_form").action = 'menu.do?action=updateNameAndLink';
		document.getElementById("change_one_form").submit();
	}

}

// 验证修改栏目名称和链接弹框
function checkUpdateForm() {
	var put = document.getElementsByName("checkMenu");
	var name = document.getElementsByName("menuName");
	var link = document.getElementsByName("link");
	var str = '';
	var n = 0;
	var flag = 0;
	for (var i = 0; i < put.length; i++) {
		if (put[i].checked)
			flag++;
	}
	if (flag == 0) {
		alert("你未选择,请选择记录");
		return false;
	} else {
		for (var i = 0; i < name.length; i++) {
			if (put[i].checked) {
				if (name[i].value == '' || name[i].value == ' ') {
					alert("你选中的第" + (i + 1) + "个栏目名称为空！");
					return false;
				} else {
					var strRegex = "^((https|http|ftp|rtsp|mms)://)?[a-z0-9A-Z]{3}\.[a-z0-9A-Z][a-z0-9A-Z]{0,61}?[a-z0-9A-Z]\.com|net|cn|cc (:s[0-9]{1-4})?/$";
					var re = new RegExp(strRegex);
					if (re.test(link[i].value) || link[i].value == '#') {
						str += name[i].value + "," + link[i].value + ",";
						n++;
						if (n == flag) {
							document.getElementById("params").value = str;
							return true;
						} else {
							continue;
						}
					}
					alert("第" + (n + 1) + "个链接格式错误");
					return false;
				}
			}
		}
	}
}
// 增加栏目是，检查是否输入为空，连接是否格式正确
function check_add(x, y) {
	if (checkMenu(x)) {
		return false;
	} else {
		if (checkLink(y)) {
			document.getElementById("addForm").action = " ";
			return false;
		} else {
			if (check_rank()) {
				document.getElementById("addForm").action = 'menu.do?action=addMenu';
				document.getElementById("addForm").submit();
			}
		}

	}
}
// 发送审核，检查是否有选择栏目
function releaseMenu(x, y) {
	if (check_selectMenu(x)) {
		document.getElementById(y).action = "menu.do?action=sendCheckRequest";
		document.getElementById(y).submit();
		return true;
	}
}
// 删除栏目，检查是否选择栏目
function deleteMenu(x, y) {
	if (check_selectMenu(x)) {
		if (confirm("确定要删除这条记录吗，如果他有子栏目也会被删除")) {
			document.getElementById(y).action = "menu.do?action=deleteMenu";
			document.getElementById(y).submit();
			return true;
		} else {
			return false;
		}
	}
}
// 检查修改前台显示栏目顺序，是否为正整数
function check_displayOrder(id) {
	var x = document.getElementById(id);
	var reg = new RegExp("^[0-9]*$");
	if (x.value == '') {
		alert("输入值为空");
	} else {
		if (reg.test(x.value)) {
			window.location.href = "menu.do?action=updateDisplayOrder&menuId="
					+ id + "&displayOrder=" + x.value;
		} else {
			alert("请输入数字!");
		}
	}
}

// 显示对话框
function showDailog(x, y, z, a) {
	document.getElementById(x).style.display = "block";
	document.getElementById("old_name").innerHTML = y;
	document.getElementById("old_link").innerHTML = z;
	var n = document.getElementById("menuId").value = a;
}
// 隐藏对话框
function hideDailog(x) {
	document.getElementById(x).style.display = "none";
}
// 添加栏目时，添加栏目为一级栏目，隐藏上级栏目选项，选择为二级栏目时，显示上级栏目选项
function change_select(x) {
	var y = document.getElementsByName('preMenuId');
	if (x.value == 1) {
		for (var i = 0; i < y.length; i++) {
			y[i].value = 0;
			y[i].style.display = 'none';
		}
	} else {
		for (var i = 0; i < y.length; i++) {
			y[i].style.display = 'inline-block';
		}
	}
}

function check_rank() {
	var x = document.getElementsByName('menuRank');
	var y = document.getElementsByName('preMenuId');
	for(var j =0;j<x.length;j++){
		if(x[j].value==2&&y[j].value == 0){
			alert("你没有选择上级栏目");
			return false;
		}
	}
	return true;
}