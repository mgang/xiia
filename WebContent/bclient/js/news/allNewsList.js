var _allNewsId = "0";
var flag1 = 0;
var flag2 = 0;

function doHr(){
	$("#newsList").attr("action",$("#doHref").val());
	alert("设置成功");
	$("#newsList").submit();
	
}
function deleteNe(){
	$(".allNewsId").each(function(index,dom){
		if(this.checked){
			_allNewsId += "," + $(this).val();
		}
	});
	$("#arr").val(_allNewsId);
	if(_allNewsId.length==1){
		alert("您还没有选择！");
	}
	else{
		var r=confirm("确认删除？");
		if (r==true)
		  {
			$("#newsList").attr("action",$("#deleteNews").val());
			alert("删除成功");
			$("#newsList").submit();
		  }
		else
		  {
			alert("取消成功");
		  }
	}
}
function sen(){
	$(".allNewsId").each(function(index,dom){
		if(this.checked){
			_allNewsId += "," + $(this).val();
		}
	});
	$("#arr").val(_allNewsId);
	if(_allNewsId.length==1){
		alert("您还没有选择！");
	}
	else{
		var r=confirm("确认发送审核？");
		if (r==true)
		  {
			$("#newsList").attr("action",$("#sendCheck").val());
			alert("发送成功");
			$("#newsList").submit();
		  }
		else
		  {
			alert("取消成功");
		  }
	}
}
$(function(){
	$(".btnHeadline").click(function(){
		var action =$("#hidden_photo").val()+"headline.do?action=addHeadlineA&newsId="+ $(this).attr("newsId");
		$("#form_headline").attr("action",action);
		$.layer({
		    type: 1,
		    shade: [0],
		    area: ['300px', '230px'],
		    title: '设置头条信息',
		    border: [1,0.5,'black'],
		    page: {dom : '#set_headline'}
		});
	});
	$(".btnHead").click(function(){
		var uploadAction =$("#hidden_photo").val()+"headline.do?action=uploadPhotoA&newsId="+ $(this).attr("newsId");
		$("#form_head").attr("action",uploadAction);
		$.layer({
		    type: 1,
		    shade: [0],
		    area: ['250px', '150px'],
		    title: '设置头条照片',
		    border: [1,0.5,'black'],
		    page: {dom : '#set_head'}
		});
	});
});
function headlineUrl(){
	var urlString = $("#head_url").val();
	var houzhui=urlString.substring(urlString.lastIndexOf("."));
	if(houzhui==".jpg"||houzhui==".png"||houzhui==".gif"){
		alert("上传成功")
		return true;
	}
	else{
		alert("文件类型不对");
		return false;
	}	
}
function checkHeadName(){
	var headlineTitle = $("#headline_title").val();
	if(headlineTitle.length>10){
		flag1 = 0;
		$("#titleLong").text("头条标题长度不能大于10");
	}
	else{
		flag1 = 1;
		$("#titleLong").text("");
	}
}
function checkHeadNote(){
	var headlineNote = $("#headline_note").val();
	if(headlineNote.length>20){
		flag2 = 0;
		$("#noteLong").text("头条标题长度不能大于20");
	}
	else{
		flag2 = 1;
		$("#noteLong").text("");
	}
}
function goSubmit(){
	if(flag1 ==1&&flag2 ==1){
		alert("设置成功");
		return true;
		
	}
	else
		return false;
}
function allSelect(){
	$(".allNewsId").each(function(){
		$(this).prop("checked",true);
	})
}
function reverseSelect(){
	$(".allNewsId").each(function(){
		if(this.checked)
			$(this).prop("checked",false);
		else
			$(this).prop("checked",true);
	})
}
function cancelSelect(){
	$(".allNewsId").each(function(){
		$(this).prop("checked",false);
	})
}