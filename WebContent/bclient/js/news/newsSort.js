var flag = 0;
var m = 0;
$(function(){
	$("#create_news_sort").validate({
		rules:{
			sortName:{
						required:true,
						maxlength:7
			},	
			sortNote:{
						required:true,
						maxlength:30
			}
		},
		messages:{
			sortName:{
						required:"您的标题不能够为空",
						maxlength:"您的标题长度不能大于7"
			},
			sortNote:{
						required:"您的描述不能为空",
						maxlength:"您的描述不能大于30"
}
		}
	})
})
function checkSortName(object){
	var name = $(".sort_title").val();
	
	$("#title_hidden").text("");
	if(name!=""){
		$.ajax({
			url:getRootPath()+"/ajaxServlet",
			data:{"action":"checkSortName","sortName":name},
			type:'post',
			dataType:"json",
			success:function(res){
				var data = res.message;
				if(data=="F"){
					flag = 0;
					$("#title_have").text("对不起该分类已经存在！");
				}
				if(data=="T"){
					$("#title_have").text("")
					flag = 1;
				}
					
			}
		})
	}
	else
		$("#title_have").text("");
}
function updateSortName(object){
	var name = $(".sort_title").val();
	var oldName = $("#hidden_title").val();
	var status;
	if(oldName == name){
		stutus = 1;
	}
	else
		status = 0;
	$("#title_hidden").text("");
	if(name!=""){
		$.ajax({
			url:getRootPath()+"/ajaxServlet",
			data:{"action":"checkSortName","sortName":name},
			type:'post',
			dataType:"json",
			success:function(res){
				var data = res.message;
				if(data=="F"){
					if(status ==0){
						flag = 0;
						$("#title_have").text("对不起该分类已经存在！");
					}
					else{
						flag = 1;
						$("#title_have").text("");
					}
				}
				if(data=="T"){
					$("#title_have").text("");
					flag = 1;
				}
					
			}
		})
	}
	else
		$("#title_have").text("");
}
function goSubmit(){
	var sortTitle = $("#sortTitle").val();
	var sortNote = $("#sortNote").val();
	if(sortTitle.length>0&&sortTitle.length<8&&sortNote.length>0&&sortNote.length<31){
		m = 1;
	}
	else
		m = 0;
	if(flag==1&&m==1){
		alert("操作成功");
		return true;
	}
	else{
		alert("不符合要求！");
		return false;
	}
	
}