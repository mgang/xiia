<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/bclient/css/user/bt_table.css" type="text/css" rel="stylesheet"/>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/bclient/js/org.js" type="text/javascript"> </script>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;分支机构&gt;查看机构列表
	</div>
	<div class="funContent">
		<form  id="org" action="" method="post">
		<table class="table table-condensed table-bordered table-striped table-hover table12" cellspacing="0" cellpadding="0">
		    	<tr>
				<td>编号</td>
				<td>机构标题</td>
				<td>机构链接</td>
				<td>添加时间</td>
				<td>更新时间</td>
				<td>状态</td>
				<td>添加人</td>
				<td colspan=2>&nbsp;&nbsp;操作</td>
			</tr>
			<%
				int a=0;
			%>
			<c:if test="${empty page.list}">
				<td colspan="10">暂无数据!</td>
			</c:if>
			<c:forEach items="${page.list}" var="org">
			<tr>
				<%
					a=a+1;
				%>
				<td><%=a%></td>
				<td>${org.orgName}</td>
				<td><a href="http://${org.link}" target="blank">${org.link}</a></td>
				<td>${org.addTime}</td>
				<td>${org.lastUpdateTime}</td>
				 <c:if test="${org.status eq 1 }" >
						<td>编辑完成</td>
					</c:if>
				 	<c:if test="${org.status eq 2 }" >
						<td>待审核</td>
					</c:if>
					<c:if test="${org.status eq 3 }" >
						<td>审核通过</td>
					</c:if>
					<c:if test="${org.status eq 4 }" >
						<td>审核不通过</td>
					</c:if>
					<c:if test="${org.status eq 5 }" >
						<td>已发布</td>
					</c:if>
					<c:if test="${org.status eq 6 }" >
						<td>已撤销</td>
					</c:if>
				<td>${org.user.userName}</td>	
				<td><a href="${pageContext.request.contextPath}/org.do?action=editOrg&orgId=${org.orgId}">修改</a></td>
				<td><input type="checkbox" class="c" value="${org.orgId}" name="orgId" id="checkbox"></td>
			</tr>
			</c:forEach>
		</table>	
			<center><input type="button" value="删除" onclick="deleteO()"/></center>
			<input type="hidden" value="${pageContext.request.contextPath}/org.do?action=deleteOrg" id="deleteOrg">
		</form>
		<div align="center">
			<jsp:include page="../../public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/org.do?action=findAllOrg"
					name="path" />
			</jsp:include>
		</div>
	</div>
</div>