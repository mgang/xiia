<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="${pageContext.request.contextPath}/bclient/css/user/admin.css" type="text/css" rel="stylesheet"/>
<jsp:include page="../public/importValidate.jsp"></jsp:include>
<div class="fun">
	<div class="funTitle">
		　栏目管理&gt;分支机构&gt;修改分支机构
	</div>
		<form action="${pageContext.request.contextPath}/org.do?action=updateOrg&orgId=${org.orgId}" method="post" id="org">  
			<b>修改机构:</b><br>
			机构名称：<input type="text" name="orgName" required="required" value="${org.orgName}" id="name" onblur="checkName()"><br>
			机构连接：<input type="text" name="orgLink" required="required" value="${org.link}"><br>
			<input type="submit" value="确定" class="sort_submit">
		</form>				
	</div>

<script type="text/javascript" charset="utf-8">
$(function(){
	$("#org").validate({
		errorClass:"inputError",
		rules:{
			orgName:{
				required:true,
				maxlength:16
			},
			orgLink:{
				required:true,
				maxlength:16
			}		
		},
		messages:{
			orgName:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			},
			orgLink:{
				required:"机构名称不能为空",
				maxlength:"最多16个字符"
			}
		}
	})
});
function checkName(){
	var name = $("#name").val();
	if($("#name").val()!=''){
		$.ajax({
			url:getRootPath()+"/ajaxServlet",
			data:{"action":"orgName","name":name},
			type:'post',
			dataType:"json",
			success:function(res){
				if(res.msg=="T"){
					alert("该分类已存在！");
					$("#name").val("");
				}
			}
		})
	}
}

</script>