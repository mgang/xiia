﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<title>襄阳市信息产业协会云平台服务中心登陆</title>
<link href="${pageContext.request.contextPath}/bclient/css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
</head>

<body>
	<div class="login-panel">
	<form action="${pageContext.request.contextPath}/gf?action=login" method="post">
    <table class="login-context">
        <tr>
            <td width="400" align="center">
            </td>
            <td align="center">
                <table>
                    <tr>
                        <td height="40" align="center"><img src ="${pageContext.request.contextPath}/bclient/image/adminLogin/login_tit.png" alt="logo"/></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td width="80" height="35" align="right">帐 号：</td>
                        <td align="left"><input type="text" name="userName" value="${resUser.userName }" id="txtAccount" class="txt"></td>
                    </tr>
                    <tr>
                        <td width="80" height="35" align="right">密 码：</td>
                        <td align="left"><input type="password" name="password" value="${resUser.password }" id="txtLoginPwd" class="txt"></td>
                    </tr>
                    <tr>
                        <td width="80" height="35" align="right">验证码：</td>
                        <td align="left">
                            <table>
                                <tr>
                                    <td width="100" align="left">
                                        <input type="text" name="inCode" id="txtVerifyInput" maxlength="4" class="verify-input">
                                    </td>
                                    <td align="left" valign="middle">
                                        <img id="imgVerifyCode"   onclick="refresh(this)" src="${pageContext.request.contextPath}/validateCode?action=getCode" alt="验证码" width="60" height="25" title="验证码不区分大小写，点击刷新" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="80" height="35" align="right"></td>
                        <td align="left"><input type="submit" value="登 录" class="btn-login"></td>
                    </tr>
                    <tr>
                        <td width="80" height="35" align="right"><input id="resString" type="hidden" value="${resString }"/></td>
                        <td align="left"><span id="res_msg"></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</div>
<script type="text/javascript">
  		$(function(){
  			$("#res_msg").text($("#resString").val());
  		});
  		function refresh(obj) {
  	        obj.src = getRootPath() + "/validateCode?action=getCode&rand="+Math.random();
  	    }
  </script>
</body>
</html>