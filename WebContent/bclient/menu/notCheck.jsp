<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" href="${pageContext.request.contextPath }/bclient/css/menu/menu.css" type="text/csss"></link>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/bclient/js/menu/menu.js"></script>
<style type="text/css">
#show_all_menu_lyl {
	border: 1px solid #ccc;
	width: 95%;
	height: 500px;
	margin: 10px auto;
	border-radius:0px 0px 15px 15px;
}
tr:HOVER {
	background-color: #F5F5F5;
}
</style>

<div id="show_all_menu_lyl">
	<jsp:include page="module/menuNav.jsp"></jsp:include>
	<!--所有栏目  -->
	<div id="all_menu_lyl">
		<p><font size="2px"><b>位置：</b>查看栏目》》编辑完成栏目</font></p>
		<hr>
		<form action="" method="post" id="change_one_form">
			<table class="table table-condensed table-bordered table-striped table-hover" cellspacing="0" cellpadding="0" style="text-align:center;">
				<tr style="font-size: 14px;">
					<th>选择<a href="javascript:checkAll(true,'checkMenu');">全选</a>&nbsp;<a
						href="javascript:checkAll(false,'checkMenu');">取消</a></th>
					<th>栏目编号</th>
					<th>栏目名称</th>
					<th>链接</th>
					<th>创建时间</th>
					<th>修改时间</th>
					<th>栏目级别</th>
					<th>上级栏目</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
				<c:if test="${empty page.list}">
				<tr>
					<td colspan="10" style="font-size:12px;">没有栏目</td>
				</tr>
			</c:if>
				<c:if test="${not empty page.list}">
					<c:forEach items="${page.list}" var="menu">
						<tr style="font-size: 12px;">
							<td><input type="checkbox" name="checkMenu"
								value="${menu.menuId }" height="20px"></td>
							<td>${menu.menuId}</td>
							<td>${menu.menuName}</td>
							<td>${menu.link}</td>
							<td><fmt:formatDate type="both" dateStyle="medium"
									timeStyle="medium" value="${menu.createTime}" /></td>
							<td><fmt:formatDate type="both" dateStyle="medium"
									timeStyle="medium" value="${menu.lastUpdateTime}" /></td>
							<td><c:choose>
									<c:when test="${menu.menuRank=='1'}">
										<font color="blue">一级</font>
									</c:when>
									<c:otherwise>
										<font color="green">二级</font>
									</c:otherwise>
								</c:choose></td>
							<td><c:choose>
								<c:when test="${empty menu.menu}">
 									--
   								</c:when>
								<c:otherwise>
    								${menu.menu.menuName}
    							</c:otherwise>
							</c:choose></td>
							<td><font color="black">未审核</font></td>
							<td><a
								href="${pageContext.request.contextPath }/menu.do?action=sendCheckRequest&menuId=${menu.menuId}"
								style="text-decoration: none;">发送</a> <a
								href="${pageContext.request.contextPath }/menu.do?action=deleteMenu&menuId=${menu.menuId}"
								style="text-decoration: none;color:red" onclick="return confirm('你确定要删除这条栏目吗？它如果有子栏目也会被删掉')">删除</a>
						</tr>
					</c:forEach>
				</c:if>
			</table>
			<c:if test="${not empty page.list}">
				<center>
					<input type="button" value="发送"
						onclick="releaseMenu('checkMenu','change_one_form')" />&nbsp;&nbsp;<input
						type="button" value="删除"
						onclick="deleteMenu('checkMenu','change_one_form')" />&nbsp;&nbsp;<input
						type="reset" value="取消" />
				</center>
			</c:if>
		</form>
		<div align="center" id="lyl_page" id="lyl_page" style="position: absolute;bottom:29px;left:460px;">
			<jsp:include page="/public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/menu.do?action=showMenuBystatus&status=1"
					name="path" />
			</jsp:include>
		</div>
	</div>
</div>
<script type="text/javascript">
	window.onload = function() {
		if ('${message}' != "") {
			alert('${message}');
		}
	}
</script>
