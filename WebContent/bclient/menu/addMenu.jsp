<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<style type="text/css">
#add_menu_form {
	border: 1px solid #ccc;
	width: 95%;
	height: 480px;
	margin: 0px auto;
	border-radius: 0px 0px 15px 15px;
}

#add_menu_nav {
	line-height: 30px;
	margin-top: 0px;
	width: 100%;
	height: 30px;
	color: black;
	border-bottom: 1px solid #ccc;
	background: url("bclient/image/height30bg.png") repeat-x top;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/bclient/js/menu/menu.js"></script>
<script type="text/javascript">
	window.onload = function() {
		if ('${message}' != '') {
			alert('${message}');
		}
	}
</script>
<div id="add_menu_form">
	<p id="add_menu_nav">增加栏目</p>
	<form
		action="${pageContext.request.contextPath}/menu.do?action=addMenu"
		method="post" id="addForm">
		<input type="hidden" name="userId" value="1"> 栏目名称：<input
			type="text" name="menuName" size="12" id="update_name" /> 链 接：<input
			type="text" name="link" value="http://" size="16" id="update_link" />
		栏目等级：<select name="menuRank" onclick="change_select(this)">
			<option value="1">1</option>
			<option value="2">2</option>
		</select> 
		上级栏目：<select name="preMenuId" style="display:none;">
			<option value="0">空</option>
			<c:if test="${not empty requestScope.fatherMenu}">
				<c:forEach items="${requestScope.fatherMenu }" var="menu">
					<option value="${menu.menuId }">${menu.menuName}</option>

					<c:if test="${not empty menu.childMenu}">
						<c:forEach items="${menu.childMenu }" var="childMenu">
							<option class="child" disabled>&nbsp;&nbsp;|--${childMenu.menuName }</option>
						</c:forEach>
					</c:if>
					<c:if test="${empty menu.childMenu}">
						<option class="child" disabled>&nbsp;&nbsp;|--空</option>
					</c:if>
				</c:forEach>
			</c:if>
		</select>
		<p style="margin-left: 300px;">
			<input type="button" value="完成"
				onclick="check_add('update_name','update_link')" /> <input
				type="reset" value="取消" />
		</p>
	</form>
</div>