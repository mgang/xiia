<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/bclient/css/menu/menu.css"
	type="text/csss"></link>
<style type="text/css">
#show_all_menu_lyl {
	border: 1px solid #ccc;
	width: 95%;
	height: 500px;
	margin: 10px auto;
	border-radius: 0px 0px 15px 15px;
	position: relative;
}
</style>

<div id="show_all_menu_lyl">
	<jsp:include page="module/menuNav.jsp"></jsp:include>
	<!--所有栏目  -->
	<div id="all_menu_lyl">
		<p>
			<font size="2px"><b>位置：</b>查看栏目》》所有栏目</font>
		</p>
		<hr>
		<table
			class="table table-condensed table-bordered table-striped table-hover"
			cellspacing="0" cellpadding="0" style="text-align: center;">
			<tr>
				<th>栏目编号</th>
				<th>栏目名称</th>
				<th>链接</th>
				<th>创建时间</th>
				<th>修改时间</th>
				<th>栏目级别</th>
				<th>上级栏目</th>
				<th>状态</th>
			</tr>
			<c:if test="${empty page.list}">
				<tr>
					<td colspan="10" style="font-size: 12px;">没有栏目</td>
				</tr>
			</c:if>
			<c:if test="${not empty page.list}">
				<c:forEach items="${page.list}" var="menu">
					<tr style="font-size: 12px; height:20px;">
						<td>${menu.menuId}</td>
						<td>${menu.menuName}</td>
						<td>${menu.link}</td>
						<td><fmt:formatDate type="both" dateStyle="medium"
								timeStyle="medium" value="${menu.createTime}" /></td>
						<td><fmt:formatDate type="both" dateStyle="medium"
								timeStyle="medium" value="${menu.lastUpdateTime}" /></td>
						<td><c:choose>
								<c:when test="${menu.menuRank=='1'}">
									<font color="blue" size="1px">一级</font>
								</c:when>
								<c:otherwise>
									<font color="green" size="2px">二级</font>
								</c:otherwise>
							</c:choose></td>
						<td><c:choose>
								<c:when test="${menu.menu==null}">
 									--
   								</c:when>
								<c:otherwise>
    								${menu.menu.menuName}
    							</c:otherwise>
							</c:choose></td>
						<td><c:choose>
								<c:when test="${menu.status=='1'}">
									<font color="black" size="1px">未审核</font>
								</c:when>
								<c:when test="${menu.status=='2'}">
									<font color="red" size="2px">待审核</font>
								</c:when>
								<c:when test="${menu.status=='3'}">
									<font color="green" size="1px">通过</font>
								</c:when>
								<c:when test="${menu.status=='4'}">
									<font color="red" size="2px">未通过</font>
								</c:when>
								<c:when test="${menu.status=='5'}">
									<font color="blue" size="1px">前台</font>
								</c:when>
								<c:otherwise>
									<font color="#ccc" size="2px">撤除</font>
								</c:otherwise>
							</c:choose></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
		<div align="center" id="lyl_page"
			style="position: absolute; bottom: 5px; left: 360px;">
			<jsp:include page="/public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/menu.do?action=showAllMenuByPager"
					name="path" />
			</jsp:include>
		</div>
	</div>
</div>

