<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<style>
#update_nav{
	margin-top:0px;
	list-style-type: none;
	width:96.4%;
	height:30px;
	background: url("bclient/image/height30bg.png") repeat-x top;
}
#update_nav li{
	margin-left:20px;
 	background-color: #29D33B;
 	color:white;
 	border:1px solid white;
	border-top:none;
	font-size:15px;
	float:left;
}
#update_nav li a{
	text-decoration: none;
	color:white;
	line-height:25px;
}
#update_nav li a:HOVER {
	color:blue;
	height:16px;
}
</style>
<div id="update_menu_nav">
	<ul id="update_nav">
		<li><a href="${pageContext.request.contextPath }/menu.do?action=showCanUpdateMenu" target="main">修改栏目名称和链接</a></li>
		<li><a href="${pageContext.request.contextPath }/menu.do?action=showCanUpdateDisplayOrderMenu" target="main">修改显示顺序</a></li>
	</ul>
</div>