<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<style type="text/css">
#menu_nav{
	width:97%;
	height:30px;
	padding:0px;
	margin-top:0px;
}
#menu_nav_menu{
	margin-top:0px;
	list-style-type: none;
	background: url("bclient/image/height30bg.png") repeat-x top;
	width:99.5%;
	height:30px;
}

#menu_nav_menu li{
	float:left;
	background-color: #29D33B;
	border:1px solid white;
	border-top:none;
	margin-left:10px;
	padding:0px;
	height:25px;
	color:white;
	font-size:15px;
}
#menu_nav_menu li a{
	text-decoration: none;
	color:white;
	line-height:25px;
	width:100%;
}
#menu_nav_menu li a:HOVER{
	color:blue;
	height:16px;
}
</style>
<div id="menu_nav">
		<ul id="menu_nav_menu">
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showAllMenuByPager" target="main">所有栏目</a></li>
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showMenuBystatus&status=1" target="main">编辑完成栏目</a></li>
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showMenuBystatus&status=2" target="main">待审核栏目</a></li>
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showMenuBystatus&status=3" target="main">审核通过栏目</a></li>
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showMenuBystatus&status=4" target="main">未通过栏目</a></li>
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showMenuBystatus&status=5" target="main">前台显示栏目</a></li>
			<li><a href="${pageContext.request.contextPath }/menu.do?action=showMenuBystatus&status=6" target="main">撤除的栏目</a></li>
		</ul>	
</div>