<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/bclient/css/menu/menu.css"
	type="text/css"></link>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/bclient/js/menu/menu.js"></script>
<script type="text/javascript">
	window.onload=function (){
		if( '${message}'!=''){
			alert('${message}');
		}
	}
</script>
<style type="text/css">
#Rank {
	border:1px solid #ccc;
	margin: 10px auto;
	height:90%;
	width:96%;
	border-radius:0px 0px 15px 15px;
}
.do_what{
	height:30px;
	width:100%;
}
tr:HOVER {
	background-color: #F5F5F5;
}
</style>

<div id="Rank">
<jsp:include page="module/updateMenuNav.jsp"></jsp:include>
	<div class="do_what"><p><font size="2px"><b>位置：</b>修改栏目》》修改显示栏目</font></p></div>
	<div class="rank_table">
		<form action="menu.do?action=updateDisplayOrder" method="post" onsubmit="return check_select('checkMenu')">
			<table class="table table-condensed table-bordered table-striped table-hover" cellspacing="0" cellpadding="0" style="text-align: center;">
				<tr>
					<th>选择<a href="javascript:checkAll(true,'checkMenu');">全选</a>&nbsp;<a
						href="javascript:checkAll(false,'checkMenu');">取消</a></th>
					<th>栏目名称</th>
					<th>显示顺序</th>
					<th>创建时间</th>
					<th>最后修改时间</th>
					<th>栏目等级</th>
					<th>操作</th>
				</tr>
				<c:if test="${empty requestScope.fatherMenu}">
				<tr>
					<td colspan="7" style="font-size:12px;">没有栏目</td>
				</tr>
			</c:if>
				<c:if test="${not empty requestScope.fatherMenu }">
					<c:forEach items="${requestScope.fatherMenu }" var="menu">
						<tr style="font-size: 12px;text-align: center">
							<td><input type="checkbox" name="checkMenu" value="${menu.menuId }"></td>
							<td>${menu.menuName }s<input type="hidden" name="id"
								value="${menu.menuId }"></td>
							<td><input type="text" value="${menu.displayOrder }"
								size="8" name="displayOrder"id="${menu.menuId }"></td>
							<td><fmt:formatDate type="both" dateStyle="medium"
									timeStyle="medium" value="${menu.createTime}" /></td>
							<td><fmt:formatDate type="both" dateStyle="medium"
									timeStyle="medium" value="${menu.lastUpdateTime}" /></td>
							<td><font color="green" size="3px">一级</font></td>
							<td><a href="javascript:check_displayOrder('${menu.menuId }');" style="text-decoration: none; font-size:12px;">修改</a></td>
						</tr>
							<c:if test="${not empty menu.childMenu }">
								<c:forEach items="${menu.childMenu }" var="child">
								<tr style="font-size: 12px;text-align: center;color:black;">
									<td><input type="checkbox" name="checkMenu" value="${child.menuId }"></td>
									<td>&nbsp;&nbsp;|--<font color="#ccc" size="1em">${child.menuName }</font><input type="hidden" name="id"
										value="${child.menuId }"></td>
									<td><input type="text" value="${child.displayOrder }"
										size="5" name="displayOrder" id="${child.menuId }"></td>
									<td><fmt:formatDate type="both" dateStyle="medium"
											timeStyle="medium" value="${child.createTime}" /></td>
									<td><fmt:formatDate type="both" dateStyle="medium"
											timeStyle="medium" value="${child.lastUpdateTime}" /></td>
									<td><font color="#ccc" size="1em">二级</font></td>
									<td><a href="javascript:check_displayOrder('${child.menuId }');"style="text-decoration: none; font-size:12px;">修改</a></td>
										</tr>
								</c:forEach>
							</c:if>
					</c:forEach>
				</c:if>
			</table>
			<c:if test="${not empty requestScope.fatherMenu }">
				<center>
					<br/> <input type="submit" value="修改"/>&nbsp;&nbsp; <input
						type="reset" value="取消" />
				</center>
			</c:if>
			</form>
	</div>

</div>