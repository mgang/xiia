<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/bclient/css/menu/menu.css"
	type="text/csss"></link>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/bclient/js/menu/menu.js"></script>
<script type="text/javascript">
	window.onload = function() {
		if ('${message}' != '') {
			alert('${message}');
		}
	}
</script>
<style type="text/css">
#show_all_menu_lyl {
	border: 1px solid #ccc;
	width: 95%;
	height: 500px;
	margin: 10px auto;
	border-radius: 0px 0px 15px 15px;
}

tr:HOVER {
	background-color: #F5F5F5;
}

#update_menu_lyl {
	display: none;
	position: absolute;
	left: 400px;
	top: 70px;
	width: 400px;
	height: 320px;
	border: 1px solid #ccc;
	z-index: 999;
	background-color: white;
}

#update_menu_lyl h4 {
	margin: 0px;
	padding: 5px;
	width: 98%;
	height: 30px;
	background: url("bclient/image/height30bg.png") repeat-x top;
	font-size: 15px;
	color: black;
	font-style: 宋体;
}

#update_menu_lyl p {
	margin-left: 100px;
}
</style>

<div id="show_all_menu_lyl">
	<jsp:include page="module/menuNav.jsp"></jsp:include>
	<!--所有栏目  -->
	<div id="all_menu_lyl">
		<p>
			<font size="2px"><b>位置：</b>查看栏目》》未通过栏目</font>
		</p>
		<hr>
		<form action="" method="post" id="change_one_form">
			<input type="hidden" id="params" name="params" value=""/>
			<input type="hidden" name="flag" value="notpass"/>
			<input type="hidden" name="s" value="3"/>
			<table
				class="table table-condensed table-bordered table-striped table-hover"
				cellspacing="0" cellpadding="0" style="text-align: center;">
				<tr bgcolor="#ccc" style="font-size: 14px;">
					<th>选择<a href="javascript:checkAll(true,'checkMenu');">全选</a>&nbsp;<a
						href="javascript:checkAll(false,'checkMenu');">取消</a></th>
					<th>栏目编号</th>
					<th>栏目名称</th>
					<th>链接</th>
					<th>创建时间</th>
					<th>修改时间</th>
					<th>栏目级别</th>
					<th>上级栏目</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
				<c:if test="${empty page.list}">
					<tr>
						<td colspan="10" style="font-size: 12px;">没有栏目</td>
					</tr>
				</c:if>
				<c:if test="${not empty page.list}">
					<c:forEach items="${page.list}" var="menu">
						<tr style="font-size: 12px;">
							<td><input type="checkbox" name="checkMenu"
								value="${menu.menuId }"></td>
							<td>${menu.menuId}</td>
							<td><input type="text" name="menuName" value="${menu.menuName}" /></td>
							<td><input type="text" name="link" value="${menu.link}" /></td>
							<td><fmt:formatDate type="both" dateStyle="medium"
									timeStyle="medium" value="${menu.createTime}" /></td>
							<td><fmt:formatDate type="both" dateStyle="medium"
									timeStyle="medium" value="${menu.lastUpdateTime}" /></td>
							<td><c:choose>
									<c:when test="${menu.menuRank=='1'}">
										<font color="blue" size="1px">一级</font>
									</c:when>
									<c:otherwise>
										<font color="green" size="2px">二级</font>
									</c:otherwise>
								</c:choose></td>
							<td><c:choose>
									<c:when test="${empty menu.menu}">
 									--
   								</c:when>
									<c:otherwise>
    								${menu.menu.menuName}
    							</c:otherwise>
								</c:choose></td>
							<td><font color="red" size="2px">未通过</font></td>
							<td><a href="#" style="text-decoration: none;"
								onclick="showDailog('update_menu_lyl','${menu.menuName}','${menu.link}','${menu.menuId }')"
								name="update_btn">修改</a> <a
								href="${pageContext.request.contextPath }/menu.do?action=deleteMenu&s=3&menuId=${menu.menuId}"
								style="text-decoration: none; color: red"
								onclick="return confirm('你确定要删除这条栏目吗？它如果有子栏目也会被删掉')">删除</a></td>
						</tr>
					</c:forEach>
				</c:if>
			</table>
			<c:if test="${not empty page.list}">
				<center>
					<input type="button" value="修改" onclick="check_update()" />&nbsp;&nbsp;<input
						type="button" value="删除" onclick="deleteMenu('checkMenu','change_one_form')" />&nbsp;&nbsp;<input
						type="reset" value="取消" />
				</center>
			</c:if>
		</form>
		<div align="center" id="lyl_page"
			style="position: absolute; bottom: 29px; left: 460px;">
			<jsp:include page="/public/pager.jsp">
				<jsp:param
					value="${pageContext.request.contextPath}/menu.do?action=showMenuBystatus&status=4"
					name="path" />
			</jsp:include>
		</div>
	</div>
</div>
<div id="update_menu_lyl" style="display: none;">
	<h4>
		输入链接和名称
		<button onclick="hideDailog('update_menu_lyl')"
			style="float: right; margin: 0px;">X</button>
	</h4>
	<br />
	<p>
		栏目名称:<span id="old_name"></span>
	</p>
	<p>
		链&nbsp;&nbsp;&nbsp;&nbsp;接:<span id="old_link"></span>
	</p>
	<form
		action="${pageContext.request.contextPath }/menu.do?action=updateNameAndLink&flag=notpass"
		method="post" onsubmit="return check_Link('input_link')">
		<p>
			<input type="hidden" name="menuId" id="menuId" value="" />
			新&nbsp;名&nbsp;称:<input type="text" name="menuName"
				class="newMenuName" required="required" />
		</p>
		<p>
			新&nbsp;链&nbsp;接:<input type="text" name="link" id="input_link"
				class=" newMenuLink" required="required" /><span></span>
		</p>
		<p style="margin-left: 170px;">
			<input type="submit" value="修改" /><input type="reset" value="取消" />
		</p>
	</form>
</div>
