<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/nav.css" type="text/css"></link>
<script type="text/javascript" src="${pageContext.request.contextPath}/fclient/js/nav.js"></script>
<div id="nav_zh1">
	<div id="nav_content_zh1">
		<div id="nav_content_left_zh1">
			<div class="unit_zh1">
				<span class="title_zh1"><a href="${pageContext.request.contextPath}">首页</a></span>
			</div>
			<c:if test="${not empty requestScope.fatherMenu }">
				<c:forEach items="${requestScope.fatherMenu }" var="menu">
					<div class="line_zh1">|</div>
					<div class="unit_zh1" onmouseover="show_drown(this)" onmouseout="hide_drown(this)">
						<span class="title_zh1"><a href="${menu.link }">${menu.menuName }</a></span>
						<ul class="menu_zh1">
							<c:forEach items="${menu.childMenu}" var="childMenu">						
								<li class="menu_li_zh1"><a style="color:black;" href="${childMenu.link }">${childMenu.menuName}</a></li>
							</c:forEach>
						</ul>
					</div>
				</c:forEach>
			</c:if>
		</div>
		<div id="nav_content_right_zh1">
			<form id="form_zh1" action="#" method="post">
				<input id="search_content_zh1" placeholder="请输入要查找的内容"/>
				<button id="search_btn_zh1">搜　索</button>
			</form>
		</div>
	</div>
</div>