<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<link href="<%=basePath %>fclient/css/pptShow.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=basePath %>fclient/js/jquery.mgang.pptShow.2.0.js"></script>

    
<div class="pptShowBox" style="">
	<div class="pptItem"><a href="#"><img src="<%=basePath %>fclient/image/1.jpg" imgId="1"><span class="urlText"><p class="light">第一张11111111111111</p></span></a></div>
    <div class="pptItem"><a href="#"><img src="<%=basePath %>fclient/image/2.jpg" imgId="2"><span class="urlText"><p>第二张</p></span></a></div>
    <div class="pptItem"><a href="#"><img src="<%=basePath %>fclient/image/3.jpg" imgId="3"><span class="urlText"><p>第三张</p></span></a></div>
    <div class="pptItem"><a href="#"><img src="<%=basePath %>fclient/image/4.jpg" imgId="4"><span class="urlText"><p>第四张</p></span></a></div>
    <div class="pptItem"><a href="#"><img src="<%=basePath %>fclient/image/5.jpg" imgId="5"><span class="urlText"><p>第五张</p></span></a></div>
</div>
<script type="text/javascript">
     $(".pptShowBox").pptShow({
         width:780,
         height:332,
         bwtPx:55,
         urlTextMarginLeft:400,
         urlTextMarginTop:250,
         maxCount:5
     }).initPPTShow().openLoopShow();
  </script>