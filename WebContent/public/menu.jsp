<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="menu">
	<div class="menu_head">
		<div class="news" id="news"><a href="${pageContext.request.contextPath}/fNewsSort.do?action=newsSortListFclient">新闻</a></div><div class="announcement"><a href="${pageContext.request.contextPath}/noticeSort.htm?action=showNoticeSortList">公告</a></div>
		<div class="academic"><a href="#">学术</a></div><div class="declare"><a href="#">申报</a></div>
		<div class="new_media"><a href="#">新媒体</a></div>
	</div>
	<div class="menu_body">
		<div class="menu_body1">
			<div class="list_head">常务理事协会</div>
			<div class="menu_ul">
				<ul>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
				</ul>
			</div>
		</div>
		<div class="menu_body1">
			<div class="list_head">常务理事协会</div>
			<div class="menu_ul">
				<ul>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
				</ul>
			</div>
		</div>
		<div class="menu_body1">
			<div class="list_head">常务理事协会</div>
			<div class="menu_ul">
				<ul>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
				</ul>
			</div>
		</div>
		<div class="menu_body2">
			<div class="list_head">常务理事协会</div>
			<div class="menu_ul">
				<ul>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
					<li><a href="#">襄阳信息产业协会</a></li>
				</ul>
			</div>
		</div>
	</div>
	
		<div id="news_hover" class="_hover">
			<ul class="_ul">
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
			</ul>
			<div class="_more">
				<a href="#">更多>>></a>
			</div>
		</div>
		<div id="announcement_hover" class="_hover">
			<ul class="_ul">
				<c:forEach items="${topXNotices }" var="notice">
					<li>
						<a title="${notice.title }" href="${pageContext.request.contextPath}/notice.htm?action=showNotice&noticeId=${notice.noticeId}">${notice.title }</a>
						<span>（<fmt:formatDate value="${notice.lastUpdateTime}" pattern="yyyy-M-d"/>）</span>
					</li>
				</c:forEach>
			</ul>
			<div class="_more">
				<a href="${pageContext.request.contextPath}/noticeSort.htm?action=showNoticeSortList">更多>>></a>
			</div>
		</div>
		<div id="academic_hover" class="_hover">
			<ul class="_ul">
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
			</ul>
			<div class="_more">
				<a href="#">更多>>></a>
			</div>
		</div>
		<div id="declare_hover" class="_hover">
			<ul class="_ul">
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
			</ul>
			<div class="_more">
				<a href="#">更多>>></a>
			</div>
		</div>
		<div id="new_media_hover" class="_hover">
			<ul class="_ul">
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
				<li><a href="#">襄阳信息产业协会云博科技有限公司</a>&nbsp;&nbsp;&nbsp;(2014/10/21)</li>
			</ul>
			
			<div class="_more">
				<a href="#">更多>>></a>
			</div>
		</div>
	
	
</div>