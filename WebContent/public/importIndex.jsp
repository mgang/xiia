<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/common.css" type="text/css"></link>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/head.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/index.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/logo.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/nav.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/nav_2.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/menu.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/foot.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/member/mem_main.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/member/mem_login.css" type="text/css"></link>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/member/mem_register.css" type="text/css"></link>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/fclient/js/head.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/fclient/js/nav.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/fclient/js/menu.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/fclient/js/more.js"></script>
