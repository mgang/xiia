<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/fclient/css/home/nav_2.css" type="text/css"></link>  
<script type="text/javascript" src="${pageContext.request.contextPath}/fclient/js/nav.js"></script>
<div id="nav_zh2">
	<div id="nav_content_zh2">
		<div id="nav_content_left_zh2">
			<div class="unit_zh2" onmouseover="show_drown2(this)" onmouseout="hide_drown2(this)">
				<div class="title_zh2_1">
					<span>
						<a href="${pageContext.request.contextPath}/fclient/society/soc_index.jsp">协会介绍</a>
					</span>
				</div>
				<ul class="menu_zh2">						
					<li class="menu_li_zh2"><a href="#">简介</a></li>
					<li class="menu_li_zh2"><a href="#">章程</a></li>
					<li class="menu_li_zh2"><a href="#">荣誉</a></li>
					<li class="menu_li_zh2"><a href="#">领导</a></li>
					<li class="menu_li_zh2"><a href="#">组织结构</a></li>
					<li class="menu_li_zh2"><a href="#">联系方式</a></li>
				</ul>
			</div>
			<div class="unit_zh2" onmouseover="show_drown2(this)" onmouseout="hide_drown2(this)">
				<div class="title_zh2_2">
					<span>
						<a href="${pageContext.request.contextPath}/fclient/society/soc_index.jsp">政策法规</a>
					</span>
				</div>
				<ul class="menu_zh2">						
					<li class="menu_li_zh2"><a href="#">条例章程</a></li>
					<li class="menu_li_zh2"><a href="#">规定办法</a></li>

				</ul>
			</div>
			<div class="unit_zh2" onmouseover="show_drown2(this)" onmouseout="hide_drown2(this)">
				<div class="title_zh2_3">
					<span>
						<a href="${pageContext.request.contextPath}/fclient/society/soc_index.jsp">分支机构</a>
					</span>
				</div>
				<ul class="menu_zh2">						
					<li class="menu_li_zh2"><a href="#">办公室</a></li>
					<li class="menu_li_zh2"><a href="#">电子商务专委会</a></li>
					<li class="menu_li_zh2"><a href="#">软件业专委会</a></li>
				</ul>
			</div>
			<div class="unit_zh2" onmouseover="show_drown2(this)" onmouseout="hide_drown2(this)">
				<div class="title_zh2_4">
					<span>
						<a href="${pageContext.request.contextPath}/fclient/society/soc_index.jsp">会员</a>
					</span>
				</div>
				<ul class="menu_zh2">						
					<li class="menu_li_zh2"><a href="#">会员中心</a></li>
					<li class="menu_li_zh2"><a href="#">登录</a></li>
					<li class="menu_li_zh2"><a href="#">注册</a></li>
					<li class="menu_li_zh2"><a href="#">修改信息</a></li>
				</ul>
			</div>
		</div>
		<div id="nav_content_right_zh2">
			<form id="form_zh2" action="#" method="post">
				<button id="search_btn_zh2">搜　索</button>
				<input id="search_content_zh2" placeholder="请输入要查找的内容"/>
			</form>
		</div>
	</div>
</div>